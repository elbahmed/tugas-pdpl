<?php $app = \Yii::$app->name ?>
<?= 'Assalamualaikum Wr. Wb. ' ?>,

<?= "Kata sandi akun anda di Website $app telah di setel ulang"?>.
<?= 'Kami telah membuat kata sandi baru anda' ?>:
<?= $password ?>

<?= "Jika bukan anda yang melakukan penyetelan ulang kata sandi, segera login ke website $app menggunakan kata sandi (<strong>$password</strong>) dan segera lakukan penggantian kata sandi. Terima kasih" ?>.