<?php $app = \Yii::$app->name ?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= 'Assalamualaikum Wr. Wb. ' ?>,
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= "Kata sandi akun anda di Website $app telah di setel ulang"?>.
    <?= 'Kami telah membuat kata sandi baru anda' ?>: <strong><?= $password ?></strong>
</p>

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= "Jika bukan anda yang melakukan penyetelan ulang kata sandi, segera login ke website $app menggunakan kata sandi (<strong>$password</strong>) dan segera lakukan penggantian kata sandi. Terima kasih" ?>.
</p>
