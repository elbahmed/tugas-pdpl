<?php

namespace app\controllers;

use yii\web\Controller;

class CopyrightController extends Controller
{
    public function actionDisclaimer()
    {
        return $this->render('disclaimer');
    }

    public function actionPrivacyPolicy()
    {
        return $this->render('privacy-policy');
    }

    public function actionTermsConditions()
    {
        return $this->render('terms-conditions');
    }
}
