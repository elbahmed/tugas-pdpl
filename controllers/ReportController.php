<?php

namespace app\controllers;

use app\models\Mahad;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'super'],
                        'allow' => true,
                        'actions' => ['mahad-pdf', 'monthly-pdf', 'semesterial-pdf', 'yearly-pdf']
                    ],
                ],
            ],
        ];
    }

    public function actionMonthly()
    {
        $mahad =  Mahad::find()->all();

        return $this->render('monthly', [
            'mahad' => $mahad
        ]);
    }

    public function actionMonthlyPdf()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $mahad =  Mahad::find()->all();
        $content = $this->renderPartial('monthly', [
            'mahad' => $mahad
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}', 
            'options' => ['title' => 'Laporan Bulanan Santri Al Askar'],
            'methods' => [ 
                'SetHeader'=>['Ma\'had Al Askar'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
            
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionSemesterialPdf()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $mahad =  Mahad::find()->all();
        $content = $this->renderPartial('semesterial', [
            'mahad' => $mahad
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}', 
            'options' => ['title' => 'Laporan Semester Santri Al Askar'],
            'methods' => [ 
                'SetHeader'=>['Ma\'had Al Askar'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
            
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionYearlyPdf()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $mahad =  Mahad::find()->all();
        $content = $this->renderPartial('yearly', [
            'mahad' => $mahad
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}', 
            'options' => ['title' => 'Laporan Tahunan Santri Al Askar'],
            'methods' => [ 
                'SetHeader'=>['Ma\'had Al Askar'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
            
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionMahadPdf()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $query = Mahad::find()->orderBy([
            'mahad_type_id' => SORT_ASC, 
            'province_id' => SORT_ASC, 
            'regency_id' => SORT_ASC,
            'name' => SORT_ASC,
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);
        $content = $this->renderPartial('mahad', [
            'dataProvider' => $dataProvider
        ]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_BROWSER, 
            'content' => $content,  
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}', 
            'options' => ['title' => 'Laporan Bulanan Santri Al Askar'],
            'methods' => [ 
                'SetHeader'=>['Ma\'had Al Askar'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
            
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
    
}
