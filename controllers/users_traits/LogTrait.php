<?php

namespace app\controllers\users_traits;

use app\models\users\Log;
use app\models\users\LogType;
use yii\data\ActiveDataProvider;
use app\traits\LogingTrait;
use Yii;

trait LogTrait 
{
    use LogingTrait;

    public function actionLoginLog()
    {
        $searchModel = Log::find()->where(['type_id' => 3])->orderBy(['time' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel,
            'pagination' => false,
            'sort' => false
        ]);
        $this->createTransactionLog('[SELECT] at Login Log Table');

        return $this->render('log/login-log', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTransactionLog()
    {
        $searchModel = Log::find()->where(['type_id' => 2])->orderBy(['time' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel,
            'pagination' => false,
            'sort' => false
        ]);
        $this->createTransactionLog('[SELECT] at Transaction Log Table');

        return $this->render('log/transaction-log', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteLog($id)
    {
        $type = LogType::findOne($id)->name;
        Log::deleteAll(['type_id' => $id]);
        \Yii::$app->getSession()->setFlash('success',"$type Log has been deleted");
        $this->createTransactionLog('[DELETE] at '. LogType::findOne($id)->name .' Log Table');

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

}