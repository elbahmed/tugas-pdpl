<?php

namespace app\controllers\users_traits;

use app\models\users\forms\AssignUserRoleForm;
use yii\data\ActiveDataProvider;
use app\models\users\User;
use app\models\users\UserRole;
use app\traits\LogingTrait;
use Yii;

trait RoleTrait 
{
    use LogingTrait;

    public function actionCreateRole()
    {
        $model = new UserRole();
        $auth = Yii::$app->authManager;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $admin = $auth->createRole($model->name);
            $admin->description = $model->description;
            $auth->add($admin);
            \Yii::$app->getSession()->setFlash('success', 'New Role has been created');
            $this->createTransactionLog('[INSERT] at User Role Table');

            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->renderAjax('role/_create-role', [
            'model' => $model,
        ]);
    }

    public function actionAssignUserRole()
    {
        $model = new AssignUserRoleForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $auth = Yii::$app->authManager;
            $roleObject = $auth->getRole($model->item_name);
            $auth->assign($roleObject, $model->user_id);
            \Yii::$app->getSession()->setFlash('success', sprintf('%s Has Been Assigned to %s', $roleObject->name, User::findOne($model->user_id)->username));
            $this->createTransactionLog('[INSERT] at Role Assignment Table');

            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->renderAjax('role/_assign-user-role', [
            'model' => $model,
        ]);
    }

    public function actionIndexRole()
    {
        $searchModel = UserRole::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel,
            'pagination' => false
        ]);
        $this->createTransactionLog('[SELECT] at User Role Table');

        return $this->renderAjax('role/_index-role', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteAllRole()
    {
        $auth = Yii::$app->authManager;   
        $auth->removeAllRoles();
        \Yii::$app->getSession()->setFlash('success','Deleted All Role.');
        $this->createTransactionLog('[DELETE] All Row at User Role Table');

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionDeleteRole($name)
    {
        $auth = Yii::$app->authManager;   
        $delete = $auth->getRole($name);
        $auth->remove($delete);
        \Yii::$app->getSession()->setFlash('success', sprintf('%s Role Has been deleted', $name));
        $this->createTransactionLog('[DELETE] One Row at User Role Table');

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
}