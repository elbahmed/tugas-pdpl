<?php

namespace app\controllers\users_traits;

use app\models\users\forms\ChangePasswordForm;
use app\models\users\Log;
use app\models\users\User;
use app\traits\LogingTrait;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

trait UserTrait
{
    use LogingTrait;

    public function actionIndexUser() 
    {
        $searchModel = User::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $searchModel,
            'pagination' => false,
            'sort' => false
        ]);
        $this->createTransactionLog('[SELECT] at User Table');

        return $this->render('user/index-user', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewUser($id)
    {   
        $model = User::findOne($id);

        if ( Yii::$app->user->can('super')) {
            if ($id != Yii::$app->user->id) {
                throw new ForbiddenHttpException('Access Denied');
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['id' => $id]),
            'pagination' => false,
            'sort' => false
        ]);

        $logProvider = new ActiveDataProvider([
            'query' => Log::find()->where(['user' => $model->username])->orderBy(['type_id' => SORT_ASC, 'time' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => false
        ]);

        $this->createTransactionLog('[SELECT] One Row at User Table');

        return $this->render('user/view-user', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'logProvider' => $logProvider,
        ]);
    }

    public function actionBlockUser($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', 'You can not block your own account');

        } else {
            $user  = $this->findUser($id);

            if ($user->getIsBlocked()) {
                $user->unblock();
                \Yii::$app->getSession()->setFlash('success', "$user->username has been unblocked");

            } else {
                $user->block();
                \Yii::$app->getSession()->setFlash('success', "$user->username has been blocked");
            }
        }
        $this->createTransactionLog('[UPDATE] at User Meta Table [Block Status]');

        return $this->redirect(\Yii::$app->request->referrer ?: \Yii::$app->homeUrl);
    }

    public function actionDeleteUser($id)
    {
        $model = $this->findUser($id);
        $username = $model->username;
        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash('success', "$username has been deleted");
            $this->createTransactionLog('[DELETE] at User Table');

            return $this->redirect(\Yii::$app->request->referrer ?: \Yii::$app->homeUrl);
        }
    }

    public function actionChangePassword() 
    {
        $model = new ChangePasswordForm();

	    if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            
	    	return ActiveForm::validate($model);
        }
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            
            if($model->changed()) {
                \Yii::$app->getSession()->setFlash('success', 'Kata sandi anda berhasil dirubah.');
                \Yii::$app->user->logout();
                $this->goHome();
            }
            else {
                return 'error';
            }
        }

        return $this->renderAjax('user/_change-password', [
            'model' => $model,
        ]);
    }

    protected function findUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}