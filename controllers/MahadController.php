<?php

namespace app\controllers;

use Yii;
use app\models\Mahad;
use app\models\users\User;
use app\traits\DepDropTrait;
use app\traits\LogingTrait;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * MahadController implements the CRUD actions for Mahad model.
 */
class MahadController extends Controller
{
    use LogingTrait;
    use DepDropTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'super', 'admin'],
                        'allow' => true,
                        'actions' => ['create', 'update', 'view', 'form', 'delete']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mahad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Mahad::find()->orderBy([
            'mahad_type_id' => SORT_ASC, 
            'province_id' => SORT_ASC, 
            'regency_id' => SORT_ASC,
            'name' => SORT_ASC,
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);
        $this->createTransactionLog('[SELECT] at Mahad Table');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mahad model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('super') || Yii::$app->user->can('developer') || User::findOne(Yii::$app->user->id)->mahad_id == $id) {
            $this->createTransactionLog('[SELECT] One at Mahad Table');
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        throw new ForbiddenHttpException('User Denied');
        
    }

    /**
     * Creates a new Mahad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mahad();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', 'Berhasil Menambah Ma\'had');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        $this->createTransactionLog('[INSERT] at MAHAD Table');

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Mahad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', 'Berhasil Merubah Ma\'had');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        $this->createTransactionLog('[UPDATE] at MAHAD Table');

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionForm() {
        $model = new Mahad();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Berhasil Menambah Ma\'had');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        } 
        $this->createTransactionLog('[INSERT] at MAHAD Table');

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Mahad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $this->createTransactionLog('[DELETE] at MAHAD Table');

        \Yii::$app->getSession()->setFlash('warning', 'Berhasil Menghapus Ma\'had');
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Finds the Mahad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Mahad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mahad::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
