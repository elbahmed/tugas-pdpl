<?php

namespace app\controllers;

use app\models\City;
use Yii;
use app\models\Graduated;
use app\models\GraduatedSearch;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * GraduatedController implements the CRUD actions for Graduated model.
 */
class GraduatedController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'create', 
                            'index', 
                            'update', 
                            'view', 
                            'city', 
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => ['super']
                    ],
                    [
                        'actions' => ['index', 'view',],
                        'allow' => true,
                        'roles' => ['supervisor'],
                    ],
                    [
                        'actions' => [
                            'create', 
                            'index', 
                            'update', 
                            'view', 
                            'city', 
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'city' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Graduated models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GraduatedSearch();
        $model = new Graduated();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Graduated model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
        if (
            $branch_id == Graduated::findOne(
                ['id' => $id])->branch_id || 
            Yii::$app->user->can('super') || 
            Yii::$app->user->can('supervisor')
        ) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        throw new ForbiddenHttpException("Access Denied");
    }

    /**
     * Creates a new Graduated model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Graduated();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Alumni Berhasil di Daftarkan.");
                return $this->refresh();
            }
            Yii::$app->session->setFlash('error', "Alumni Gagal di Daftarkan.");
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Graduated model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
        if (
            $branch_id == Graduated::findOne(
                ['id' => $id])->branch_id || 
            Yii::$app->user->can('super')) 
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        throw new ForbiddenHttpException("Access Denied");
    }

    public function actionCity()
    {
        if (isset($_POST['depdrop_parents'])) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $city = City::findAll(['province_id' => $parents[0]]);
                return ['output'=>$city, 'selected'=> ''];    
            }

            return ['output'=>'', 'selected'=> ''];
        }
    }

    /**
     * Deletes an existing Graduated model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Graduated model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Graduated the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Graduated::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
