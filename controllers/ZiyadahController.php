<?php

namespace app\controllers;

use app\models\Student;
use app\models\StudentRating;
use app\models\StudentRatingType;
use Yii;
use app\models\Ziyadah;
use app\traits\DepDropTrait;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZiyadahController implements the CRUD actions for Ziyadah model.
 */
class ZiyadahController extends Controller
{
    use DepDropTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'admin'],
                        'allow' => true,
                        'actions' => ['create', 'delete', 'page-depdrop']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Ziyadah models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Ziyadah::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ziyadah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ziyadah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($student_id)
    {
        $model = new Ziyadah();
        $studentModel = Student::findOne($student_id);

        $model->student_id = $studentModel->id;
        $model->teacher_id = $studentModel->studentGroup->teacher->id;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = strtotime(
                Yii::$app->request->post('created-at-year-id') .'-'.
                Yii::$app->request->post('created-at-month-id') .'-'.
                Yii::$app->request->post('created-at-date-id')
            );
            if ($model->end < $model->start) {
                $tmp = $model->end;
                $model->end = $model->start;
                $model->start = $tmp;
            }
            if ($model->save(false)) {
                \Yii::$app->getSession()->setFlash('success', 'Berhasil Menambahkan Ziyadah');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ziyadah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ziyadah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        \Yii::$app->getSession()->setFlash('danger', 'Berhasil Menghapus Ziyadah');
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Finds the Ziyadah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ziyadah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ziyadah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
