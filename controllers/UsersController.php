<?php

namespace app\controllers;

use app\controllers\users_traits\LogTrait;
use app\controllers\users_traits\RoleTrait;
use app\controllers\users_traits\UserTrait;
use app\models\users\forms\RecoveryForm;
use app\models\users\forms\RegistrationForm;
use app\traits\LogingTrait;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class UsersController extends Controller
{
    use RoleTrait;
    use UserTrait;
    use LogTrait;
    use LogingTrait;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['?'],
                        'allow' => true,
                        'actions' => ['registration', 'recovery', 'message']
                    ],
                    [
                        'roles' => ['developer'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    public function actionRegistration()
    {
        $model = new RegistrationForm();

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            
	    	return ActiveForm::validate($model);
        }
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', "$model->username has been created");
                $this->createTransactionLog('[CREATE] at User Table');

                return $this->redirect(\Yii::$app->request->referrer ?: \Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('registration', [
            'model' => $model,
        ]);
    }

    public function actionRecovery()
    {
        $model = new RecoveryForm();

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            
	    	return ActiveForm::validate($model);
        }
        
        if ($model->load(\Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            \Yii::$app->getSession()->setFlash('success','Kata sandi baru telah dikirim ke email anda.');
            return $this->redirect('/users/message');
        }
        return $this->render('recovery', [
            'model' => $model,
        ]);
    }

    public function actionMessage()
    {
        return $this->render('message');
    }
}