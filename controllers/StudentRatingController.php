<?php

namespace app\controllers;

use app\models\forms\StudentRatingForm;
use app\models\Mahad;
use app\models\Student;
use Yii;
use app\models\StudentRating;
use app\models\StudentRatingType;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StudentRatingController implements the CRUD actions for StudentRating model.
 */
class StudentRatingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'super'],
                        'allow' => true,
                        'actions' => ['create', 'delete', 'select-mahad', 'multi-create']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all StudentRating models.
     * @return mixed
     */
    public function actionMultiCreate($mahad_id)
    {
        $mahad = Mahad::findOne($mahad_id);

        return $this->render('multi-create', [
            'mahad' => $mahad,
        ]);
    }

    /**
     * Displays a single StudentRating model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSelectMahad()
    {
        return $this->render('select-mahad');
    }

    /**
     * Creates a new StudentRating model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $studentRatingType = StudentRatingType::find()->all();
        $model = new StudentRatingForm();
        $model->student_id = $id;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            if ($model->insert()) {
                \Yii::$app->getSession()->setFlash('success', 'Berhasil Menambahkan Hasil Penilaian');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('_form', [
            'model' => $model,
            'studentRatingType' => $studentRatingType,
        ]);
    }

    /**
     * Updates an existing StudentRating model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing StudentRating model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentRating model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentRating the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentRating::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
