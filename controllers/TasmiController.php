<?php

namespace app\controllers;

use app\models\Student;
use Yii;
use app\models\Tasmi;
use app\models\TasmiProgress;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TasmiController implements the CRUD actions for Tasmi model.
 */
class TasmiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'admin'],
                        'allow' => true,
                        'actions' => ['create', 'delete']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Tasmi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Tasmi::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tasmi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tasmi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($student_id)
    {
        $model = new Tasmi();
        $studentModel = Student::findOne($student_id);

        $model->student_id = $studentModel->id;
        $model->teacher_id = $studentModel->studentGroup->teacher->id;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = strtotime(
                Yii::$app->request->post('created-at-year-id') .'-'.
                Yii::$app->request->post('created-at-month-id') .'-'.
                Yii::$app->request->post('created-at-date-id')
            );
            if ($model->save(false)) {
                $tasmiProgres = TasmiProgress::find()->where(['<', 'id', $model->tasmi_progress_id])->all();
                foreach ($tasmiProgres as $x) {
                    $tasmi = Tasmi::findOne(['tasmi_progress_id' => $x['id'], 'student_id' => $model->student_id]);
                    if ($tasmi == null) {
                        $olderTasmi = new Tasmi();
                        $olderTasmi->created_at = $model->created_at;
                        $olderTasmi->tasmi_progress_id = $x['id'];
                        $olderTasmi->teacher_id = $model->teacher_id;
                        $olderTasmi->student_id = $model->student_id;
                        $olderTasmi->video_link = $model->video_link;

                        if ($model->comment_id != null) {
                            $olderTasmi->comment_id = $model->comment_id;
                        }

                        $olderTasmi->save(false);
                    }
                }

                \Yii::$app->getSession()->setFlash('success', 'Berhasil Menambahkan Hasil Tasmi');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tasmi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tasmi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tasmi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasmi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasmi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
