<?php

namespace app\controllers;

use app\models\PassReason;
use Yii;
use app\models\Student;
use app\models\StudentGroup;
use app\models\users\User;
use app\traits\DepDropTrait;
use app\traits\LogingTrait;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    use DepDropTrait;
    use LogingTrait;
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['developer', 'admin'],
                        'allow' => true,
                        'actions' => ['view', 'delete', 'student-group-create', 'update', 'mahad-form', 'graduated-form', 'list-juz', 'regency-depdrop', 'student-group-delete']
                    ],
                    [
                        'roles' => ['super'],
                        'allow' => true,
                        'actions' => ['view', 'list-juz']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'student-group-create' => ['POST'],
                    'update' => ['POST'],
                    'mahad-form' => ['POST'],
                    'graduated-form' => ['POST'],
                    'list-juz' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = Student::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionStudentGroupCreate($mahad_id)
    {
        $model = new StudentGroup();
        $model->mahad_id = $mahad_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/mahad/view', 'id' => $mahad_id]);
        }

        return $this->renderAjax('_student-group-create', [
            'model' => $model,
        ]);
    }
    
    public function actionStudentGroupDelete($id)
    {
        if (($model = StudentGroup::findOne($id)) !== null) {
            $model->delete();
            $this->createTransactionLog('[DELETE at Student Group Table by Flag');

            \Yii::$app->getSession()->setFlash('success', 'Berhasil Menghapus Halaqoh');
            return $this->redirect('/mahad/view?id='.$model->mahad_id);
        }
    }

    /**
     * Displays a single Student model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('super') || Yii::$app->user->can('developer') || User::findOne(Yii::$app->user->id)->mahad_id == $model->mahad_id) {
            $this->createTransactionLog('[SELECT] at Student Table');

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        throw new ForbiddenHttpException('User Denied');
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dobYear    = date('Y', $model->dob);
        $dobMonth   = sprintf('%01d', date('m', $model->dob));
        $dobDate    = date('d', $model->dob);

        $model->doe = date('l, d F Y', $model->doe);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->createTransactionLog('[UPDATE] at Student Table');
            \Yii::$app->getSession()->setFlash('success', 'Berhasil Merubah Data Santri');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->render('update', [
            'model' => $model,
            'dobYear' => $dobYear,
            'dobMonth' => $dobMonth,
            'dobDate' => $dobDate,
        ]);
    }

    public function actionListJuz($id) 
    {
        $model = Student::findOne($id);
        
        return $this->renderAjax('list-juz', [
            'result' => $model->listZiyadah()
        ]);
    }

    public function actionMahadForm($id) 
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->id = sprintf('%s%s', $model->mahad_id, substr($model->id, 7));
            if ($model->save(false)) {
                $this->createTransactionLog('[UPDATE] at Student Table');

                \Yii::$app->getSession()->setFlash('success', 'Berhasil Memindahkan Santri.');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('_mahad-form', [
            'model' => $model,
        ]);
    }

    public function actionGraduatedForm($id)
    {
        $model = new PassReason();
        $model->student_id = $id;
        
        if ($model->load(Yii::$app->request->post())) {
            $student = Student::findOne($id);
            $student->student_flag_id = 3; 
            $student->save(false);
            if ($model->save()) {
                $this->createTransactionLog('[UPDATE] at Student Table');

                \Yii::$app->getSession()->setFlash('success', 'Santri Telah Menjadi Alumni.');
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        } 

        return $this->renderAjax('_graduated-form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->student_flag_id = 4;
        $this->createTransactionLog('[DELETE at Student Table by Flag');
        $model->save(false);

        \Yii::$app->getSession()->setFlash('success', 'Berhasil Menghapus Santri');
        return $this->redirect('/mahad/view?id='.$model->mahad_id);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
