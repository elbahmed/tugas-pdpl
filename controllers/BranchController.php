<?php

namespace app\controllers;

use Yii;
use app\models\Branch;
use app\models\BranchSearch;
use app\models\City;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * BranchController implements the CRUD actions for Branch model.
 */
class BranchController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'index', 'update', 'view', 'city', 'delete'],
                        'allow' => true,
                        'roles' => ['super']
                    ],
                    [
                        'actions' => ['index', 'view',],
                        'allow' => true,
                        'roles' => ['supervisor'],
                    ],
                    [
                        'actions' => ['index', 'update', 'view',],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'city' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Branch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
        if ( Yii::$app->user->can('admin')) {
            $this->redirect("branch/view?id=$branch_id");
        }
        $searchModel = new BranchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $city = ArrayHelper::map(City::find()->all(), 'id', 'name');
        
        if (isset(Yii::$app->request->get('BranchSearch')['province_id'])) {
            $request = Yii::$app->request;
            $parent = $request->get('BranchSearch')['province_id'];

            if(!empty($parent)) {
                $city = ArrayHelper::map(City::findAll(['province_id' => $parent]), 'id', 'name');
                
                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'city' => $city,
                ]);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'city' => $city,
        ]);
    }

    /**
     * Displays a single Branch model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
        if ($branch_id == $id || Yii::$app->user->can('super') || Yii::$app->user->can('supervisor')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        throw new ForbiddenHttpException("Access Denied");
        
    }

    /**
     * Creates a new Branch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Branch();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', "Ma'had Berhasil di Daftarkan");
                    return $this->refresh();
                }
                Yii::$app->session->setFlash('error', "Ma'had Gagal di Daftarkan.");

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Branch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
        if ($branch_id == $id || Yii::$app->user->can('super')) {
                $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        throw new ForbiddenHttpException("Access Denied");


        
    }

    public function actionCity()
    {
        if (isset($_POST['depdrop_parents'])) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $city = City::findAll(['province_id' => $parents[0]]);
                return ['output'=>$city, 'selected'=> ''];    
            }

            return ['output'=>'', 'selected'=> ''];
        }
    }

    /**
     * Deletes an existing Branch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Branch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Branch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Branch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
