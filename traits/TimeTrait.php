<?php

namespace app\traits;

use app\models\CalendarMonth;
use app\models\CalendarDay;

trait TimeTrait 
{
    public function setTime($time) 
    {
        return date('U', $time);
    }

    public function getTime($time) 
    {
        return date('D d M, H:i', $time);
    }

    public function getDate($date) 
    {
        return date('l, d M Y', $date);
    }

    public function setDate($date) 
    {
        return date('Y-M-d', $date);
    }

    public static function translateDate($n) 
    {
        $day    = ucfirst(strtolower(CalendarDay::findOne(date('N', $n))->day));
        $date   = date('d', $n);
        $month  = ucfirst(strtolower(CalendarMonth::findOne(date('n', $n))->month));
        $year   = date('Y', $n); 

        return "$day, $date $month $year";
    }
}