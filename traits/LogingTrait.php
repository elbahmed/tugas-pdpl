<?php

namespace app\traits;

use app\models\users\Log;

trait LogingTrait
{
    public function createTransactionLog($desc)
    {
        $model = new Log();

        if(!\Yii::$app->user->isGuest) {
            $model->user = \Yii::$app->user->identity->username;
        } 

        else {
            $model->user = '[DANGER]Anonymous';
        }

        $model->time = time();
        $model->user_ip = \Yii::$app->request->userIP;
        $model->type_id = 2;
        $model->description = $desc;
        $model->save();
    }

    public function createLogInLog($user, $desc)
    {
        $model = new Log();
        $model->user = $user;
        $model->time = time();
        $model->user_ip = \Yii::$app->request->userIP;
        $model->type_id = 3;
        $model->description = $desc;
        $model->save();
    }
}