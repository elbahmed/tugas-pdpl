<?php
namespace app\traits;

Trait GeneratorTrait 
{
    public function generateRandomAscii($length)
    {
        return \Yii::$app->security->generateRandomKey($length);
    }
    
    public function generateRandomString($length)
    {
        return \Yii::$app->security->generateRandomString($length);
    }

    public function hashPassword($password, $salt)
    {
        $hash = $salt . $password;
        $hashed = \Yii::$app->security->generatePasswordHash($hash);

        return $hashed;
    }

    public function generateRandomAlphaNum($length)
    {
        $sets = ['abcdefghjkmnpqrstuvwxyz', 'ABCDEFGHJKMNPQRSTUVWXYZ', '23456789'];
        $text = $this->randomGenerator($sets, $length);
    
        return $text;
    }

    public function generateRandomNum($length)
    {
        $sets = ['123456789', '123456789', '123456789'];
        $num = $this->randomGenerator($sets, $length);

        return (int)$num;
    }

    protected function randomGenerator($sets, $length) 
    {
        $all = '';
        $text = '';

        foreach ($sets as $set) {
            $text .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        
        $all = str_split($all);

        for ($i = 0; $i < $length - count($sets); $i++) {
            $text .= $all[array_rand($all)];
        }

        $text = str_shuffle($text);

        return $text;
    }
}