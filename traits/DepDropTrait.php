<?php
namespace app\traits;

use app\models\QuranPage;
use app\models\Regency;
use yii\helpers\Json;

Trait DepDropTrait 
{
    public function actionRegencyDepdrop()
    {
        $city = [];

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $city = Regency::find()->where(['province_id'=>$parents[0]])->select(['id','name'])->asArray()->all();

                return Json::encode(['output'=> $city, 'selected'=> '']);    
            }

            return Json::encode(['output'=>'', 'selected'=> '']);
        }
    }

    public function actionPageDepdrop()
    {
        $page = [];

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {
                $page = QuranPage::find()->where(['quran_juz_id' => $parents[0]])->select(['id' , 'page AS name'])->asArray()->all();

                return Json::encode(['output'=> $page, 'selected'=> '']);    
            }

            return Json::encode(['output'=>'', 'selected'=> '']);
        }
    }
}