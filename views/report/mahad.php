<?php

use app\models\MahadRating;
use app\models\MahadRatingType;
use app\models\Student;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\Pjax;

use function PHPSTORM_META\map;

$this->title = 'Laporan Bulanan | Al Askar';

?>
<div class="monthly">
    <h1>Laporan Penilaian Ma'had</h1>

    <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"></div>
                    <?php Pjax::begin(['id' => 'report-mahad']) ?>
                        <div class="box-body">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'class' => 'table-responsive'
                                ],
                                'tableOptions' => [
                                    'class' => 'table table-striped'
                                ],
                                'summary' => false,
                                
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    [
                                        'label' => 'Nama',
                                        'attribute' => 'name',
                                    ],
                                    [
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'contentOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'label' => 'Kebersihan',
                                        'value' => function($dataProvider) {
                                            $rating = 0;
                                            $mahadRating = MahadRating::find()
                                            ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                            ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
                                            ->where(['mahad_rating_type_id' => 1, 'mahad_id' => $dataProvider->id])
                                            ->orderBy(['created_at' => SORT_DESC])
                                            ->asArray()
                                            ->one();
                                            if ($mahadRating != null) {
                                                $rating = $rating + $mahadRating['score'];
                                            }
                                            return $rating . '%';
                                        }
                                    ],
                                    [
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'contentOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'label' => 'Administrasi',
                                        'value' => function($dataProvider) {
                                            $rating = 0;
                                            $mahadRating = MahadRating::find()
                                            ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                            ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
                                            ->where(['mahad_rating_type_id' => 2, 'mahad_id' => $dataProvider->id])
                                            ->orderBy(['created_at' => SORT_DESC])
                                            ->asArray()
                                            ->one();
                                            if ($mahadRating != null) {
                                                $rating = $rating + $mahadRating['score'];
                                            }
                                            return $rating . '%';
                                        }
                                    ],
                                    [
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'contentOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'label' => 'Keaktifan',
                                        'value' => function($dataProvider) {
                                            $rating = 0;
                                            $mahadRating = MahadRating::find()
                                            ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                            ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
                                            ->where(['mahad_rating_type_id' => 3, 'mahad_id' => $dataProvider->id])
                                            ->orderBy(['created_at' => SORT_DESC])
                                            ->asArray()
                                            ->one();
                                            if ($mahadRating != null) {
                                                $rating = $rating + $mahadRating['score'];
                                            }
                                            return $rating . '%';
                                        }
                                    ],
                                    [
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'contentOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'label' => 'Pengabdian',
                                        'value' => function($dataProvider) {
                                            $rating = 0;
                                            $mahadRating = MahadRating::find()
                                            ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                            ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
                                            ->where(['mahad_rating_type_id' => 3, 'mahad_id' => $dataProvider->id])
                                            ->orderBy(['created_at' => SORT_DESC])
                                            ->asArray()
                                            ->one();
                                            if ($mahadRating != null) {
                                                $rating = $rating + $mahadRating['score'];
                                            }
                                            return $rating . '%';
                                        }
                                    ],
                                    [
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'contentOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        'label' => 'Ubudiyah',
                                        'value' => function($dataProvider) {
                                            $rating = 0;
                                            $mahadRating = MahadRating::find()
                                            ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                            ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
                                            ->where(['mahad_rating_type_id' => 4, 'mahad_id' => $dataProvider->id])
                                            ->orderBy(['created_at' => SORT_DESC])
                                            ->asArray()
                                            ->one();
                                            if ($mahadRating != null) {
                                                $rating = $rating + $mahadRating['score'];
                                            }
                                            return $rating . '%';
                                        }
                                    ],
                                    [
                                        'label' => 'Akreditasi',
                                        'value' => function($dataProvider) {
                                            $mahadRatingType = MahadRatingType::find()->all();
                                            $mahadQuality = 0;
                                            $i = 0;

                                            foreach($mahadRatingType as $x) {
                                                $mahadRating = MahadRating::find()
                                                ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                                ->where(['mahad_rating_type_id' => $x['id'], 'mahad_id' => $dataProvider->id])
                                                ->all();
                                            
                                                if ($mahadRating != null) {
                                                    foreach($mahadRating as $y) {
                                                        $mahadQuality = $mahadQuality + $y['score']['score'];
                                                        $i++;
                                                    }
                                                }
                                                
                                            }
                                            if ($mahadQuality != 0) {
                                                $mahadQuality = round($mahadQuality / $i);
                                            }

                                            return $mahadQuality . '%';
                                        }
                                    ]
                                ],
                            ]) ?>
                        </div>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
</div>