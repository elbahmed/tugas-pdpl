<?php

use app\models\StudentExam;
use app\models\Tasmi;
use app\models\TasmiProgress;
use app\models\Ziyadah;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ListView;

?>
<div class="table-student">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'label' => 'Santri',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->name));
                }
                
            ],
            [
                'label' => "Musyrif",
                'attribute' => 'studentGroup.teacher.name',
            ],
            [
                'label' => "Angkatan",
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'attribute' => 'studentGraduate.year',
            ],
            [
                'format' => 'raw',
                'label' => 'Level',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($model) {
                    if ($model->doe == 0) {
                        return '-';
                    }
                    $level = round((time() - $model->doe) / (3600*24*365.242199));
                    
                    return 'Ke-'. $level;
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Tasmi',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $tasmi = Tasmi::find()->where(['student_id' => $dataProvider->id])->orderBy('tasmi_progress_id DESC')->one();
                    if ($tasmi != null) {
                        return TasmiProgress::findOne($tasmi['tasmi_progress_id'])->name;
                    }

                    return 'Belum Pernah Tasmi';
                }
            ],
            [
                'label' => 'Hafalan',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    return $dataProvider->totalZiyadah();
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => 'Ujian',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $examTahfiz = StudentExam::find()->join('LEFT OUTER JOIN', 'score', 'score.id = student_exam.score_id')->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam_type.id = student_exam.student_exam_type_id')->join('LEFT OUTER JOIN', 'student_exam_type_category', 'student_exam_type_category.id = student_exam_type.student_exam_type_category_id')->where(['student_id' => $dataProvider->id, 'student_exam_type.student_exam_type_category_id' => 2])->asArray()->orderBy('student_exam.created_at ASC')->all();
                    $examTahsin = StudentExam::find()->join('LEFT OUTER JOIN', 'score', 'score.id = student_exam.score_id')->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam_type.id = student_exam.student_exam_type_id')->join('LEFT OUTER JOIN', 'student_exam_type_category', 'student_exam_type_category.id = student_exam_type.student_exam_type_category_id')->where(['student_id' => $dataProvider->id, 'student_exam_type.student_exam_type_category_id' => 1])->asArray()->orderBy('student_exam.created_at ASC')->all();
                    return 'Tahfiz '. count($examTahfiz) .', Tahsin '. count($examTahsin);
                }
            ],
        ],
    ]); ?>
</div>

<?php

Modal::begin([
    'id' => 'modal-student',
    'size' => 'modal-md',
]);
echo "<div id='role-content'></div>";
Modal::end();

$this->registerJs("
    $(function(){
        $('.addStudentRatingBtn').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.addStudentExamBtn').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.addZiyadahBtn').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.addTasmiBtn').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
");
?>