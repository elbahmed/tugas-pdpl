<?php

use app\models\Branch;
use app\models\Page;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\QuranReport */

$this->title = 'Input Ziyadah';
$this->params['breadcrumbs'][] = ['label' => 'Quran Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quran-report-create">

    <h1><?= Html::encode($this->title .' : '. $model->student->name) ?></h1>

    <div class="quran-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'student_id')->hiddenInput()->label(false)?>

    <div class="form-group">
        <label for="start_juz">Dari Juz</label>
        <?= Select2::widget([
            'name' => 'start_juz',
            'data' => ArrayHelper::map(Page::find()->groupBy('juz')->all(),'juz', 'juz'),
            'options' => [
                'placeholder' => 'Dari Juz...',
                'class' => 'form-control',
                'id' => 'start_juz',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])?>
    </div>

    <?= $form->field($model, 'start_page')->widget(DepDrop::className(), [
        'options'=>['id'=>'start_page'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['start_juz'],
            'placeholder' => 'Dari Halaman...',
            'url' => Url::to(['/ziyadah/startpage']),
        ],
    ])?>

    <div class="form-group">
        <label for="end_juz">Sampai Juz</label>
        <?= Select2::widget([
            'name' => 'end_juz',
            'data' =>  ArrayHelper::map(Page::find()->groupBy('juz')->all(),'juz', 'juz'),
            'options' => [
                'placeholder' => 'Sampai Juz...',
                'class' => 'form-control',
                'id' => 'end_juz',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])?>
    </div>

    <?= $form->field($model, 'end_page')->widget(DepDrop::className(), [
        'options'=>['id'=>'end_page'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['end_juz'],
            'placeholder' => 'Sampai Halaman...',
            'url' => Url::to(['/ziyadah/endpage']),
        ],
    ])?>

    <?= $form->field($model, 'date')->widget(DatePicker::className(), [
        'name' => 'date',
        'value' => '2010-10-21',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
        ],
    ])?>

    <?= $form->field($model, 'desc')->textarea()?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
