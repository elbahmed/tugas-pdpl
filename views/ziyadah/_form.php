<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\QuranJuz;
use app\models\QuranPage;
use app\models\Score;
use app\models\StudentExamType;
use app\models\Teacher;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentExam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ziyadah-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group"><label>Santri</label> <p class="form-control" readonly><?= $model->student->name ?></p></div>
        </div>
        <div class="col-md-6">
            <div class="form-group"><label>Musyrif</label> <p class="form-control" readonly><?= $model->teacher->name ?></p></div>
        </div>
    </div>

    <!--CREATED AT-->
    <div class="form-group">
        <label>Tanggal</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'created-at-date-id',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'created-at-date-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'created-at-month-id',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'created-at-month-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'created-at-year-id',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['between', 'year', 2015, date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'created-at-year-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>
    
    <!--START-->
    <fieldset>
        <legend>Dari</legend>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Juz</label>
                    <?= Select2::widget([
                        'name' => 'quran-juz-start',
                        'data' => ArrayHelper::map(QuranJuz::find()->all(), 'id', 'juz'),
                        'options' => [
                            'placeholder' => 'Juz...',
                            'id' => 'quran-juz-start',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <label>Halaman</label>
                    <?= $form->field($model, 'start')->widget(DepDrop::className(),[
                        'options' => [
                            'placeholder' => 'Halaman...',
                            'id' => 'start',
                        ],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['quran-juz-start'],
                            'url' => Url::to(['page-depdrop'])
                        ]
                    ])->label(false) ?>
                </div>
            </div>
        </div>
    </fieldset>

    <!--END-->
    <fieldset>
        <legend>Sampai</legend>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Juz</label>
                    <?= Select2::widget([
                        'name' => 'quran-juz-end',
                        'data' => ArrayHelper::map(QuranJuz::find()->all(), 'id', 'juz'),
                        'options' => [
                            'placeholder' => 'Juz...',
                            'id' => 'quran-juz-end',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <label>Halaman</label>
                    <?= $form->field($model, 'end')->widget(DepDrop::className(),[
                        'options' => [
                            'placeholder' => 'Halaman...',
                            'id' => 'end',
                        ],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['quran-juz-end'],
                            'url' => Url::to(['page-depdrop'])
                        ]
                    ])->label(false) ?>
                </div>
            </div>
        </div>
    </fieldset>

    <?= $form->field($model, 'comment')->textarea()  ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
