<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ziyadah */

?>
<div class="ziyadah-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
