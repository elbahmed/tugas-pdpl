<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ziyadah */

$this->title = 'Update Ziyadah: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ziyadahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ziyadah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
