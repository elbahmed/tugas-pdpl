<?php

use app\models\Branch;
use app\models\Province;
use app\models\Tasmi;
use app\models\User;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Graduated */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="graduated-form card">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob')->widget(DatePicker::className(), [
        'name' => 'dob',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
        ],
    ])?>

    <?= $form->field($model, 'province_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Province::find()->all(),'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Provinsi ...',
            'id' => 'province_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ])->label('Provinsi') ?>

        <?= $form->field($model, 'city_id')->widget(DepDrop::classname(), [
            'options'=>['id'=>'city_id'],
            'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
                'depends' => ['province_id'],
                'placeholder' => 'Pilih Kota/Kab...',
                'url' => Url::to(['/graduated/city']),
            ],
        ])->label('Kota') ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6, 'style' => 'resize: none']) ?>

    <?= $form->field($model, 'branch_id')->widget(Select2::className(), [
        'data' => !Yii::$app->user->can('super') ? (
            ArrayHelper::map(Branch::find()->where(['id' => User::findOne(Yii::$app->user->getId())->branch_id])->all(),'id', 'name')
        ) : (
            ArrayHelper::map(Branch::find()->all(),'id', 'name')
        ),
        'options' => [
            'placeholder' => "Pilih Ma'had...",
            'id' => 'branch_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'ziyadah')->textInput() ?>
    <?= $form->field($model, 'tasmi_id')->widget(Select2::className(), [
        'name' => 'tasmi_id',
        'data' => ArrayHelper::map(Tasmi::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Pilih Progres Tasmi ...', 'id' => 'tasmi'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'desc')->widget(Select2::className(), [
        'data' => [
            'Kuliah' => 'Kuliah', 
            "Masalah dengan Ma'had" => "Masalah dengan Ma'had", 
            'Keinginan Wali Murid' => 'Keinginan Wali Murid', 
            'Tidak Mencapai Target' => 'Tidak Mencapai Target', 
            'Sudah Lulus' => 'Sudah Lulus'
        ],
        'options' => [
            'placeholder' => 'Pilih Alasan Lulus ...',
            'id' => 'desc',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ])?>

    <?= $form->field($model, 'doe')->widget(DatePicker::className(), [
        'name' => 'dob',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
        ],
    ])?>

    <?= $form->field($model, 'dog')->widget(DatePicker::className(), [
        'name' => 'dog',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
        ],
    ])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
