<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Graduated */

$this->title = 'Create Alumni';
$this->params['breadcrumbs'][] = ['label' => 'Graduateds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="graduated-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
