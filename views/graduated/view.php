<?php

use app\models\Tasmi;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Graduated */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Alumni', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="graduated-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'branch.name' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Ma\'had',
                'attribute' => 'branch.name',
            ],
            'city.name' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Kota',
                'attribute' => 'city.name',
            ],
            'province.name' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Provinsi',
                'attribute' => 'province.name',
            ],
            'name',
            'address:ntext',
            'ziyadah' => [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'ziyadah',
                'value' => function($model) {
                    return $model->ziyadah ;
                },
            ],
            'tasmi_id' => [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'tasmi_id',
                'value' => function($model) {
                    $tasmi = Tasmi::findOne($model->tasmi_id);
                    if ($tasmi->id == 6) {
                        return 'Selesai';
                    }
                    return $tasmi['name'];
                },
            ],
            'desc',
            'dob' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Umur',
                'value' => function ($model) {
                    $date = new DateTime($model['dob']);
                    $now = new DateTime();
                    return $now->diff($date)->y . ' tahun';
                }
            ],
            'doe' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Tanggal Laporan',
                'attribute' => 'doe',
                'value' =>  function($data) {
                                return date('d M Y', strtotime($data->__get('doe')));
                }
            ],
            'dog' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Tanggal Laporan',
                'attribute' => 'dog',
                'value' =>  function($data) {
                                return date('d M Y', strtotime($data->__get('dog')));
                }
            ],
        ],
    ]) ?>

</div>
