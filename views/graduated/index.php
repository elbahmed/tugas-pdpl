<?php

use app\models\Branch;
use app\models\Tasmi;
use app\models\User;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GraduatedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumni';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="graduated-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alumni', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'id' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'id',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'branch.name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'branch.name',
                'label' => "Ma'had",
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'branch_id',
                    'data' => !Yii::$app->user->can('super') ? (
                        ArrayHelper::map(Branch::find()->where(['id' => User::findOne(Yii::$app->user->getId())->branch_id])->all(),'id', 'name')
                    ) : (
                        ArrayHelper::map(Branch::find()->all(),'id', 'name')
                    ),
                    'options' => ['placeholder' => "Ma'had..."],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            //'city_id',
            //'province_id',
            'name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'name',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            //'address:ntext',
            'ziyadah' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'ziyadah',
                'value' => function($model) {
                    return $model->ziyadah;
                },
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'tasmi_id' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'tasmi_id',
                'value' => function($model) {
                    $tasmi = Tasmi::findOne($model->tasmi_id);
                    if ($tasmi->id == 6) {
                        return 'Selesai';
                    }
                    return $tasmi['name'];
                },
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'desc' => [

                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'desc',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            //'dob',
            'doe' => [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Tanggal Muqim',
                'attribute' => 'doe',
                'value' =>  function($model) {
                                return date('d M Y', strtotime($model->__get('doe')));
                },          
                'filter' => DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute' => 'doe',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'options' => ['class' => 'form-control'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                        
                    ],
                ]),
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'dog'=> [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Tanggal Lulus',
                'attribute' => 'doe',
                'value' =>  function($model) {
                                return date('d M Y', strtotime($model->__get('dog')));
                },          
                'filter' => DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute' => 'dog',
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'options' => ['class' => 'form-control'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                        
                    ],
                ]),
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],

            ['class' => 'kartik\grid\ActionColumn'],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], 
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'beforeHeader'=>[
            [
                'columns'=>[
                    ['content'=> 'List Santri', 'options'=>['colspan'=>12, 'class'=>'text-center']],
                ], 
                'options'=>['class'=>'skip-export'] 
            ]
        ],
        'exportConfig' => [
            GridView::PDF => [
                'label' => 'Save as PDF',
                'pdfConfig' => [
                    'methods' => [
                        'SetTitle' => 'Santri - Alaskar.com',
                        'SetSubject' => '',
                        'SetHeader' => ['Ma\'had Al Askar||Generated On: ' . date("r")],
                        'SetFooter' => ['|Page {PAGENO}|'],
                    ]
                ],
            ],
            GridView::EXCEL => ['label' => 'Save as EXCEL'], 
            GridView::CSV => ['label' => 'Save as CSV'], 
        ],
          
        'toolbar' =>  [
            '{export}', 
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'floatHeader' => true,
        'panel' => [
            'type' => GridView::TYPE_INFO,
            'heading' => 'Santri',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
