<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\Education;
use app\models\Mahad;
use app\models\TeacherPosition;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Teacher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label>Tanggal Lahir</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'dob-date',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'dob-date',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'dob-month',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'dob-month',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'dob-year',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['<=', 'year', date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'dob-year',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>      

    <?= $form->field($model, 'total_tahfiz')->textInput() ?>
    
    <?= $form->field($model, 'mahad_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Mahad::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Ma\'had...',
            'id' => 'mahad-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'education_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Education::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Pendidikan Terakhir...',
            'id' => 'education-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'teacher_position_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(TeacherPosition::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Jabatan...',
            'id' => 'teacher-position-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <div class="form-group">
        <label>Tanggal Muqim</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'doe-date',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'doe-date-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'doe-month',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'doe-month',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'doe-year',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['<=', 'year', date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'doe-year',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
