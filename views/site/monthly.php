<?php

use app\models\Branch;
use app\models\Scholarship;
use app\models\Student;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\Tasmi;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Bulanan Santri';
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
                        'query' => Student::find()->orderBy(['branch_id' => SORT_ASC]),
                        'sort' => false,
                        'pagination' => [
                            'pageSize' => 5,
                        ],
]);
?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'id' => [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'ID',
                'attribute' => 'id',
            ],
            'name' => [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Nama',
                'attribute' => 'name',
            ],
            'branch.name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'branch.name',
                'label' => "Ma'had",
            ],
            'doe'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Tanggal Muqim',
                'attribute' => 'doe',
                'value' =>  function($model) {
                                return date('d M Y', strtotime($model->__get('doe')));
                },
            ],
            'ziyadah'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Hafalan',
                'value' =>  function($model) {
                    $id = $model->id;
                    $month = date('m');
                    $query = Yii::$app->db->createCommand(" SELECT COUNT(DISTINCT page) AS count_page, COUNT(DISTINCT juz) AS count_juz, juz  FROM `ziyadah` LEFT OUTER JOIN `page` p ON p.id 
                                                                            BETWEEN 
                                                                            CASE
                                                                                WHEN start_page < end_page THEN start_page
                                                                                ELSE end_page
                                                                            END
                                                                            AND
                                                                            CASE
                                                                                WHEN start_page < end_page THEN end_page
                                                                                ELSE start_page
                                                                            END
                                                                            WHERE student_id = $id AND MONTH(date) = $month
                                                                            GROUP BY juz")->queryAll();
                    $ziyadah_juz = 0;
                    $ziyadah_page = 0;
                    for ($i = 0; $i < count($query); $i++) {
                        if ($query[$i]['juz'] == 30) {
                            if ($query[$i]['count_page'] == 23) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    
                        else if ($query[$i]['juz'] == 1) {
                            if ($query[$i]['count_page'] == 21) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    
                        else {
                            if ($query[$i]['count_page'] == 20) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    }
                    return $ziyadah_juz . ' Juz ' . $ziyadah_page . ' Halaman';
                },
            ],
            'list_juz'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Daftar Juz',
                'value' =>  function($model) {
                    $id = $model->id;
                    $month = date('m');
                    $query = Yii::$app->db->createCommand(" SELECT juz, COUNT(DISTINCT juz)  
                                                                        FROM `ziyadah` 
                                                                        LEFT OUTER JOIN `page` p ON p.id 
                                                                        BETWEEN
                                                                        CASE
                                                                            WHEN start_page < end_page THEN start_page
                                                                            ELSE end_page
                                                                        END
                                                                        AND
                                                                        CASE
                                                                            WHEN start_page < end_page THEN end_page
                                                                            ELSE start_page
                                                                        END
                                                                        WHERE student_id = $id AND MONTH(date) = $month
                                                                        GROUP BY juz")->queryAll();

                    if (count($query) == 0) {
                        return 'Belum Memiliki Hafalan';
                    }

                    for($i = 0; $i < count($query); $i++) {
                        $juz[$i] = 'juz '. $query[$i]['juz'];
                    }
                    if (count($juz) == 30) {
                        return 'Khatam';
                    }
                    $juz = implode(", ",$juz);
                    return  $juz;
                },
            ],
            'target' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'target',
                'label' => HtmlPurifier::process("Target Bulanan"),
                'value' => function($model) {
                    $id = $model->id;
                    $month = date('m');
                    $query = Yii::$app->db->createCommand(" SELECT COUNT(DISTINCT page) AS count_page FROM `ziyadah` LEFT OUTER JOIN `page` p ON p.id 
                                                                            BETWEEN 
                                                                            CASE
                                                                                WHEN start_page < end_page THEN start_page
                                                                                ELSE end_page
                                                                            END
                                                                            AND
                                                                            CASE
                                                                                WHEN start_page < end_page THEN end_page
                                                                                ELSE start_page
                                                                            END
                                                                            WHERE student_id = $id AND MONTH(date) = $month
                                                                            GROUP BY student_id")->queryAll();
                    if (empty($query)) {
                        return 'Tidak Tercapai';
                    }
                    
                    if ($query[0]['count_page'] >= Scholarship::findOne(['id' => $model->status_id])['target']) {
                        return 'Tercapai';
                    }
                    return 'Tidak Tercapai';
                },
            ],
            'tasmi_id' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'tasmi_id',
                'label' => HtmlPurifier::process("Progress Tasmi Akbar"),
                'value' => function($model) {
                    $tasmi = Tasmi::findOne($model->tasmi_id);
                    if ($tasmi->id == 6) {
                        return 'Selesai';
                    }
                    return $tasmi['name'];
                },
            ],
        ],
        'exportConfig' => [
            GridView::PDF => [
                'label' => 'Save as PDF',
            ],
            GridView::EXCEL => ['label' => 'Save as EXCEL'], 
            GridView::CSV => ['label' => 'Save as CSV'], 
        ],
          
        'toolbar' =>  [
            '{export}', 
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'floatHeader' => false,
        'panel' => [
            'type' => GridView::TYPE_INFO,
            'heading' => 'Laporan Santri Bulan '.date('F'),
        ],
            
    ]);?>

</div>