<?php

use yii\helpers\Html;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="user-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'initialPassword')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'confirmPassword')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'superadmin')->widget(Select2::className(), [
            'data' => [0, 1, 2],
            'options' => [
                'placeholder' => 'Pilih Superadmin...',
                'id' => 'superadmin'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <?= $form->field($model, 'branch_id')->widget(DepDrop::className(), [
            'options'=>['id'=>'city_id'],
            'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
                'depends' => ['superadmin'],
                'placeholder' => "Pilih Ma'had...",
                'url' => Url::to(['/site/branch']),
            ],
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>