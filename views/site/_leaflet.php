<?php

use app\models\Mahad;
use app\models\Province;
use app\models\Student;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$province = Province::find()->where(['!=', 'total_mahad', 0])->all();

$center = new dosamigos\leaflet\types\LatLng(['lat' => -5.506640, 'lng' => 109.451775]);

$leaflet = new \dosamigos\leaflet\LeafLet([
    'name' => 'propertyMap',
    'center' => $center, // set the center,
    'zoom' => 6,
]);

foreach ($province as $x) {
    $name = $x['name'];
    $totalMahad = $x['total_mahad'];
    $popup = "<div class ='box no-border'>
        <div class='box-header with-border'><h5 class='box-title'>$name<h5></div>
        ". GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => Mahad::find()->where(['province_id' => $x['id']]),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false
        ]),
        'summary' => false,
        'tableOptions' => [
            'class' => 'table table-condensed'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Nama Ma\'had',
                'value' => function($model) {
                    return ucwords(strtolower($model->name));
                },
            ],
            [
                'format' => 'raw',
                'label' => 'Santri',
                'value' => function($model) {
                    if (Student::find()->where(['mahad_id' => $model->id, 'student_flag_id' => 1])->count() == 0) {
                        return '-';
                    }
                    return Student::find()->where(['mahad_id' => $model->id, 'student_flag_id' => 1])->count() .' <i class="fas fa-sm fa-child"></i>';
                },
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
        ]
    ]) ."</div>";
    $popup  = preg_replace("/[\r\n]*/","",$popup);
    $latlng = new dosamigos\leaflet\types\LatLng(['lat' => $x['latitude'], 'lng' => $x['longitude']]);
    $marker = new \dosamigos\leaflet\layers\Circle([
        'radius' => 20000,
        'latLng' => $latlng, 
        'popupContent' => $popup
    ]);
    $leaflet->addLayer($marker);
}

$tileLayer = new \dosamigos\leaflet\layers\TileLayer([
   'urlTemplate' => 'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png',
]);
$leaflet->addLayer($tileLayer);  // add the tile layer
$leaflet->appendJs('propertyMap.scrollWheelZoom.disable();'); // disable zoom

echo \dosamigos\leaflet\widgets\Map::widget([
    'leafLet' => $leaflet,
]);
// echo $leaflet->widget();

?>