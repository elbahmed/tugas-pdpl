<?php

    use app\models\Mahad;
    use app\models\Student;
    use app\models\Teacher;

    $countMahad     = Mahad::find()->count();
    $countTeacher   = Teacher::find()->count();
    $countStudent   = Student::find()->where(['student_flag_id' => 1])->count();
    $countGraduated = Student::find()
        ->join('LEFT OUTER JOIN', 'pass_reason', 'pass_reason.student_id = student.id')
        ->join(
            'LEFT OUTER JOIN', 
            'pass_reason_description', 
            'pass_reason_description.id = pass_reason.pass_reason_description_id'
        )
        ->where([
            'student.student_flag_id' => 3,
            'pass_reason_description.pass_reason_description_flag_id' => 1
        ])
        ->count();
?>
<div class="information-box">
    <div class="row">

        <!--COUNT STUDENT-->
        <div class="col-md-3">
            <div class="info-box no-margin">
                <span class="info-box-icon bg-aqua"><i class="fas fa-user-graduate" style="-webkit-text-stroke: 1px rgba(0, 0, 0, 0.5); -webkit-text-fill-color: rgba(0, 0, 0, 0.1);"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Santri</span>
                    <span class="info-box-number"><?= $countStudent ?></span>
                </div>
            </div>
        </div>

        <!--COUNT GRADUATED-->
        <div class="col-md-3">
            <div class="info-box no-margin">
                <span class="info-box-icon bg-green"><i class="fas fa-award" style="-webkit-text-stroke: 1px rgba(0, 0, 0, 0.5); -webkit-text-fill-color: rgba(0, 0, 0, 0.1);"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Lulus</span>
                    <span class="info-box-number"><?= $countGraduated ?></span>
                </div>
            </div>
        </div>

        <!--COUNT TEACHER-->
        <div class="col-md-3">
            <div class="info-box no-margin">
                <span class="info-box-icon bg-yellow"><i class="fas fa-chalkboard-teacher" style="-webkit-text-stroke: 1px rgba(0, 0, 0, 0.5); -webkit-text-fill-color: rgba(0, 0, 0, 0.1);"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Ustadz</span>
                    <span class="info-box-number"><?= $countTeacher ?></span>
                </div>
            </div>
        </div>

        <!--COUNT TEACHER-->
        <div class="col-md-3">
            <div class="info-box no-margin">
                <span class="info-box-icon bg-red"><i class="fas fa-university" style="-webkit-text-stroke: 1px rgba(0, 0, 0, 0.5); -webkit-text-fill-color: rgba(0, 0, 0, 0.1);"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Ma'had</span>
                    <span class="info-box-number"><?= $countMahad ?></span>
                </div>
            </div>
        </div>
    </div>
</div>