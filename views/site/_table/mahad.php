<?php

    use app\models\Student;
    use yii\grid\GridView;
    use yii\helpers\Html;
?>
<div class="table-mahad">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Nama',
                'value' => function($model) {
                    return ucwords(strtolower($model->name));
                },
            ],
            [
                'label' => 'Mudir',
                'value' => function($model) {
                    return ucwords(strtolower($model->mudir));
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/mahad-rating/create?mahad_id='. $id;
                    return Yii::$app->user->can('super') || Yii::$app->user->can('developer') ? (Html::a('<i class ="fas fa-sm fa-award"></i> Nilai Ma\'had', $link, ['class' => 'addMahadRatingBtn btn btn-xs btn-success'])):('');
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/mahad/view?id='. $id;
                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Detail', $link, ['class' => 'btn btn-xs btn-primary']);
                }
            ],
        ],
    ]); ?>
</div>