<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="table-student">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Nama',
                'value' => function($model) {
                    return ucwords(strtolower($model->name));
                },
            ],
            [
                'label' => "Angkatan",
                'attribute' => 'studentGraduate.year',
            ],
            [
                'format' => 'raw',
                'label' => 'Level',
                'value' => function($model) {
                    if ($model->doe == 0) {
                        return '-';
                    }
                    $level = round((time() - $model->doe) / (3600*24*365.242199));
                    if ($level >= 3) {
                        return "<small class='well well-sm bg-red text-center' style='padding: 5px 5px;'>Ke - $level<small>";
                    } elseif ($level == 0) {
                        return "<small class='well well-sm bg-blue text-center' style='padding: 5px 5px;'> Jadid <small>";
                    }
                    return "<small class='well well-sm bg-green text-center' style='padding: 5px 5px;'>Ke - $level<small>";
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/student/view?id='. $id;
                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Detail', $link, ['class' => 'btn btn-xs btn-primary']);
                }
            ],
        ],
    ]); ?>
</div>