<?php

use app\models\CalendarMonth;
use app\models\Mahad;
use app\models\Score;
use app\models\Student;
use app\models\StudentExam;
use miloschuman\highcharts\Highcharts;
use yii\widgets\Pjax;


    //COUNT STUDENT
    $mahad = Mahad::find()->all();
    $i = 0;
    $totalStudent = 0;        
    $score = Score::find()->all();

    foreach ($mahad as $x) {
        $countStudent = Student::find()->where(['mahad_id' => $x['id'], 'student_flag_id' => 1])->count();
        $j = 0;
        foreach ($score as $y) {
            $arrayScore[$j] = $y['name'];
            $studentExam = Student::find()
            ->join('LEFT OUTER JOIN', 'student_exam', 'student_exam.student_id = student.id')
            ->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam.student_exam_type_id = student_exam_type.id')
            ->join('LEFT OUTER JOIN', 'student_exam_type_category', 'student_exam_type.student_exam_type_category_id = student_exam_type_category.id')
            ->where(['student.mahad_id' => $x['id'], 'student.student_flag_id' => 1, 'student_exam.score_id' => $y['id'], 'student_exam_type.student_exam_type_category_id' => 2])
            ->count();
            $countRating[$i]['data'][$j] = (float)$studentExam;
            $j++;
            
        }
        $countRating[$i]['name'] = ucwords(strtolower($x['name']));
        $chartStudent[$i] = [ucwords(strtolower($x['name'])), (float)$countStudent];
        $totalStudent = $totalStudent + $countStudent;
        $i++;
    }

    //COUNT EXAM
    $year = date('Y');
    $start = strtotime("01 - jan - $year");
    $end = time();
    $exam = StudentExam::find()
    ->join('LEFT OUTER JOIN', 'student', 'student_exam.student_id = student.id')
    ->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam.student_exam_type_id = student_exam_type.id')
    ->where(['student_exam_type.student_exam_type_category_id' => 2, 'student.student_flag_id' => 1])
    ->andFilterWhere(['between', 'created_at', $start, $end])
    ->asArray()
    ->all();
    $countExam[0]['name'] = 'Mumtaz';
    $countExam[1]['name'] = 'Jayyid Jiddan';
    $countExam[2]['name'] = 'Jayyid';
    $countExam[3]['name'] = 'Rasib';
    for ($i = 0; $i < 12; $i++) {
        $countExam[0]['data'][$i] = 0;
        $countExam[1]['data'][$i] = 0;
        $countExam[2]['data'][$i] = 0;
        $countExam[3]['data'][$i] = 0;
    }
    foreach ($exam as $x) {
        $date = date('n', $x['created_at']);
        for($i = 0; $i < 12; $i++) {
            if ($x['score_id'] == 4) {
                if ($date == $i + 1) {
                    $countExam[3]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 3)  {
                if ($date == $i + 1) {
                    $countExam[2]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 2)  {
                if ($date == $i + 1) {
                    $countExam[1]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 1)  {
                if ($date == $i + 1) {
                    $countExam[0]['data'][$i]++;
                }
            }
        }
    }

    $month = CalendarMonth::find()->select('month')->asArray()->all();
    $i = 0;
    foreach ($month as $x) {
        $arrayMonth[$i] = ucfirst(strtolower($x['month']));
        $i++;
    }

?>
<!--MAP LEAFLET-->
<div class="row">
    <div class="col-md-12">
        <div class="box no-border" style="border: none; box-shadow: none; height: 100%;">
            <div class="box-header text-center"><h5 class="box-title">Peta Persebaran Ma'had Al-Askar</h5></div>
            <?php Pjax::begin(['id' => 'leaflet']) ?>
                <div class="box-body">
                    <?= $this->render('_leaflet') ?>
                </div>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<!--PIE CHART STUDENT-->
<div class="row">
    <div class="col-md-12">
        <?= Highcharts::widget([
            'scripts' => [
                'highcharts-more',
                'modules/exporting'
            ],
            'options' => [
                'chart' => [
                    'plotBackgroundColor' => null,
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                    'type' => 'pie',
                ],
                'tooltip' => [
                    'pointFormat' => "
                        {series.name}: <b>{point.percentage:.1f}%</b><br>
                        Total: <b>{point.y}</b> Santri"
                ],
                'accessibility' => [
                    'point' => [
                        'valueSuffix' => '%'
                    ]
                ],
                'title' => ['text' => 'Persentase Total Santri di Ma\'had'],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect'=> true,
                        'cursor' => 'pointer',
                        'dataLabels' => [
                            'enabled' => false
                        ],
                        'showInLegend' => true
                    ],
                ],
                'series' => [
                    [
                        'colorByPoint' => true,
                        'type' => 'pie',
                        'name' => 'Persentase',
                        'data' => $chartStudent,
                    ]
                ],
                'credits' => ['enabled' => false],
            ],
        ]) ?>
    </div>
</div>

<!--BAR CHART-->
<div class="row">
    <div class="col-md-12">
        <?= Highcharts::widget([
            'scripts' => [
                'highcharts-more',
                'modules/exporting'
            ],
            'options' => [
                'chart' => [
                    'type' => 'column',
                ],
                'tooltip' => [
                    'headerFormat' => '<span style="font-size:10px"><strong>{point.key}</strong></span><table class="" style="padding: 5px; font-size:10px;">',
                    'pointFormat' => '<tr><td style="color:{series.color};padding:3px">{series.name} </td> <td style="padding:3px"><b>{point.y}</b></td></tr>',
                    'footerFormat' => '</table>',
                    'shared' => true,
                    'useHTML' => true
                ],
                'title' => ['text' => 'Tingkat Keberhasilan Ujian Tahfiz Santri'],
                'plotOptions' => [
                    'column' => [
                        'pointPadding' => 0.2,
                        'borderWidth' => 0,
                        'showInLegend' => true
                    ],
                ],
                'xAxis' => [
                    'categories' => $arrayScore,
                    'crosshair' => true,
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                        'text' => 'Jumlah',
                    ],
                ],
                'series' => $countRating,
                'credits' => ['enabled' => false],
            ],
        ]) ?>
    </div>
</div>

<!--LINE CHART-->
<div class="row">
    <div class="col-md-12">
        <?= Highcharts::widget([
            'scripts' => [
                'highcharts-more',
                'modules/exporting'
            ],
            'options' => [
                'chart' => [
                    'type' => 'line'
                ],
                'title' => ['text' => "Data Keberhasilan Ujian Tahfiz Santri Tahun $year"],
                'plotOptions' => [
                    'line' => [
                        'dataLabels' => [
                            'enabled' => true,
                        ],
                        'enableMouseTracking' => true,
                    ],
                ],
                'xAxis' => [
                    'categories' => $arrayMonth,
                ],
                'yAxis' => [
                    'title' => [
                        'text' => 'Jumlah',
                    ],
                ],
                'series' => $countExam,
                'credits' => ['enabled' => false],
            ],
        ]) ?>
    </div>
</div>