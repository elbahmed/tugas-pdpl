<?php

use app\models\Scholarship;
use app\models\Student;
use kartik\grid\GridView;
use app\models\Tasmi;
use yii\data\ActiveDataProvider;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Semesteran Santri';
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
                        'query' => Student::find()->orderBy(['branch_id' => SORT_ASC]),
                        'sort' => false,
                        'pagination' => [
                            'pageSize' => 5,
                        ],
]);
?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'id' => [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'ID',
                'attribute' => 'id',
            ],
            'name' => [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Nama',
                'attribute' => 'name',
            ],
            'branch.name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'branch.name',
                'label' => "Ma'had",
            ],
            'doe'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Tanggal Muqim',
                'attribute' => 'doe',
                'value' =>  function($model) {
                                return date('d M Y', strtotime($model->__get('doe')));
                },
            ],
            'ziyadah'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Hafalan',
                'value' =>  function($model) {
                    $id = $model->id;
                    $ago = date("m", strtotime("-6 months"));
                    $month = date('m');
                    $query = Yii::$app->db->createCommand(" SELECT COUNT(DISTINCT page) AS count_page, COUNT(DISTINCT juz) AS count_juz, juz  FROM `ziyadah` LEFT OUTER JOIN `page` p ON p.id 
                                                                            BETWEEN 
                                                                            CASE
                                                                                WHEN start_page < end_page THEN start_page
                                                                                ELSE end_page
                                                                            END
                                                                            AND
                                                                            CASE
                                                                                WHEN start_page < end_page THEN end_page
                                                                                ELSE start_page
                                                                            END
                                                                            WHERE student_id = $id AND DATE(`date`) >= CAST($ago AS DATE)
                                                                            GROUP BY juz")->queryAll();
                    $ziyadah_juz = 0;
                    $ziyadah_page = 0;
                    for ($i = 0; $i < count($query); $i++) {
                        if ($query[$i]['juz'] == 30) {
                            if ($query[$i]['count_page'] == 23) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    
                        else if ($query[$i]['juz'] == 1) {
                            if ($query[$i]['count_page'] == 21) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    
                        else {
                            if ($query[$i]['count_page'] == 20) {
                                $ziyadah_juz = $ziyadah_juz + 1;
                            }
                        
                            else {
                                $ziyadah_page = $ziyadah_page + $query[$i]['count_page'];
                            }
                        }
                    }
                    return $ziyadah_juz . ' Juz ' . $ziyadah_page . ' Halaman';
                },
            ],
            'list_juz'=> [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Daftar Juz',
                'value' =>  function($model) {
                    $id = $model->id;
                    $ago = date("Y-m-d", strtotime("-6 months"));
                    $month = date('Y-m-d');
                    $query = Yii::$app->db->createCommand(" SELECT juz, COUNT(DISTINCT juz)  
                                                                        FROM `ziyadah` 
                                                                        LEFT OUTER JOIN `page` p ON p.id 
                                                                        BETWEEN
                                                                        CASE
                                                                            WHEN start_page < end_page THEN start_page
                                                                            ELSE end_page
                                                                        END
                                                                        AND
                                                                        CASE
                                                                            WHEN start_page < end_page THEN end_page
                                                                            ELSE start_page
                                                                        END
                                                                        WHERE student_id = $id AND DATE(`date`) >= CAST($ago AS DATE)
                                                                        GROUP BY juz")->queryAll();

                    if (count($query) == 0) {
                        return 'Belum Memiliki Hafalan';
                    }

                    for($i = 0; $i < count($query); $i++) {
                        $juz[$i] = 'juz '. $query[$i]['juz'];
                    }
                    if (count($juz) == 30) {
                        return 'Khatam';
                    }
                    $juz = implode(", ",$juz);
                    return  $juz;
                },
            ],
            'target' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'target',
                'label' => HtmlPurifier::process("Target Bulanan"),
                'value' => function($model) {
                    $id = $model->id;
                    $ago = date("Y-m-d", strtotime("-6 months"));
                    $target = Scholarship::findOne(['id' => $model->status_id])['target'] * 6;
                    $query = Yii::$app->db->createCommand(" SELECT COUNT(DISTINCT page) AS count_page FROM `ziyadah` LEFT OUTER JOIN `page` p ON p.id 
                                                                            BETWEEN 
                                                                            CASE
                                                                                WHEN start_page < end_page THEN start_page
                                                                                ELSE end_page
                                                                            END
                                                                            AND
                                                                            CASE
                                                                                WHEN start_page < end_page THEN end_page
                                                                                ELSE start_page
                                                                            END
                                                                            WHERE student_id = $id AND DATE(`date`) >= CAST($ago AS DATE)")->queryAll();
                    
                    if ($query[0]['count_page'] >= $target) {
                        return 'Tercapai';
                    }
                    return 'Tidak Tercapai';
                },
            ],
            'tasmi_id' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'tasmi_id',
                'label' => HtmlPurifier::process("Progress Tasmi Akbar"),
                'value' => function($model) {
                    $tasmi = Tasmi::findOne($model->tasmi_id);
                    if ($tasmi->id == 6) {
                        return 'Selesai';
                    }
                    return $tasmi['name'];
                },
            ],
        ],
        'exportConfig' => [
            GridView::PDF => [
                'label' => 'Save as PDF',
                'pdfConfig' => [
                    'methods' => [
                        'SetTitle' => 'Santri - Alaskar.com',
                        'SetSubject' => '',
                        'SetHeader' => ['Ma\'had Al Askar||Generated On: ' . date("r")],
                        'SetFooter' => ['|Page {PAGENO}|'],
                    ]
                ],
            ],
            GridView::EXCEL => ['label' => 'Save as EXCEL'], 
            GridView::CSV => ['label' => 'Save as CSV'], 
        ],


        'export' => [
            'PDF' => [
                'options' => [
                    'title' => 'Santri - Alaskar.id',
                    'subject' => ['Ma\'had Al Askar||Generated On: ' . date("r")],
                    'author' => 'Admin Al Askar',
                ]
            ],
        ],
          
        'toolbar' =>  [
            '{export}', 
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'floatHeader' => false,
        'panel' => [
            'type' => GridView::TYPE_INFO,
            'heading' => 'Laporan Santri Bulan '.date("F", strtotime("-6 months")) . ' - ' . date("F"),
        ],
            
    ]);?>

</div>