<?php

/* @var $this yii\web\View */

use app\models\Mahad;
use app\models\Student;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\data\ActiveDataProvider;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="body-content">

        <!--INFORMATION BOX-->
        <div class="row">
            <div class="box no-border" style="background-color: rgba(0, 0, 0, 0); box-shadow: none;"> 
                <div class="box-body"><?= $this->render('_information-box') ?></div>
            </div>
        </div>

        <div class="row">  
            <!--TABLE MAHAD-->
            <div class="col-md-6">
                <div class="box box-danger box-solid collapsed-box" style="height: 100%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ma'had</h3>
                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                           </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php Pjax::begin(['id' => 'table-mahad']) ?>
                            <?= $this->render('_table/mahad', [
                                'dataProvider' => new ActiveDataProvider([
                                    'query' => Mahad::find()->orderBy([
                                        'mahad_type_id' => SORT_ASC, 
                                        'province_id' => SORT_ASC, 
                                        'regency_id' => SORT_ASC,
                                        'name' => SORT_ASC,
                                    ]),
                                    'pagination' => [
                                        'pageSize' => 9,
                                    ],
                                    'sort' => false
                                ]),
                            ]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>

            <!--TABLE STUDENT-->
            <div class="col-md-6">
                <div class="box box-solid box-primary collapsed-box" style="height: 100%;">
                    <div class="box-header with-border">
                        <h3 class="box-title">Santri</h3>
                        <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                           </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php Pjax::begin(['id' => 'table-student']) ?>
                            <?= $this->render('_table/student', [
                                'dataProvider' => new ActiveDataProvider([
                                    'query' => Student::find()
                                    ->join('LEFT OUTER JOIN', 'mahad', 'mahad.id = student.mahad_id')
                                    ->orderBy([
                                        'mahad.mahad_type_id' => SORT_ASC, 
                                        'mahad.province_id' => SORT_ASC, 
                                        'mahad.regency_id' => SORT_ASC,
                                        'mahad.name' => SORT_ASC,
                                        'student.doe' => SORT_ASC,
                                        //'student.student_graduate_id' => SORT_ASC,
                                        //'student.student_group_id' => SORT_ASC,
                                        'student.name' => SORT_ASC,
                                    ])
                                    ->where(['student_flag_id' => 1]),
                                    'pagination' => [
                                        'pageSize' => 10,
                                    ],
                                    'sort' => false
                                ]),
                            ]) ?>
                        <?php Pjax::end() ?>
                    </div>
                    <div class="box-body">
                        <table>
                            <th><i>NB</i>:</th>
                            <tr>
                                <td>
                                    <div class="label label-primary text-center">Santri Baru</div>
                                    <div class="label label-success text-center">Santri dibawah 3 tahun</div>
                                    <div class="label label-danger text-center">Santri 3 tahun dan diatasnya</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--ADDON-->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning box-solid">
                    <div class="box-body"><?= $this->render('_addon') ?></div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
    Modal::begin([
        'header' => '',
        'id' => 'modal',
        'size' => 'modal-md',
    ]);
    echo "<div id='role-content'></div>";
    Modal::end();

    $this->registerJs("
        $(function(){
            $('#addMahadBtn').click(function (){
                $.get($(this).attr('href'), function(data) {
                  $('#modal').modal('show').find('#role-content').html(data)
               });
               return false;
            });
        });
    ");

    $this->registerJs("
        $(function(){
            $('.addMahadRatingBtn').click(function (){
                $.post($(this).attr('href'), function(data) {
                  $('#modal').modal('show').find('#role-content').html(data)
               });
               return false;
            });
        });
    ");
    
    $this->registerCss("
        .leaflet-container {
            height: 400px !important;
            border-radius: 3px;
        }
    ")
?>