<?php

use app\models\Student;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentExamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penilaian Santri';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-exam-index">
    <!--TABLE STUDENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">SANTRI <?= $mahad->name ?></h3></div>
                <?php Pjax::begin(['id' => 'table-student']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/student', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Student::find()
                                ->join('LEFT OUTER JOIN', 'mahad', 'mahad.id = student.mahad_id')
                                ->orderBy([
                                    'mahad.mahad_type_id' => SORT_ASC, 
                                    'mahad.province_id' => SORT_ASC, 
                                    'mahad.regency_id' => SORT_ASC,
                                    'mahad.name' => SORT_ASC,
                                    'student.doe' => SORT_ASC,
                                    //'student.student_graduate_id' => SORT_ASC,
                                    //'student.student_group_id' => SORT_ASC,
                                    'student.name' => SORT_ASC,
                                ])
                                ->where(['mahad_id' => $mahad->id])
                                ->andWhere(['student_flag_id' => 1]),
                                'pagination' => [
                                    'pageSize' => false,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
