<?php

use app\models\Mahad;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\StudentExam */
$this->title = 'Penilaian Santri';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-exam-select-mahad">
    <div class="container" style="padding: 10% 0;">

        <form method="get" action="multi-create">

            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12" style="margin: auto; float: none;">
                    <div class="box box-solid box-primary">

                        <div class="box-header with-border"><h1 class="box-title">Pilih Ma'had</h1></div>

                        <div class="box-body" style="padding: 20px 10px;">
                            <?= Select2::widget([
                                'name' => 'mahad_id',
                                'data' => ArrayHelper::map(Mahad::find()->all(), 'id', 'name'),
                                'options' => ['placeholder' => 'Pilih Ma\'had...'],
                            ]) ?> 

                        </div>

                        <div class="box-footer">
                            <input type="submit" class="btn btn-success pull-right" value="Lanjutkan &raquo;">
                        </div>

                    </div>
                </div>
            </div>
                        
        </form>
                        
    </div>
</div>
