<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\Score;
use app\models\Student;
use app\models\Teacher;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentRating */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-rating-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group"><p readonly class="form-control"><?= Student::findOne($model->student_id)->name ?></p></div>

    <div class="form-group">
        <label>Tanggal</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'created-at-date-id',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'created-at-date',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'created-at-month-id',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'created-at-month',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'created-at-year-id',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['between', 'year', 2015, date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'created-at-year',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <?php foreach ($studentRatingType as $x): ?>
        <?= $form->field($model, strtolower($x['name']))->widget(Select2::className(),[
            'data' => ArrayHelper::map(Score::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Pilih Nilai...',
                'id' => strtolower($x['name']),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ])?>
    <?php endforeach; ?>

    <?= $form->field($model, 'teacher_id')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Teacher::find()->where(['teacher_position_id' => 1])->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Ustadz Penilai...',
            'id' => 'teacher_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textarea()  ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
