<?php

use app\models\Score;
use app\models\StudentRating;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="table-student">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
            ],
            [
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'label' => 'Kelancaran',
                'value' => function($dataProvider) {
                    $studentRatingKelancaran = 0;
                    $modelStudentRatingKelancaran = StudentRating::find()->where(['student_rating_type_id' => 1])->andWhere(['student_id' => $dataProvider->id])->all();
                    if ($modelStudentRatingKelancaran != null) {
                        $i = 0;
                        foreach ($modelStudentRatingKelancaran as $x) {
                            $studentRatingKelancaran = $studentRatingKelancaran + Score::findOne($x['score_id'])->score;
                            $i++;
                        }
                        $studentRatingKelancaran = round($studentRatingKelancaran / $i);
                    }
                    return $studentRatingKelancaran . '%';
                }
            ],
            [
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'label' => 'Makhroj',
                'value' => function($dataProvider) {
                    $modelStudentRatingMakhroj = StudentRating::find()->where(['student_rating_type_id' => 2])->andWhere(['student_id' => $dataProvider->id])->all();
                    $studentRatingMakhroj= 0;
                    if ($modelStudentRatingMakhroj!= null) {
                        $i = 0;
                        foreach ($modelStudentRatingMakhroj as $x) {
                            $studentRatingMakhroj = $studentRatingMakhroj + Score::findOne($x['score_id'])->score;
                            $i++;
                        }
                        $studentRatingMakhroj = round($studentRatingMakhroj / $i);
                    }
                    return $studentRatingMakhroj . '%';
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/student-rating/create?id='. $id;
                    return Html::a('<i class ="fas fa-sm fa-plus"></i> Penilaian', $link, ['class' => 'btn btn-xs btn-success addStudentRatingBtn']);
                }
            ],
        ],
    ]); ?>
</div>

<?php
Modal::begin([
    'id' => 'modal-student',
    'size' => 'modal-md',
]);
echo "<div id='role-content'></div>";
Modal::end();

$this->registerJs("
    $(function(){
        $('.addStudentRatingBtn').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
");
?>