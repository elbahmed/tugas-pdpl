<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StudentRating */
?>
<div class="student-rating-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
