<?php
    
use yii\helpers\Url;

$this->title = 'Copyright Disclaimer'
?>
<div class="container" style="background-color: #fff;">
    <h1>Disclaimer for Al Askar</h1>

    <p>If you require any more information or have any questions about our site's disclaimer, please feel free to contact us by email at admin@alaskar.id. Our Disclaimer was generated with the help of the <a href="https://www.disclaimergenerator.net/">Disclaimer Generator</a>.</p>

    <h2>Disclaimers for Al Askar</h2>

    <p>All the information on this website - alaskar.id - is published in good faith and for general information purpose only. Al Askar does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website (Al Askar), is strictly at your own risk. Al Askar will not be liable for any losses and/or damages in connection with the use of our website.</p>

    <p>From our website, you can visit other websites by following hyperlinks to such external sites. While we strive to provide only quality links to useful and ethical websites, we have no control over the content and nature of these sites. These links to other websites do not imply a recommendation for all the content found on these sites. Site owners and content may change without notice and may occur before we have the opportunity to remove a link which may have gone 'bad'.</p>

    <p>Please be also aware that when you leave our website, other sites may have different privacy policies and terms which are beyond our control. Please be sure to check the Privacy Policies of these sites as well as their "Terms of Service" before engaging in any business or uploading any information. Our Privacy Policy was created by <a href="https://www.generateprivacypolicy.com/">the Privacy Policy Generator</a>.</p>

    <h2>Consent</h2>

    <p>By using our website, you hereby consent to our disclaimer and agree to its terms.</p>

    <h2>Update</h2>

    <p>Should we update, amend or make any changes to this document, those changes will be prominently posted here.</p>

    <a href="privacy-policy"><strong>Our Privacy Policy</strong></a>

    <h1>Special Thanks to...</h1>
    <div class="row">
        <div class="col-md-4">
            <a target="_blank" href="https://www.yiiframework.com"><img width="100%" height="100px" src="<?=Url::to('@web/img/sponsor/yii.png')?>"></a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="https://www.adminlte.io"><img width="100%" height="100px" src="<?=Url::to('@web/img/sponsor/adminlte.svg')?>"></a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="https://www.highcharts.com"><img width="100%" height="100px" src="<?=Url::to('@web/img/sponsor/highcharts.png')?>"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <a target="_blank" href="https://www.getbootstrap.com"><img width="100%" height="200px" src="<?=Url::to('@web/img/sponsor/bootstrap.png')?>" style="background: white;"></a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="https://www.leafletjs.com"><img width="100%" height="200px" src="<?=Url::to('@web/img/sponsor/leaflet.png')?>"></a>
        </div>
        <div class="col-md-4">
            <a target="_blank" href="https://www.krajee.com"><img width="100%" height="200px" src="<?=Url::to('@web/img/sponsor/krajee.png')?>" style="background: #bbb;"></a>
        </div>
    </div>
</div>