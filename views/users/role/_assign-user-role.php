<?php

use app\models\users\User;
use app\models\users\UserRole;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\web_adm\forms\AssignUserRoleForm */
/* @var $form ActiveForm */
?>
<div class="users-role-_assign-user-role">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
            'options' => [
                'placeholder' => 'Select User ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <?= $form->field($model, 'item_name')->widget(Select2::className(), [
            'data' => ArrayHelper::map(UserRole::find()->all(), 'name', 'name'),
            'options' => [
                'placeholder' => 'Select Role ...',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- web-adm-role-_assign-user-role -->
