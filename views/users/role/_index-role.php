<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
?>

<p>
    <?= Html::a('Delete All', ['delete-all-role'], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete all role?',
            'method' => 'post',
        ],
    ]) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'table-responsive'
    ],
    'summary' => false,
    'columns' => [
        'name',
        'description',
        'created_at' => [
            'class' => 'yii\grid\DataColumn',
            'attribute' => 'created_at',
            'value' => function($dataProvider) {
                return date('D, d M Y', $dataProvider->created_at);
            },
        ],
        [                
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function($url, $dataProvider, $key) {
                    return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-role', 'name' => $dataProvider->name],[
                        'data-confirm' => 'Are you sure you want to delete this role?',
                    ]);
                },
            ],
        ]
    ]
]) ?>