<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\web_adm\forms\RecoveryForm */
/* @var $form ActiveForm */

$this->title = 'Setel ulang kata sandi';

?>
<div class="web-adm-recovery">
    <div class="row" style="margin-top: 5rem;">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body">

                    <?php $form = ActiveForm::begin([
                        'id' => 'web-adm-recovery-form',
                        'enableAjaxValidation' => true,
                    ]); ?>

                        <?= $form->field($model, 'username') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>