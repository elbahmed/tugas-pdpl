<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index-user']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">
    
    <h1><?= Html::encode($model->nick_name) ?></h1>
    <?php 
        Modal::begin([
            'header' => 'Change Password',
            'id' => 'modal-change-password',
            'size' => 'modal-lg',
        ]);
        echo "<div id='change-password-content'></div>";
        Modal::end();
    ?>

    <div class="panel panel-info">
        <div class="panel-heading"><h1 class="panel-title"><i class="fas fa-user"></i> User Data</h1></div>
        <div class="panel-body">
            <?php
                if (Yii::$app->user->id == $model->id) {
                    echo Html::a('Change password', ['change-password', 'id' => $model->id], ['data-method' => 'post', 'class' => 'btn btn-primary changePasswordButton']);
                }
            ?>
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                'email',
                'nick_name',
                'role',
                'blocked_at',
                [
                    'attribute' => 'created_at',
                    'value' => function($model) {
                        return date('l, d M Y H:i:s', $model->created_at);
                    }
                ],
                [
                    'attribute' => 'last_login_at',
                    'value' => function($model) {
                        return date('l, d M Y H:i:s', $model->last_login_at);
                    }
                ],
                'created_ip'
            ],
        ]) ?>

    </div>

    <!--USER LOG ACTIVITY-->
    <div class="panel panel-info">
        <div class="panel-heading"><h1 class="panel-title"><i class="fas fa-history"></i>  User Log Activity</h1></div>
        <?= GridView::widget([
            'dataProvider' => $logProvider,
            'summary' => false,
            'options' => [
                'class' => 'table-responsive'
            ],
            'tableOptions' => [
                'class' => 'table table-striped',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'type.name' => [
                    'class' => 'yii\grid\DataColumn',
                    'label' => 'Type',
                    'value' => 'type.name'
                ],
                'time' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'time',
                    'value' => function($dataProvider) {
                        return date('l, d M Y H:i:s', $dataProvider->time);
                    },
                ],
                'user_ip',
                'description',
            ]
        ]); ?>
    </div>

</div>

<?php
$this->registerJs("
    $(function(){
        // changed id to class
        $('.changePasswordButton').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-change-password').modal('show').find('#change-password-content').html(data)
           });
           return false;
        });
    });
");
?>