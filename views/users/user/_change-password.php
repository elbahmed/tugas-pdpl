<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\RegistrationForm */
/* @var $form ActiveForm */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="users-user-_change-password">

    <?php $form = ActiveForm::begin([
        'id' => 'users-user-_change-password-form',
        'enableAjaxValidation' => true,
    ]); ?>

        <?= $form->field($model, 'oldPassword')->passwordInput() ?>

        <?= $form->field($model, 'newPassword')->passwordInput() ?>

        <?= $form->field($model, 'confirmPassword')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', [
                'class' => 'btn btn-lg btn-primary btn-block', 
                'style' => 'margin-top: 2rem;'
            ]) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>