<?php

use app\models\web_adm\UserMeta;
use app\models\web_adm\UserRoleAssignment;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-user-index-user container-fluid">
    
    <div class="panel panel-info">
        <div class="panel-heading">
            <h1 class="panel-title"><i class="fas fa-users"></i><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body" style="outline: 1px solid #bce8f1;">
            <div class="row">
                <div class="col-md-6 btn-group">
                    <?= Html::a('Create User', ['registration'], ['data-method' => 'post', 'class' => 'btn btn-sm btn-default registrationButton']) ?>
                    <?= Html::a('Create Role', ['create-role'], ['data-method' => 'post', 'class' => 'btn btn-sm btn-default roleButton']) ?>
                    <?= Html::a('Assign Role', ['assign-user-role'], ['data-method' => 'post', 'class' => 'btn btn-sm btn-default assignButton']) ?>
                </div>
                <div class="col-md-6">
                    <div class="btn-group" style="float: right;">
                        <?= Html::a('List of Role', ['index-role'], ['data-method' => 'post', 'class' => 'btn btn-sm btn-default listRoleButton']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?= GridView::widget([
            'tableOptions' => [
                'class' => 'table table-striped text-center',
            ],
            'options' => [
                'style' => 'padding: .5%;',
                'class' => 'table-responsive'
            ],
            'summary' => false,
            'dataProvider' => $dataProvider,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
            
                'username',
                'email',
                'nick_name',
                'role',
                'blocked' => [
                    'header' => 'Block status',
                    'value' => function ($dataProvider) {
                        if ($dataProvider->blocked_status == 1) {
                            return Html::a('Unblock', ['block-user', 'id' => $dataProvider->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'Are you sure you want to unblock this user?',
                            ]);
                        }
                        else if ($dataProvider->role == 'developer') {
                            return Html::a('Can\'t Block', '#', [
                                'class' => 'btn btn-xs btn-danger btn-block disabled',
                            ]);
                        }
                        else {
                            return Html::a('Block', ['block-user', 'id' => $dataProvider->id], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'Are you sure you want to block this user?',
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],
                'blocked_at' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'blocked_at',
                    'value' => function($dataProvider) {
                        if ($dataProvider->role == 'developer') {
                            return 'Never';
                        }
                        else if ($dataProvider->blocked_at === null) {
                            return 'Not Set';
                        }
                        return date('D d M, H:i', $dataProvider->blocked_at);
                    },
                ],
                'created_at' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'created_at',
                    'value' => function($dataProvider) {
                        if ($dataProvider->created_at != null) {
                            return date('D d M, H:i', $dataProvider->created_at);
                        }
                    },
                ],
                'last_login_at' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'last_login_at',
                    'value' => function($dataProvider) {
                        if ($dataProvider->last_login_at != null) {
                            return date('D d M, H:i', $dataProvider->last_login_at);
                        }
                        return 'never';
                    },
                ],
                'registration_ip' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'registration_ip',
                    'value' => function($dataProvider) {
                        if ($dataProvider->created_ip != null) {
                            return $dataProvider->created_ip;
                        }
                    },
                ],
                [               
                    'class' => 'yii\grid\ActionColumn',
            
                    'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
            
                	'template' => '{view} {delete}',
            
                    'buttons' => [
                    
                    
                	    'view' => function ($url, $model, $key) {
                        
                	    		return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view-user', 'id' => $model->__get('id')]);
                        
                    },
                    'delete' => function($url, $dataProvider, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-user', 'id' => $dataProvider->id], [
                            'data-method' => 'post',
                            'data-confirm' => 'Are you sure you want to delete this user?',
                        ]);
                        },
                    
                	],
                    
                ]
            ],
        ]); ?>
    </div>

    <?php 
        Modal::begin([
            'header' => 'Create New Role',
            'id' => 'modal-role',
            'size' => 'modal-md',
        ]);
        echo "<div id='role-content'></div>";
        Modal::end();
    
        Modal::begin([
            'header' => 'Assign Role to User',
            'id' => 'modal-assign',
            'size' => 'modal-md',
        ]);
        echo "<div id='assign-content'></div>";
        Modal::end();

        Modal::begin([
            'header' => '<i class="fas fa-user-plus"></i> Create New User',
            'id' => 'modal-registration',
            'size' => 'modal-md',
        ]);
        echo "<div id='registration-content'></div>";
        Modal::end();
     
        Modal::begin([
            'header' => 'Role',
            'id' => 'modal-list-role',
            'size' => 'modal-md',
        ]);
        echo "<div id='list-role-content'></div>";
        Modal::end();
    ?>
</div>

<?php 
$this->registerJs("
    $(function(){
        $('.roleButton').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-role').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
");

$this->registerJs("
    $(function(){
        $('.assignButton').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-assign').modal('show').find('#assign-content').html(data)
           });
           return false;
        });
    });
");

$this->registerJs("
    $(function(){
        $('.registrationButton').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-registration').modal('show').find('#registration-content').html(data)
           });
           return false;
        });
    });
");
 
$this->registerJs("
    $(function(){
        $('.listRoleButton').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal-list-role').modal('show').find('#list-role-content').html(data)
           });
           return false;
        });
    });
");
?>
