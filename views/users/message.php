<?php

use yii\bootstrap\Alert;
use yii\helpers\Html;

?>

    <div class="row card" style="width: 50%; margin: 10% auto;">
        <div class="col-xs-12">
            <?= Alert::widget([
                'options' => ['class' => 'callout callout-' . 'success'],
                'body' => 'Kata sandi baru telah dikirim ke email anda.'
            ]) ?>
        </div>
        <div class="col-xs-12">
            <?= Html::a('Ke halaman login', '/site/login', ['class' => 'btn btn-info',]) ?>
        </div>
    </div>