<?php

use app\models\Mahad;
use app\models\users\UserRole;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\web_adm\forms\RegistrationForm */
/* @var $form ActiveForm */
?>
<div class="web-adm-registration">

    <?php $form = ActiveForm::begin([
        'id' => 'web-adm-registration-form',
        'enableAjaxValidation' => true,
    ]); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'first_name') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'last_name') ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'username') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'email') ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'initialPassword')->passwordInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
            </div>

        </div>
        
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mahad_id')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(Mahad::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Pilih ma\'had...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'role')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(UserRole::find()->all(), 'name', 'name'),
                    'options' => [
                        'placeholder' => 'Pilih role...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary pull-right']) ?>
            <?= Html::a('Cancel', '/web-adm/index-user', ['class' => 'btn btn-warning']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- web-adm-registration -->
