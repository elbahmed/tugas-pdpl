<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Login Log';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-log-login-log">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a('Delete All', ['delete-log', 'id' => 3] ,[
        'class' => 'btn btn-danger',
        'data-method' => 'post',
        'data-confirm' => 'Are you sure you want to delete all login log?',
    ]) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'user',
            'type.name',
            'time' => [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'time',
                'value' => function($dataProvider) {
                    return date('l, d F Y H:i:s', $dataProvider->time);
                },
            ],
            'user_ip',
            'description',
        ]
    ]); ?>

</div>