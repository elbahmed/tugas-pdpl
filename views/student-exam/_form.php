<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\QuranJuz;
use app\models\Score;
use app\models\StudentExamType;
use app\models\StudentExamTypeCategory;
use app\models\Teacher;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentExam */
/* @var $form yii\widgets\ActiveForm */
$studentExamType = StudentExamType::find()->asArray()->all();
$i = 0;

foreach ($studentExamType as $x) {
    $studentExamType[$i] = [
        'id' => $x['id'],
        'name' => StudentExamTypeCategory::findOne($x['student_exam_type_category_id'])->name .' - '. $x['name'],
    ];
    $i++;
}
$studentExamType = ArrayHelper::map($studentExamType, 'id', 'name');
?>

<div class="student-exam-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group"><label>Santri</label> <p class="form-control" readonly><?= $model->student->name ?></p></div>
        </div>
        <div class="col-md-6">
            <div class="form-group"><label>Musyrif</label> <p class="form-control" readonly><?= $model->teacher->name ?></p></div>
        </div>
    </div>

    <div class="form-group">
        <label>Tanggal</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'created-at-date-id',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'created-at-date',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'created-at-month-id',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'created-at-month',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'created-at-year-id',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['between', 'year', 2015, date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'created-at-year',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'student_exam_type_id')->widget(Select2::className(),[
        'data' => $studentExamType,
        'options' => [
            'placeholder' => 'Pilih Tipe Ujian...',
            'id' => 'student-exam-type-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'start')->widget(Select2::className(),[
        'data' => ArrayHelper::map(QuranJuz::find()->all(), 'id', 'juz'),
        'options' => [
            'placeholder' => 'Pilih Juz Mulai...',
            'id' => 'start',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'end')->widget(Select2::className(),[
        'data' => ArrayHelper::map(QuranJuz::find()->all(), 'id', 'juz'),
        'options' => [
            'placeholder' => 'Pilih Juz Selesai...',
            'id' => 'end',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'score_id')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Score::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Nilai...',
            'id' => 'score-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textarea()  ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
