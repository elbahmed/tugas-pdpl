<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StudentExam */
?>
<div class="student-exam-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
