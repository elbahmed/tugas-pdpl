<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MahadRatingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahad Ratings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahad-rating-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mahad Rating', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mahad_id',
            'mahad_rating_type_id',
            'score_id',
            'teacher_id',
            //'comment_id',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
