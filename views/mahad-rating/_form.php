<?php

use app\models\Score;
use app\models\Teacher;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MahadRating */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mahad-rating-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php foreach($mahadRatingType as $x): ?>
        <?= $form->field($model, strtolower($x['name']))->widget(Select2::className(),[
            'data' => ArrayHelper::map(Score::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Pilih Nilai...',
                'id' => strtolower($x['name']),
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>
    <?php endforeach; ?>

    <?= $form->field($model, 'teacher_id')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Teacher::find()->where(['teacher_position_id' => 3])->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Ustadz Penilai...',
            'id' => 'teacher_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
