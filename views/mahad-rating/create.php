<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MahadRating */

$this->title = 'Create Mahad Rating';
$this->params['breadcrumbs'][] = ['label' => 'Mahad Ratings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahad-rating-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'mahadRatingType' => $mahadRatingType,
    ]) ?>

</div>
