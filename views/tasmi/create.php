<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tasmi */

?>
<div class="tasmi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
