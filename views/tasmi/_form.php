<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\Tasmi;
use app\models\TasmiProgress;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tasmi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasmi-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group"><label>Santri</label> <p class="form-control" readonly><?= $model->student->name ?></p></div>
        </div>
        <div class="col-md-6">
            <div class="form-group"><label>Musyrif</label> <p class="form-control" readonly><?= $model->teacher->name ?></p></div>
        </div>
    </div>

    <div class="form-group">
        <label>Tanggal</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'created-at-date-id',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'created-at-date',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'created-at-month-id',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'created-at-month',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'created-at-year-id',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['between', 'year', 2015, date('Y', time())])->all(), 'id', 'year'),
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'created-at-year',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'tasmi_progress_id')->widget(Select2::className(),[
        'data' => ArrayHelper::map(Tasmi::findAll(['student_id' => $model->student->id]) == null ? (TasmiProgress::find()->all()) : (TasmiProgress::find()->where(['>', 'id', Tasmi::find()->where(['student_id' => $model->student_id])->orderBy('tasmi_progress_id DESC')->one()['tasmi_progress_id']])->all()), 'id', 'name'),
        'options' => [
            'placeholder' => 'Progres Tasmi...',
            'id' => 'tasmi-progress-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea()  ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
