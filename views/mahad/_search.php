<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MahadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mahad-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mahad_type_id') ?>

    <?= $form->field($model, 'province_id') ?>

    <?= $form->field($model, 'regency_id') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'mudir') ?>

    <?php // echo $form->field($model, 'map') ?>

    <?php // echo $form->field($model, 'address') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
