<?php

use app\models\PassReason;
use app\models\PassReasonDescriptionFlag;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<div class="table-student">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
            ],
            [
                'label' => "Angkatan",
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'attribute' => 'studentGraduate.year',
            ],
            [
                'label' => 'Total Hafalan',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    return $dataProvider->totalZiyadah();
                }
            ],
            [
                'format' => 'raw',
                'label' => 'Daftar Juz',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $listJuz = '/student/list-juz?id='. $dataProvider->id;

                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Daftar Juz', $listJuz,['class' => 'viewListJuzGraduated btn btn-xs btn-warning']);
                }
            ],
            [
                'format' => 'ntext',
                'label' => 'Alasan Kelulusan',
                'value' => function($dataProvider) {
                    $description = PassReason::findOne(['student_id' => $dataProvider->id])->passReasonDescription;
                    $descriptionFlag = PassReasonDescriptionFlag::findOne($description->id);
                    if ($descriptionFlag != null) {
                        return $descriptionFlag->name .' - '. ucfirst(strtolower($description->description));
                    }
                    return ucfirst(strtolower($description->description));
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/student/view?id='. $id;
                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Detail', $link, ['class' => 'btn btn-xs btn-primary']);
                }
            ],
        ],
    ]); ?>
</div>

<?php

Modal::begin([
    'id' => 'modal-graduated-sm',
    'size' => 'modal-sm',
    'header' => 'Progres Hafalan per Juz'
]);
echo "<div id='role-content-sm'></div>";
Modal::end();

$this->registerJs("
    $(function(){
        $('.viewListJuzGraduated').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-graduated-sm').modal('show').find('#role-content-sm').html(data)
           });
           return false;
        });
    });
");
?>