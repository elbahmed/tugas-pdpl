<?php

use app\traits\TimeTrait;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="table-teacher">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Nama',
                'value' => function($model) {
                    return ucwords(strtolower($model->name));
                },
            ],
            [
                'label' => 'Hafalan',
                'value' => function($model) {
                    return $model->total_tahfiz;
                },
            ],
            [
                'label' => 'Tanggal Muqim',
                'value' => function($model) {
                    return TimeTrait::translateDate($model->doe);
                }
            ],
            [
                'label' => 'Jabatan',
                'value' => function($model) {
                    return ucwords(strtolower($model->teacherPosition->name));
                },
            ],
            [                
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($url, $dataProvider, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/teacher/delete', 'id' => $dataProvider->id],[
                            'data-method' => 'post',
                            'data-confirm' => 'Apakah anda yakin ingin menghapus item ini?',
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>

</div>
