<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ListView;

?>
<div class="table-student">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
            ],
            [
                'label' => "Halaqoh",
                'attribute' => 'studentGroup.name',
            ],
            [
                'label' => "Angkatan",
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'attribute' => 'studentGraduate.year',
            ],
            [
                'format' => 'raw',
                'label' => 'Level',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($model) {
                    if ($model->doe == 0) {
                        return '-';
                    }
                    $level = round((time() - $model->doe) / (3600*24*365.242199));
                    if ($level >= 3) {
                        return "<small class='well well-sm bg-red text-center' style='padding: 5px 5px;'>Ke - $level<small>";
                    }
                    return "<small class='well well-sm bg-green text-center' style='padding: 5px 5px;'>Ke - $level<small>";
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => 'Ziyadah',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/ziyadah/create?student_id='. $id;
                    if (Yii::$app->user->can('admin') || Yii::$app->user->can('developer')) {
                        return Html::a('<i class ="fas fa-sm fa-plus"></i> Ziyadah', $link, ['class' => 'btn btn-xs btn-info addZiyadahBtn']);
                    }
                    return Html::a('<i class ="fas fa-sm fa-plus"></i> Ziyadah', '#', ['class' => 'btn btn-xs btn-info disabled']);
                }
            ],
            [
                'label' => 'Total Hafalan',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    return $dataProvider->totalZiyadah();
                }
            ],
            [
                'format' => 'raw',
                'label' => 'Daftar Juz',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $listJuz = '/student/list-juz?id='. $dataProvider->id;

                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Daftar Juz', $listJuz,['class' => 'viewListJuz btn btn-xs btn-warning']);
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => 'Penilaian Santri',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $studentExam    = '/student-exam/create?student_id='. $id;
                    $Tasmi          = '/tasmi/create?student_id='. $id;

                    if (Yii::$app->user->can('admin') || Yii::$app->user->can('developer')) {
                        return 
                            '<div class="btn-group">'. 
                                Html::a('<i class ="fas fa-sm fa-plus"></i> Ujian', $studentExam, ['class' => 'btn btn-xs btn-danger addStudentExamBtn']) .
                                Html::a('<i class ="fas fa-sm fa-plus"></i> Tasmi', $Tasmi, ['class' => 'btn btn-xs btn-success addTasmiBtn']) . 
                            '</div>';
                    }

                    return Html::a('<i class ="fas fa-sm fa-plus"></i> Ujian', $studentExam, ['class' => 'btn btn-xs btn-danger addStudentExamBtn']);
                    
                }
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'raw',
                'label' => null,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function($dataProvider) {
                    $id = $dataProvider->id;
                    $link = '/student/view?id='. $id;
                    return Html::a('<i class ="fas fa-sm fa-eye"></i> Detail', $link, ['class' => 'btn btn-xs btn-primary']);
                }
            ],
        ],
    ]); ?>
</div>

<?php
Modal::begin([
    'id' => 'modal-student',
    'size' => 'modal-md',
]);
echo "<div id='role-content'></div>";
Modal::end();

Modal::begin([
    'id' => 'modal-student-sm',
    'size' => 'modal-sm',
    'header' => 'Progres Hafalan per Juz'
]);
echo "<div id='role-content-sm'></div>";
Modal::end();

$this->registerJs("
    $(function(){
        $('.addStudentExamBtn').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.addZiyadahBtn').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.addTasmiBtn').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-student').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.viewListJuz').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal-student-sm').modal('show').find('#role-content-sm').html(data)
           });
           return false;
        });
    });
");
?>