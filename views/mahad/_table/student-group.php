<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="table-student-group">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Halaqoh',
                'value' => function($model) {
                    return ucwords(strtolower($model->name));
                },
            ],
            [
                'label' => 'Musyrif P.J.',
                'attribute' => 'teacher.name'
            ],
            [                
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($url, $dataProvider, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/student/student-group-delete', 'id' => $dataProvider->id],[
                            'data-method' => 'post',
                            'data-confirm' => 'Apakah anda yakin ingin menghapus item ini?',
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
</div>