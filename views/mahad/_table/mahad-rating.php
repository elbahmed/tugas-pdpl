<?php

use app\traits\TimeTrait;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="table-student-group">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Jenis Penilaian',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->mahadRatingType->name));
                },
            ],
            [
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'label' => 'Nilai',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->score->name));
                },
            ],
            [
                'label' => 'Tanggal Penilaian',
                'value' => function($dataProvider) {
                    return TimeTrait::translateDate($dataProvider->created_at);
                },
            ],
            [
                'label' => 'Ustadz Penilai',
                'attribute' => 'teacher.name'
            ],
            [
                'label' => 'Komentar',
                'attribute' => 'comment.comment'
            ],
        ],
    ]); ?>
</div>