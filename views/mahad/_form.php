<?php

use app\models\MahadType;
use app\models\Province;
use app\models\Regency;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Mahad */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="mahad-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mahad_type_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map(MahadType::find()->all(), 'id', 'name'),
            'value' => $model->mahad_type_id,
            'options' => [
                'placeholder' => 'Pilih Jenis Ma\'had...',
                'id' => 'mahad-type-id',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <?= $form->field($model, 'mudir')->textInput() ?>
            
        <?= $form->field($model, 'map')->textInput(['maxlength' => true]) ?>
            
        <?= $form->field($model, 'province_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map(Province::find()->all(), 'id', 'name'),
            'value' => $model->province_id,
            'options' => [
                'placeholder' => 'Pilih Provinsi...',
                'id' => 'province-id',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <?= $form->field($model, 'regency_id')->widget(DepDrop::className(), [
            'data' => ArrayHelper::map(Regency::find()->where(['province_id' => $model->province_id])->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Pilih Daerah...',
                'id' => 'regency-id',
            ],
            'type' => DepDrop::TYPE_SELECT2,
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['province-id'],
                'url' => Url::to(['regency-depdrop'])
            ]
        ]) ?>

        <?= $form->field($model, 'address')->textarea(['rows' => 6, 'placeholder' => 'Alamat...']) ?>
            
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
        </div>
            
    <?php ActiveForm::end(); ?>

</div>
