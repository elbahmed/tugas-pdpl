<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mahad */

$this->title = 'Create Mahad';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Ma\'had', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
