<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mahad */

$this->title = 'Update Mahad: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Daftar Ma\'had', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mahad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
