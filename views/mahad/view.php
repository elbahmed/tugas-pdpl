<?php

use app\models\CalendarMonth;
use app\models\MahadRating;
use app\models\MahadRatingType;
use app\models\Regency;
use app\models\Student;
use app\models\StudentExam;
use app\models\StudentGroup;
use app\models\Teacher;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Mahad */

$this->title = ucwords(strtolower($model->name));
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$mahadRatingType = MahadRatingType::find()->all();
$newestMahadRating = null;
$mahadQuality = 0;
$i = 0;

foreach($mahadRatingType as $x) {
    $name = $x['name']; 
    $mahadRating = MahadRating::find()
    ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
    ->where(['mahad_rating_type_id' => $x['id'], 'mahad_id' => $model->id])
    ->all();

    if ($mahadRating != null) {
        $newestMahadRating[$name] = MahadRating::find()
        ->select("mahad_rating_type_id, score.name, score.score, score.abbr, mahad_id")
        ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
        ->where(['mahad_rating_type_id' => $x['id'], 'mahad_id' => $model->id])
        ->orderBy(['created_at' => SORT_DESC])
        ->asArray()
        ->one();

        foreach($mahadRating as $y) {
            $mahadQuality = $mahadQuality + $y['score']['score'];
            $i++;
        }
    }
}

if ($mahadQuality != 0) {
    $mahadQuality = round($mahadQuality / $i);
}

$year = date('Y');
$start = strtotime("01 - jan - $year");
$end = time();
$exam = StudentExam::find()
->join('LEFT OUTER JOIN', 'student', 'student_exam.student_id = student.id')
->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam.student_exam_type_id = student_exam_type.id')
->where(['student_exam_type.student_exam_type_category_id' => 2, 'student.mahad_id' => $model->id, 'student.student_flag_id' => 1])
->andFilterWhere(['between', 'created_at', $start, $end])
->asArray()
->all();
$countExam[0]['name'] = 'Mumtaz';
$countExam[1]['name'] = 'Jayyid Jiddan';
$countExam[2]['name'] = 'Jayyid';
$countExam[3]['name'] = 'Rasib';
for ($i = 0; $i < 12; $i++) {
    $countExam[0]['data'][$i] = 0;
    $countExam[1]['data'][$i] = 0;
    $countExam[2]['data'][$i] = 0;
    $countExam[3]['data'][$i] = 0;
}
foreach ($exam as $x) {
    $date = date('n', $x['created_at']);
    for($i = 0; $i < 12; $i++) {
        if ($x['score_id'] == 4) {
            if ($date == $i + 1) {
                $countExam[3]['data'][$i]++;
            }
        }
        else if ($x['score_id'] == 3)  {
            if ($date == $i + 1) {
                $countExam[2]['data'][$i]++;
            }
        }
        else if ($x['score_id'] == 2)  {
            if ($date == $i + 1) {
                $countExam[1]['data'][$i]++;
            }
        }
        else if ($x['score_id'] == 1)  {
            if ($date == $i + 1) {
                $countExam[0]['data'][$i]++;
            }
        }
    }
}

$month = CalendarMonth::find()->select('month')->asArray()->all();
$i = 0;
foreach ($month as $x) {
    $arrayMonth[$i] = ucfirst(strtolower($x['month']));
    $i++;
}

?>

<div class="mahad-view">
    <div class="flex-row row">
        <!--DETAIL MAHAD-->
        <div class="col-md-6">
            <div class="box box-success" style="height: 100%;">
                <div class="box-header"><h3 class="box-title"><i class="fas fa-sm fa-university"></i> <?= Html::encode($this->title) ?></h3></div>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'label' => 'Nama',
                            'value' => function($model) {
                                return ucwords(strtolower($model->name));
                            },
                        ],
                        [
                            'label' => 'Mudir',
                            'value' => function($model) {
                                return ucwords(strtolower($model->mudir));
                            },
                        ],
                        [
                            'label' => 'Jenis',
                            'value' => function($model) {
                                return ucwords(strtolower($model->mahadType->name));
                            },
                        ],
                        [
                            'label' => 'Provinsi',
                            'value' => function($model) {
                                return ucwords(strtolower($model->province->name));
                            },
                        ],
                        [
                            'label' => 'Daerah',
                            'value' => function($model) {
                                return ucwords(strtolower($model->regency->name));
                            },
                        ],
                        'address:ntext',
                    ],
                ]) ?>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if(Yii::$app->user->can('super') || Yii::$app->user->can('developer')): ?>
                                <div class="btn-group" style="padding: 2px 0;">
                                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-danger',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                </div>
                            <?php endif; ?>
                            <?php if(Yii::$app->user->can('admin') || Yii::$app->user->can('developer')): ?>
                                <div class="btn-group" style="padding: 2px 0;">
                                    <?= Html::a('<i class="fas fa-sm fa-sm fa-plus"></i> Halaqoh', ['/student/student-group-create', 'mahad_id' => $model->id], [
                                        'class' => 'btn btn-warning',
                                        'id' => 'addStudentGroupBtn',
                                    ]) ?>
                                    <?= Html::a('<i class="fas fa-sm fa-plus"></i> Ustadz', ['/teacher/create', 'mahad_id' => $model->id], [
                                        'class' => 'btn btn-danger',
                                        'id' => 'addTeacherBtn',
                                    ]) ?>
                                </div>
                            <?php endif; ?>
                            <?= Yii::$app->user->can('super') || Yii::$app->user->can('developer') ? (Html::a('<i class="fas fa-sm fa-plus"></i> Penilaian Ma\'had', ['/mahad-rating/create', 'mahad_id' => $model->id], [
                                'class' => 'btn btn-success',
                                'id' => 'addMahadRatingBtn',
                            ])):('') ?>
                        </div>
                    </div>
                </div>
            </div>                    
        </div>
        <!--INFORMATION BOX-->
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel no-padding no-margin" style="background-color: rgba(0, 0, 0, 0); box-shadow: none;">
                        <div class="panel-body no-padding">
                            <div class="row">
                                <!--STUDENT-->
                                <div class="col-md-6">
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3><?= Student::find()->where(['mahad_id' => $model->id, 'student_flag_id' => 1])->count(); ?></h3>
                                            <p>Santri</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-sm fa-user-graduate"></i>
                                        </div>
                                        <div class="small-box-footer"></div>
                                    </div>
                                </div>

                                <!--TEACHER-->
                                <div class="col-md-6">
                                    <div class="small-box bg-teal">
                                        <div class="inner">
                                            <h3><?= Teacher::find()->where(['mahad_id' => $model->id])->count(); ?></h3>
                                            <p>Ustadz</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fas fa-sm fa-chalkboard-teacher"></i>
                                        </div>
                                        <div class="small-box-footer"></div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <!--PASSED-->
                                <div class="col-md-6">
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3><?= $countGraduated = Student::find()->join('LEFT OUTER JOIN', 'pass_reason', 'pass_reason.student_id = student.id')->join('LEFT OUTER JOIN', 'pass_reason_description', 'pass_reason_description.id = pass_reason.pass_reason_description_id')->where(['student.student_flag_id' => 3,'pass_reason_description.pass_reason_description_flag_id' => 1, 'student.mahad_id' => $model->id])->count(); ?></h3>
                                            <p>Lulus</p>
                                        </div>
                                        <div class="icon">
                                          <i class="fas fa-sm fa-award"></i>
                                        </div>
                                        <div class="small-box-footer"></div>
                                    </div>
                                </div>

                                <!--UN-PASSED-->
                                <div class="col-md-6">
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3><?= $countGraduated = Student::find()->join('LEFT OUTER JOIN', 'pass_reason', 'pass_reason.student_id = student.id')->join('LEFT OUTER JOIN', 'pass_reason_description', 'pass_reason_description.id = pass_reason.pass_reason_description_id')->where(['student.student_flag_id' => 3,'pass_reason_description.pass_reason_description_flag_id' => 2, 'student.mahad_id' => $model->id])->count(); ?></h3>
                                            <p>Tidak Lulus</p>
                                        </div>
                                        <div class="icon">
                                          <i class="fas fa-sm fa-exclamation-circle"></i>
                                        </div>
                                        <div class="small-box-footer"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-success">
                                        <div class="box-header"><h3 class="box-title"><i class="fas fa-sm fa-award"></i> Akreditasi</h3></div>
                                        <div class="box-body">
                                            <?php if ($mahadQuality >= 50):?>
                                                <div class="progress-circle over50 p<?=(int)$mahadQuality?>" style="margin: 15px auto;">
                                                    <span><strong><?=$mahadQuality?>%</strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                            <?php else: ?>
                                                <div class="progress-circle p<?=(int)$mahadQuality?>" style="margin: 15px auto;">
                                                    <span><strong><?=$mahadQuality?>%</strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAHAD RATING-->
    <?php if($newestMahadRating != null): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header"><h3 class="box-title">Penjabaran Penialian</h3></div>
                    <div class="box-body">
                        <div class="col-md-1"></div>
                        <?php foreach($newestMahadRating as $x): ?>
                            <?php 
                                if ($x['abbr'] === 'M') {
                                    $bg = 'primary';
                                } else if ($x['abbr'] === 'JJ') {
                                    $bg = 'success';
                                } else if ($x['abbr'] === 'J') {
                                    $bg = 'warning';
                                } else {
                                    $bg = 'danger';
                                }
                            ?>
                            <div class="col-md-2">
                                <div class="box no-border">
                                    <div class="box-header text-center"><h5 class="box-title"><?= MahadRatingType::findOne($x['mahad_rating_type_id'])->name ?></h5></div>
                                    <div class="box-body">
                                        <div class="progress-circle bg-<?= $bg ?> over50 p<?=(int)$x['score']?>" style="margin: 15px auto;">
                                            <span><strong><?=$x['abbr']?></strong></span>
                                            <div class="left-half-clipper">
                                                <div class="first50-bar"></div>
                                                <div class="value-bar"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!--TABLE MAHAD RATING-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger box-solid">
            <div class="box-header with-border"><h5 class="box-title">Penilaian Ma'had</h5></div>
                <?php Pjax::begin(['id' => 'table-mahad-rating']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/mahad-rating', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => MahadRating::find()
                                ->join('LEFT OUTER JOIN', 'score', 'score.id = mahad_rating.score_id')
                                ->join('LEFT OUTER JOIN', 'comment', 'comment.id = mahad_rating.comment_id')
                                ->where(['mahad_id' => $model->id])
                                ->orderBy(['mahad_rating.created_at' => SORT_DESC]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>            
            </div>
        </div>
    </div>
    <!--LINE CHART-->
    <div class="row">
        <div class="col-md-12">
            <?= Highcharts::widget([
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting'
                ],
                'options' => [
                    'chart' => [
                        'type' => 'line'
                    ],
                    'title' => ['text' => "Data Keberhasilan Ujian Tahfiz Santri Tahun $year"],
                    'plotOptions' => [
                        'line' => [
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                            'enableMouseTracking' => true,
                        ],
                    ],
                    'xAxis' => [
                        'categories' => $arrayMonth,
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => 'Jumlah',
                        ],
                    ],
                    'series' => $countExam,
                    'credits' => ['enabled' => false],
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <!--TABLE STUDENT GROUP-->
        <div class="col-md-6">
            <div class="box box-warning">
                <div class="box-header with-border"><h5 class="box-title">Halaqoh</h5></div>
                <?php Pjax::begin(['id' => 'table-student-group']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/student-group', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => StudentGroup::find()
                                ->where(['mahad_id' => $model->id]),
                                'pagination' => [
                                    'pageSize' => false,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div> 
        <!--TABLE TEACHER-->
        <div class="col-md-6">
            <div class="box box-warning">
                <div class="box-header with-border"><h5 class="box-title">Musyrif</h5></div>
                <?php Pjax::begin(['id' => 'table-teacher']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/teacher', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Teacher::find()
                                ->where(['mahad_id' => $model->id]),
                                'pagination' => [
                                    'pageSize' => false,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <!--TABLE STUDENT-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border"><h5 class="box-title">Santri</h5></div>
                <?php Pjax::begin(['id' => 'table-student']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/student', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Student::find()
                                ->join('LEFT OUTER JOIN', 'mahad', 'mahad.id = student.mahad_id')
                                ->orderBy([
                                    'mahad.mahad_type_id' => SORT_ASC, 
                                    'mahad.province_id' => SORT_ASC, 
                                    'mahad.regency_id' => SORT_ASC,
                                    'mahad.name' => SORT_ASC,
                                    'student.student_group_id' => SORT_ASC,
                                    'student.name' => SORT_ASC,
                                    'student.student_graduate_id' => SORT_ASC,
                                    'student.doe' => SORT_ASC,
                                ])
                                ->where(['mahad_id' => $model->id])
                                ->andWhere(['student_flag_id' => 1]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <!--TABLE GRADUATED-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border"><h5 class="box-title">Alumni</h5></div>
                <?php Pjax::begin(['id' => 'table-student']) ?>
                    <div class="box-body">
                        <?= $this->render('_table/graduated', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Student::find()
                                ->join('LEFT OUTER JOIN', 'mahad', 'mahad.id = student.mahad_id')
                                ->orderBy([
                                    'mahad.mahad_type_id' => SORT_ASC, 
                                    'mahad.province_id' => SORT_ASC, 
                                    'mahad.regency_id' => SORT_ASC,
                                    'mahad.name' => SORT_ASC,
                                    'student.doe' => SORT_ASC,
                                    //'student.student_graduate_id' => SORT_ASC,
                                    //'student.student_group_id' => SORT_ASC,
                                    'student.name' => SORT_ASC,
                                ])
                                ->where(['mahad_id' => $model->id])
                                ->andWhere(['student_flag_id' => 3]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ]),
                        ]) ?>
                    </div>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="row">
        <!--LOCATION-->
        <div class="col-lg-4 col-sm-12">
            <div class="box box-solid box-success">
                <div class="box-body">
                    <?php
                    $regency = Regency::findOne($model->regency_id);
                    $link = $model->map;
                    $name = $model->name;
                    $center = new dosamigos\leaflet\types\LatLng(['lat' => $regency->latitude, 'lng' => $regency->longitude]);
                    $marker = new \dosamigos\leaflet\layers\Marker(['latLng' => $center, 'popupContent' => "<a href='$link' target='_blank'><strong>$name</strong><a>"]);

                    $leaflet = new \dosamigos\leaflet\LeafLet([
                        'name' => 'propertyMap',
                        'center' => $center, // set the center,
                        'zoom' => 10,
                    ]);
                    $tileLayer = new \dosamigos\leaflet\layers\TileLayer([
                        'urlTemplate' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    ]);
                    $leaflet
                    ->addLayer($marker)
                    ->addLayer($tileLayer);  // add the tile layer
                    $leaflet->appendJs('propertyMap.scrollWheelZoom.disable();'); // disable zoom
                    
                    echo \dosamigos\leaflet\widgets\Map::widget([
                        'leafLet' => $leaflet,
                    ]);
                    ?>  
                </div>

            </div>        
        </div>
    </div>
</div>

<?php
    Modal::begin([
        'id' => 'modal',
        'size' => 'modal-md',
    ]);
    echo "<div id='role-content'></div>";
    Modal::end();
    $this->registerJs("
        $(function(){
            $('#addMahadRatingBtn').click(function (){
                $.post($(this).attr('href'), function(data) {
                  $('#modal').modal('show').find('#role-content').html(data)
               });
               return false;
            });
        });
        $(function(){
            $('#addStudentGroupBtn').click(function (){
                $.post($(this).attr('href'), function(data) {
                  $('#modal').modal('show').find('#role-content').html(data)
               });
               return false;
            });
        });
        $(function(){
            $('#addTeacherBtn').click(function (){
                $.post($(this).attr('href'), function(data) {
                  $('#modal').modal('show').find('#role-content').html(data)
               });
               return false;
            });
        });
    ");
?>