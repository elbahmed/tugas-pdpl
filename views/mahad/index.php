<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MahadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Ma\'had';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mahad-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mahad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'mudir',
            [
                'label' => 'Jenis',
                'attribute' => 'mahadType.name', 
            ],
            [
                'label' => 'Provinsi',
                'attribute' => 'province.name', 
            ],
            [
                'label' => 'Daerah',
                'attribute' => 'regency.name', 
            ],
            //'map',
            //'address:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
        ],
    ]); ?>


</div>
