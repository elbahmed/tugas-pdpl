<?php

use app\models\Graduated;
use app\models\Student;
use app\models\StudentSearch;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Branches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="branch-view">

    <h1><?= Html::encode($this->title) ?></h1>

        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?php
        $data = new ActiveDataProvider([
                'query' => Student::find()->where(['branch_id' => $model->id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $search = new StudentSearch();
            Modal::begin([
                'header' => '<h2>Santri</h2>',
                'toggleButton' => ['label' => 'Santri', 'class' => 'btn  btn-success'],
            ]);
                echo GridView::widget([
                    'dataProvider' => $data,
                    'columns' => [
                        'name',
                        'doe' => [
                            'class' => 'yii\grid\DataColumn',
                            'label' => 'Tanggal Muqim',
                            'attribute' => 'doe',
                            'value' =>  function($model) {
                                            return date('d M Y', strtotime($model->__get('doe')));
                            },
                        ],
                    ],
                ]);
            Modal::end();
        ?>

        <?php
            $data = new ActiveDataProvider([
                'query' => Graduated::find()->where(['branch_id' => $model->id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $search = new StudentSearch();
            Modal::begin([
                'header' => '<h2>Alumni</h2>',
                'toggleButton' => ['label' => 'Alumni', 'class' => 'btn  btn-warning'],
            ]);
                echo GridView::widget([
                    'dataProvider' => $data,
                    'columns' => [
                        'name',
                        'doe' => [
                            'class' => 'yii\grid\DataColumn',
                            'label' => 'Tanggal Muqim',
                            'attribute' => 'doe',
                            'value' =>  function($model) {
                                            return date('d M Y', strtotime($model->__get('doe')));
                            },
                        ],
                        'dog' => [
                            'class' => 'yii\grid\DataColumn',
                            'label' => 'Tanggal Muqim',
                            'attribute' => 'doe',
                            'value' =>  function($model) {
                                            return date('d M Y', strtotime($model->__get('doe')));
                            },
                        ],
                    ],
                ]);
            Modal::end();
        ?>
        <div style="margin: 10px;"></div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'city.name',
            'province.name',
            'address:ntext',
            'maps',
            'name',
            'cheif',
            'active_student',
            'total_student',
        ],
    ])
    
    ?>

</div>
