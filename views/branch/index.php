<?php

use app\models\Province;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ma\'had';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Branch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="box-body table-responsive no-padding">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'id' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'id',
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'name',
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'cheif' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'cheif',
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'province.name' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'province.name',
                'label' => 'provinsi',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'province_id',
                    'data' => ArrayHelper::map(Province::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Provinsi...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'city.name' => [
                'class' => 'kartik\grid\DataColumn',
                'label' => 'Kota',
                'attribute' => 'city.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'city_id',
                    'data' => $city,
                    'options' => ['placeholder' => 'Kota/Kab...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]),
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            //'address:ntext',
            'maps' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'maps',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;',  'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'active_student' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'active_student',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],
            'total_student' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'total_student',
                'filter' => false,
                'contentOptions' => function ($model, $key, $index, $grid) {
                
                    return ['id' => $model['id'], 'style' => 'cursor: pointer;', 'onclick' => 'window.location.href = "'.Url::to(['view', 'id' => $model->id]).'";'];
                
                },
            ],

            ['class' => 'kartik\grid\ActionColumn'],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], 
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'beforeHeader'=>[
            [
                'columns'=>[
                    ['content'=> 'List Santri', 'options'=>['colspan'=>12, 'class'=>'text-center']],
                ], 
                'options'=>['class'=>'skip-export'] 
            ]
        ],
        'exportConfig' => [
            GridView::PDF => [
                'label' => 'Save as PDF',
                'pdfConfig' => [
                    'methods' => [
                        'SetTitle' => 'Ma\'had - Alaskar.com',
                        'SetSubject' => '',
                        'SetHeader' => ['Ma\'had Al Askar||Generated On: ' . date("r")],
                        'SetFooter' => ['|Page {PAGENO}|'],
                    ]
                ],
            ],
            GridView::EXCEL => ['label' => 'Save as EXCEL'], 
            GridView::CSV => ['label' => 'Save as CSV'], 
        ],
          
        'toolbar' =>  [
            '{export}', 
        ],
        'pjax' => true,
        'striped' => true,
        'condensed' => false,
        'hover' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'tableOptions' =>[

        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => 'Ma\'had',
        ],
    ]); ?>
</div>
    <?php Pjax::end(); ?>

</div>
