<?php

use app\models\Province;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branch-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Province::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Provinsi ...',
            'id' => 'province_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'city_id')->widget(DepDrop::className(), [
        'options'=>['id'=>'city_id'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['province_id'],
            'placeholder' => 'Pilih Kota/Kab...',
            'url' => Url::to(['/branch/city']),
        ],
    ]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'maps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cheif')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
