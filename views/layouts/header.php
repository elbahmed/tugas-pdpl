<?php

use app\models\users\User;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$user = User::findOne(Yii::$app->user->id);

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">MA</span><span class="logo-lg">Ma\'had Al Askar </span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/img/user.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= $user->nick_name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/img/user.png" class="img-circle" alt="User Image"/>
                            <p>
                                <?= $user->nick_name ?>
                                <small><?= $user->role ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a data-method="post" href="/users/change-password?id=<?= $user->id ?>" class="btn btn-default btn-flat changePasswordButton">Ubah Kata Sandi</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Keluar',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<?php 
    Modal::begin([
        'header' => 'Change Password',
        'id' => 'modal-change-password',
        'size' => 'modal-lg',
    ]);
    echo "<div id='change-password-content'></div>";
    Modal::end();

    $this->registerJs("
        $(function(){
            // changed id to class
            $('.changePasswordButton').click(function (){
                $.get($(this).attr('href'), function(data) {
                  $('#modal-change-password').modal('show').find('#change-password-content').html(data)
               });
               return false;
            });
        });
    ");
?>