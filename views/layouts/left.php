<?php

use app\models\users\User;

$user = User::findOne(Yii::$app->user->id);
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/user.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $user->nick_name ?></p>
                <small ><?= $user->role ?></small>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Beranda', 'icon' => 'home', 'url' => ['/']],
                    ['label' => 'Penilaian Santri', 'icon' => 'award', 'url' => ['/student-rating/select-mahad'], 'visible' => Yii::$app->user->can('developer') || Yii::$app->user->can('super'),],
                    [
                        'label' => 'Download Laporan',
                        'icon' => 'print',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('developer') || Yii::$app->user->can('super'),
                        'items' => [
                            ['label' => 'Laporan Ma\'had', 'iconType' => 'fa', 'icon' => 'award', 'url' => ['/report/mahad-pdf'],],
                            ['label' => 'Laporan Bulanan', 'iconType' => 'fa', 'icon' => 'calendar', 'url' => ['/report/monthly-pdf'],],
                            ['label' => 'Laporan Semester', 'iconType' => 'fa', 'icon' => 'calendar', 'url' => ['/report/semesterial-pdf'],],
                            ['label' => 'Laporan Tahunan', 'iconType' => 'fa', 'icon' => 'calendar', 'url' => ['/report/yearly-pdf'],],
                            
                        ],
                    ],
                    [
                        'label' => 'Configuration',
                        'icon' => 'cog',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('developer'),
                        'items' => [
                            ['label' => 'Users', 'iconType' => 'fa', 'icon' => 'users', 'url' => ['/user'],],
                            [
                                'label' => 'Log Activity',
                                'icon' => 'history',
                                'url' => '#',
                                'visible' => Yii::$app->user->can('developer'),
                                'items' => [
                                    ['label' => 'Login Log', 'icon' => 'sign-in-alt', 'url' => ['/log/login'],],
                                    ['label' => 'Transaction Log', 'icon' => 'exchange-alt', 'url' => ['/log/transaction'],],
                                ]
                            ]
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
