<?php
    if (!Yii::$app->user->isGuest) {
        NavBar::begin([
            'brandLabel' => '<img width="40rem" style="margin-right: 10px;" src="'.Url::to('@web/images/logo.jpeg').'" class="pull-left img-circle"/><div>Ma\'had Al Askar</div>' ,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse nabvar navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['site/index']],
                ['label' => "Ma'had", 'url' => ['/branch/index']],
                ['label' => "Santri", 'url' => ['/student']],
                ['label' => "Alumni", 'url' => ['/graduated']],
                Yii::$app->user->can('super') ? (
                        ['label' => 'Registration', 'url' => ['buat/akun']]
                    
                ) : (
                    [
                        'label' => 'Registrasi', 
                        'url' => ['/site/registration'], 
                        'visible' => false
                    ]
                ),
                '<li>'
                .'</li>'. 
                '<li class="dropdown show">
                    <a class="fa waves-effect waves-dark fa-user dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <div class="dw-user-box dropdown-item">
                            <div class="text-muted">Assalamu\'alaikum</div>
                            <div class="u-text">
                                <h4>'. Yii::$app->user->identity->username .'</h4>
                        </div>
                        <div role="separator" class="divider"></div>
                        <div class="dropdown-item"><a href="/student/create">Input Santri</a></div>
                        <div class="dropdown-item"><a href="/ziyadah/create">Input Ziyadah</a></div>
                        <div class="dropdown-item">'. 
                            Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    'Logout',
                                    ['class' => ' logout']
                                )
                            .Html::endForm().
                        '</div>
                    </div>
                </li>'
            ],
        ]);
        NavBar::end();
    }
    ?>