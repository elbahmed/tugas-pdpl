<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="panel-body">
        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <div class="container">
            <?= $content ?>
        </div>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2020-2021 <a href="http://alaskar.id">Ma'had Al Askar</a>.</strong> All rights
    reserved. <a href="/copyright/disclaimer"><strong>Disclaimer</strong></a>. <a href="/copyright/terms-conditions">Our terms and conditions</a>
</footer>

<?php
$js = <<< JS
    $(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");
JS;

$this->registerJs($js, yii\web\View::POS_READY);

$js = <<< JS
    $(document).ready(function() {
        setTimeout(function() {
            $('.site-loader').fadeOut("slow");
        }, 1000);
    });
JS;

$this->registerJs($js, yii\web\View::POS_READY);