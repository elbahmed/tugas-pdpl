<?php

use app\models\Branch;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['/site/registration'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'confirmation_token',
            //'status',
            'superadmin' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'superadmin',
                'value' => function($model) {
                    $query = Yii::$app->db->createCommand(" 
                        SELECT description FROM auth_assignment a 
                        LEFT OUTER JOIN auth_item i on i.name = a.item_name
                        WHERE a.user_id = $model->id
                    ")->queryAll();
                    return $query[0]['description'];
                },
            ],
            //'created_at',
            'branch' => [
                'class' => 'kartik\grid\DataColumn',
                'attribute' => 'superadmin',
                'value' => function($model) {
                    if (($query = Branch::findOne(['id' => $model->branch_id])) !== null) {
                        return $query->name;
                    }
                    return '-';
                },
            ],

            ['class' => 'kartik\grid\ActionColumn'],
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'floatHeader' => false,
    ]); ?>

    <?php Pjax::end(); ?>

</div>
