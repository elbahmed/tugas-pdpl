<?php

use app\models\Comment;
use app\traits\TimeTrait;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="table-student-exam">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Jenis Ujian',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->studentExamType->studentExamTypeCategory->name .' '. $dataProvider->studentExamType->name));
                }
            ],
            [
                'label' => 'Juz',
                'value' => function($dataProvider) {
                    return 'Juz '. $dataProvider->start .' - '. $dataProvider->end;
                }
            ],
            [
                'label' => 'Nilai',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->score->name));
                }
            ],
            [
                'label' => 'Musyrif',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->teacher->name));
                }
            ],
            [
                'format' => 'ntext',
                'label' => 'Komentar',
                'value' => function($dataProvider) {
                    if ($dataProvider->comment_id != null) {
                        return Comment::findOne($dataProvider->comment_id)->comment;
                    }
                    return '-';
                }
            ],
            [
                'label' => 'Tanggal',
                'value' => function($dataProvider) {
                    return TimeTrait::translateDate($dataProvider->created_at);
                }
            ],
            [                
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($url, $dataProvider, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/student-exam/delete', 'id' => $dataProvider->id],[
                            'data-method' => 'post',
                            'data-confirm' => 'Apakah anda yakin ingin menghapus item ini?',
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
</div>