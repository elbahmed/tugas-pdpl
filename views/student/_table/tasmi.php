<?php

use app\models\Comment;
use yii\grid\GridView;
use app\traits\TimeTrait;
use yii\helpers\Html;

?>

<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Progres',
                'value' => function($dataProvider) {
                    return ucwords(strtolower($dataProvider->tasmiProgress->name));
                }
            ],
            [
                'label' => 'Juz',
                'value' => 'tasmiProgress.juz'
            ],
            [
                'label' => 'Musyrif',
                'attribute' => 'teacher.name',
            ],
            [
                'format' => 'text',
                'label' => 'Komentar',
                'value' => function($dataProvider) {
                    if ($dataProvider->comment_id != null) {
                        return Comment::findOne($dataProvider->comment_id)->comment;
                    }
                    return '-';
                }
            ],
            [
                'label' => 'Tanggal',
                'value' => function($dataProvider) {
                    return TimeTrait::translateDate($dataProvider->created_at);
                }
            ],
            [                
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 6%; text-align: center;'],
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($url, $dataProvider, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/tasmi/delete', 'id' => $dataProvider->id],[
                            'data-method' => 'post',
                            'data-confirm' => 'Apakah anda yakin ingin menghapus item ini?',
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
</div>