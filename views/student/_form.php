<?php

use app\models\CalendarDate;
use app\models\CalendarMonth;
use app\models\CalendarYear;
use app\models\Education;
use app\models\Province;
use app\models\Regency;
use app\models\Scholarship;
use app\models\StudentGroup;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pob')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <label>Tanggal Lahir</label>
        <div class="row">
            <div class="col-md-3">
                <?= Select2::widget([
                    'name' => 'dob-date-id',
                    'data' => ArrayHelper::map(CalendarDate::find()->all(), 'id', 'date'),
                    'value' => $dobDate,
                    'options' => [
                        'placeholder' => 'Tanggal...',
                        'id' => 'dob-date-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-5">
                <?= Select2::widget([
                    'name' => 'dob-month-id',
                    'data' => ArrayHelper::map(CalendarMonth::find()->all(), 'id', 'month'),
                    'value' => $dobMonth,
                    'options' => [
                        'placeholder' => 'Bulan...',
                        'id' => 'dob-month-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= Select2::widget([
                    'name' => 'dob-year-id',
                    'data' => ArrayHelper::map(CalendarYear::find()->where(['<=', 'year',  date('Y', time())])->all(), 'id', 'year'),
                    'value' => $dobYear,
                    'options' => [
                        'placeholder' => 'Tahun...',
                        'id' => 'dob-year-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'province_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Province::find()->all(), 'id', 'name'),
        'value' => $model->province_id,
        'options' => [
            'placeholder' => 'Pilih Provinsi...',
            'id' => 'province-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'regency_id')->widget(DepDrop::className(), [
        'data' => ArrayHelper::map(Regency::find()->where(['province_id' => $model->province_id])->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Daerah...',
            'id' => 'regency-id',
        ],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
            'depends' => ['province-id'],
            'url' => Url::to(['regency-depdrop'])
        ]
    ]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6, 'placeholder' => 'Alamat...']) ?>

    <?= $form->field($model, 'education_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Education::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Pendidikan Terakhir...',
            'id' => 'education-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'phone_parent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'scholarship_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Scholarship::find()->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Beasiswa...',
            'id' => 'scholarship-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'student_group_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(StudentGroup::find()->where(['mahad_id' => $model->mahad_id])->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Halaqoh Santri...',
            'id' => 'student-group-id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'doe')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'photo')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
