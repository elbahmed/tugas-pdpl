<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Student', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'class' => 'table-responsive'
        ],
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'label' => "Ma'had",
                'attribute' => 'mahad.name',
            ],
            [
                'label' => 'Beasiswa',
                'attribute' => 'scholarship.name',
            ],
            [
                'label' => "Angkatan",
                'attribute' => 'studentGraduate.year',
            ],
            [
                'label' => 'Level',
                'value' => function($model) {
                    return 'Ke - '. round((time() - $model->doe) / (3600*24*365.242199));
                }
            ],
            [
                'label' => "Halaqoh",
                'attribute' => 'studentGroup.name',
            ],
            //'regency_id',
            //'province_id',
            //'education_id',
            //'student_flag_id',
            //'nisn',
            //'school_origin',
            //'school_origin_graduate',
            //'address:ntext',
            //'father',
            //'mother',
            //'phone_parent',
            //'phone',
            //'email:email',
            //'pob',
            //'dob',
            //'doe',
            //'photo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
