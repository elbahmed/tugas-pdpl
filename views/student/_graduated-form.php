<?php

use app\models\Mahad;
use app\models\PassReasonDescription;
use app\models\Student;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

?>

<div class="student-mahad-form">
    <?php $form = ActiveForm::begin() ?>

        <div class="form-group">
            <label>Santri</label>
            <div readonly class="form-control"><?= Student::findOne($model->student_id)->name ?></div>
        </div>
        
        <?= $form->field($model, 'pass_reason_description_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map(PassReasonDescription::find()->all(), 'id', 'description'),
            'options' => [
                'placeholder' => 'Pilih Alasan Kelulusan...',
                'id' => 'pass-reason-description-id',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    <?php ActiveForm::end() ?>

</div>