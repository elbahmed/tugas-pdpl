<?php

use app\models\CalendarMonth;
use app\models\Mahad;
use app\models\Score;
use app\models\StudentExam;
use app\models\StudentGraduate;
use app\models\StudentRating;
use app\models\Tasmi;
use app\models\TasmiProgress;
use app\models\Ziyadah;
use app\traits\TimeTrait;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = ucwords(strtolower($model->name));
$mahad = Mahad::findOne($model->mahad_id);
$this->params['breadcrumbs'][] = ['label' => ucwords(strtolower($mahad->name)), 'url' => ['/mahad/view', 'id' => $mahad->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$yearTahfiz = null;
$yearTahsin = null;
$yearTasmi  = null;
$examTahfiz = null;
$examTahfiz = null;
$tasmi      = null;

if(($examTahfiz = StudentExam::find()
->select("score.name AS name, student_exam_type.name AS type, student_exam_type_category.name AS category, score_id, score.score, score.abbr, student_exam.created_at")
->join('LEFT OUTER JOIN', 'score', 'score.id = student_exam.score_id')
->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam_type.id = student_exam.student_exam_type_id')
->join('LEFT OUTER JOIN', 'student_exam_type_category', 'student_exam_type_category.id = student_exam_type.student_exam_type_category_id')
->where(['student_id' => $model->id, 'student_exam_type.student_exam_type_category_id' => 2])->asArray()->orderBy('student_exam.created_at ASC')->all()) != null) {
    $i = 0;
    foreach ($examTahfiz as $z) {
        $yearTahfiz[$i] = date('Y', $z['created_at']);
        $i++;
    }
    $yearTahfiz = array_unique($yearTahfiz);
}

if(($examTahsin = StudentExam::find()
->select("score.name AS name, student_exam_type.name AS type, student_exam_type_category.name AS category, score_id, score.score, score.abbr, student_exam.created_at")
->join('LEFT OUTER JOIN', 'score', 'score.id = student_exam.score_id')
->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam_type.id = student_exam.student_exam_type_id')
->join('LEFT OUTER JOIN', 'student_exam_type_category', 'student_exam_type_category.id = student_exam_type.student_exam_type_category_id')
->where(['student_id' => $model->id, 'student_exam_type.student_exam_type_category_id' => 1])->asArray()->orderBy('student_exam.created_at ASC')->all()) != null) {
    $i = 0;
    foreach ($examTahsin as $z) {
        $yearTahsin[$i] = date('Y', $z['created_at']);
        $i++;
    }
    $yearTahsin = array_unique($yearTahsin);
}

if(($tasmi = Tasmi::find()
->where(['student_id' => $model->id])->asArray()->orderBy('tasmi_progress_id ASC')->all()) != null) {
    $i = 0;
    foreach ($tasmi as $z) {
        $yearTasmi[$i] = date('Y', $z['created_at']);
        $i++;
    }
    $yearTasmi = array_unique($yearTasmi);
}

$studentRatingKelancaran = 0;
$studentRatingMakhroj= 0;
$modelStudentRatingKelancaran = StudentRating::find()->where(['student_rating_type_id' => 1])->andWhere(['student_id' => $model->id])->all();
$modelStudentRatingMakhroj = StudentRating::find()->where(['student_rating_type_id' => 2])->andWhere(['student_id' => $model->id])->all();
if ($modelStudentRatingKelancaran != null) {
    $i = 0;
    foreach ($modelStudentRatingKelancaran as $x) {
        $studentRatingKelancaran = $studentRatingKelancaran + Score::findOne($x['score_id'])->score;
        $i++;
    }
    $studentRatingKelancaran = round($studentRatingKelancaran / $i);
}

if ($modelStudentRatingMakhroj!= null) {
    $i = 0;
    foreach ($modelStudentRatingMakhroj as $x) {
        $studentRatingMakhroj = $studentRatingMakhroj + Score::findOne($x['score_id'])->score;
        $i++;
    }
    $studentRatingMakhroj = round($studentRatingMakhroj / $i);
}

 //COUNT EXAM
    $year = date('Y');
    $start = strtotime("01 - jan - $year");
    $end = time();
    $exam = StudentExam::find()
    ->join('LEFT OUTER JOIN', 'student', 'student_exam.student_id = student.id')
    ->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam.student_exam_type_id = student_exam_type.id')
    ->where(['student_exam_type.student_exam_type_category_id' => 2, 'student.student_flag_id' => 1])
    ->andFilterWhere(['between', 'created_at', $start, $end])
    ->asArray()
    ->all();
    $countExam[0]['name'] = 'Mumtaz';
    $countExam[1]['name'] = 'Jayyid Jiddan';
    $countExam[2]['name'] = 'Jayyid';
    $countExam[3]['name'] = 'Rasib';
    for ($i = 0; $i < 12; $i++) {
        $countExam[0]['data'][$i] = 0;
        $countExam[1]['data'][$i] = 0;
        $countExam[2]['data'][$i] = 0;
        $countExam[3]['data'][$i] = 0;
    }
    foreach ($exam as $x) {
        $date = date('n', $x['created_at']);
        for($i = 0; $i < 12; $i++) {
            if ($x['score_id'] == 4) {
                if ($date == $i + 1) {
                    $countExam[3]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 3)  {
                if ($date == $i + 1) {
                    $countExam[2]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 2)  {
                if ($date == $i + 1) {
                    $countExam[1]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 1)  {
                if ($date == $i + 1) {
                    $countExam[0]['data'][$i]++;
                }
            }
        }
    }

    //COUNT EXAM
    $year = date('Y');
    $start = strtotime("01 - jan - $year");
    $end = time();
    $exam = StudentExam::find()
    ->join('LEFT OUTER JOIN', 'student', 'student_exam.student_id = student.id')
    ->join('LEFT OUTER JOIN', 'student_exam_type', 'student_exam.student_exam_type_id = student_exam_type.id')
    ->where(['student.id' => $model->id, 'student_exam_type.student_exam_type_category_id' => 2])
    ->andFilterWhere(['between', 'created_at', $start, $end])
    ->asArray()
    ->all();
    $countExam[0]['name'] = 'Mumtaz';
    $countExam[1]['name'] = 'Jayyid Jiddan';
    $countExam[2]['name'] = 'Jayyid';
    $countExam[3]['name'] = 'Rasib';
    for ($i = 0; $i < 12; $i++) {
        $countExam[0]['data'][$i] = 0;
        $countExam[1]['data'][$i] = 0;
        $countExam[2]['data'][$i] = 0;
        $countExam[3]['data'][$i] = 0;
    }
    foreach ($exam as $x) {
        $date = date('n', $x['created_at']);
        for($i = 0; $i < 12; $i++) {
            if ($x['score_id'] == 4) {
                if ($date == $i + 1) {
                    $countExam[3]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 3)  {
                if ($date == $i + 1) {
                    $countExam[2]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 2)  {
                if ($date == $i + 1) {
                    $countExam[1]['data'][$i]++;
                }
            }
            else if ($x['score_id'] == 1)  {
                if ($date == $i + 1) {
                    $countExam[0]['data'][$i]++;
                }
            }
        }
    }

    //COUNT ZIYADAH
    $year = date('Y');
    $start = strtotime("01 - jan - $year");
    $end = time();
    $ziyadah = Ziyadah::find()
    ->select(['created_at, COUNT(DISTINCT quran_page.page) AS count_page, COUNT(DISTINCT quran_juz.juz) AS count_juz'])
    ->join('LEFT OUTER JOIN', 'quran_page', 'quran_page.id BETWEEN ziyadah.start AND ziyadah.end')
    ->join('LEFT OUTER JOIN', 'quran_juz', 'quran_juz.id = quran_page.quran_juz_id')
    ->asArray()
    ->groupBy('quran_juz.juz')
    ->where(['student_id' => $model->id])
    ->andFilterWhere(['between', 'created_at', $start, $end])
    ->orderBy(['quran_juz.juz' => SORT_ASC])
    ->all();

    $countZiyadah[0]['name'] = 'Ziyadah';
    for ($i = 0; $i < 12; $i++) {
        $countZiyadah[0]['data'][$i] = 0;
    }

    foreach ($ziyadah as $x) {
        $date = date('n', $x['created_at']);
        for($i = 0; $i < 12; $i++) {
            if ($date == $i + 1) {
                $countZiyadah[0]['data'][$i] = $countZiyadah[0]['data'][$i] + $x['count_page'];
            }
        }
    }

    $month = CalendarMonth::find()->select('month')->asArray()->all();
    $i = 0;
    foreach ($month as $x) {
        $arrayMonth[$i] = ucfirst(strtolower($x['month']));
        $i++;
    }
?>
<div class="student-view">
    <!--DETAIL STUDENT-->
    <div class="box box-solid box-primary collapsed-box">
        <div class="box-header no-padding">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-tools pull-right">
                       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                       </button>
                    </div>
                </div>
            </div>
            <div class="row" style="box-shadow: none; border-radius: 0%;">
                <div class="col-md-12" style="box-shadow: none; border-radius: 0%;">
                    <div class="box box-widget widget-user no-margin" style="box-shadow: none; border-radius: 0%;">
                        <div class="widget-user-header bg-light-blue" style="box-shadow: none; border-radius: 0%;">
                            <h3 class="widget-user-username"><?= $model->name ?></h3>
                            <h5 class="widget-user-desc"><?= $model->id ?></h5>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?='https://form.alaskar.id/'.$model->photo ?>" alt="User Avatar" style="height: 91px; object-fit: cover; cursor: zoom-in;" data-enlargable width="100">
                        </div>
                        <div class="box-header"></div>
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <span class="description-text">Hafalan</span>
                                        <h5 class="description-header"><?= $model->totalZiyadah() ?></h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <span class="description-text">Level</span>
                                        <h5 class="description-header"><?= 'Ke - '. round((time() - $model->doe) / (3600*24*365.242199)) ?></h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <span class="description-text">Angkatan</span>
                                        <h5 class="description-header"><?= StudentGraduate::findOne($model->student_graduate_id)->year ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="box-body table-responsive">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'label' => "Nama",
                        'attribute' => 'name',
                    ],
                    [
                        'label' => "NISN",
                        'attribute' => 'nisn',
                    ],
                    [
                        'label' => "Sekolah Asal",
                        'attribute' => 'school_origin',
                    ],
                    [
                        'label' => "Tahun Kelulusan",
                        'attribute' => 'school_origin_graduate',
                    ],
                    [
                        'label' => 'Pendidikan Terakhir',
                        'value' => function($model) {
                            return ucwords(strtolower($model->education->name));
                        },
                    ],
                    [
                        'label' => "Tempat Lahir",
                        'attribute' => 'pob',
                    ],
                    [
                        'label' => 'Umur',
                        'value' => function($model) {
                            return round((time() - $model->dob) / (3600*24*365.242199)) . ' Tahun';
                        }
                    ],
                    [
                        'label' => "Nomer Telpon",
                        'attribute' => 'phone',
                    ],
                    'email:email',
                    [
                        'label' => 'Daerah',
                        'value' => function($model) {
                            return ucwords(strtolower($model->regency->name));
                        },
                    ],
                    [
                        'label' => 'Provinsi',
                        'value' => function($model) {
                            return ucwords(strtolower($model->province->name));
                        },
                    ],
                    [
                        'format' => 'ntext',
                        'label' => "alamat",
                        'attribute' => 'address',
                    ],
                    [
                        'label' => "Ayah",
                        'attribute' => 'father',
                    ],
                    [
                        'label' => "Ibu",
                        'attribute' => 'mother',
                    ],
                    [
                        'label' => "Nomer Telpon Orang Tua",
                        'attribute' => 'phone_parent',
                    ],
                    [
                        'label' => 'Status',
                        'value' => function($model) {
                            return ucwords(strtolower($model->studentFlag->name));
                        },
                    ],
                    [
                        'label' => 'Ma\'had',
                        'value' => function($model) {
                            return ucwords(strtolower($model->mahad->name));
                        },
                    ],
                    [
                        'label' => 'Beasiswa',
                        'value' => function($model) {
                            return ucwords(strtolower($model->scholarship->name));
                        },
                    ],
                    [
                        'label' => "Angkatan",
                        'attribute' => 'studentGraduate.year',
                    ],
                    [
                        'label' => "Halaqoh",
                        'attribute' => 'studentGroup.name',
                    ],
                    [
                        'label' => 'Level',
                        'value' => function($model) {
                            return 'Ke - '. round((time() - $model->doe) / (3600*24*365.242199));
                        }
                    ],
                    [
                        'attribute' => 'doe',
                        'value' => function($model) {
                            return TimeTrait::translateDate($model->doe);
                        }
                    ],
                ],
            ]) ?>

        </div>
        <div class="box-footer">
            <div class="pull-right">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Lulus', ['graduated-form', 'id' => $model->id], ['class' => 'btn btn-success updateStudentGraduated']) ?>
                <?= Html::a('Pindah', ['mahad-form', 'id' => $model->id], ['class' => 'btn btn-warning updateStudentMahad']) ?>
            </div>
        </div>
    </div>

    <!--INFORMATION BOX-->
    <div class="row">
        <!--LAST NEW EXAM TAHFIZ-->
        <div class="col-sm-3">
            <div class="small-box bg-light-blue">
                <div class="inner">
                    <h3 class="inline"><?= $examTahfiz != null ? (round((time() - end($examTahfiz)['created_at']) / (60 * 60 * 24)) . '</h3><h4 class="inline"><strong> Hari Lalu</strong></h4>'):('-</h3>') ?>
                    <p>Terakhir Ujian Tahfiz</p>
                </div>
                <div class="icon">
                    <i class="fas fa-sm fa-quran"></i>
                </div>
            </div>
        </div>

        <!--TOTAL EXAM TAHFIZ-->
        <div class="col-sm-3">
            <div class="small-box bg-aqua">
                <div class="inner">
                <h3 class="inline"><?= count($examTahfiz) ?></h3><h2 class="inline"><strong>X</strong></h2>
                    <p>Total Ujian Tahfiz</p>
                </div>
                <div class="icon">
                    <i class="fas fa-sm fa-award"></i>
                </div>
            </div>
        </div>

        <!--LAST NEW EXAM TAHSIN-->
        <div class="col-sm-3">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3 class="inline"><?= $examTahsin != null ? (round((time() - end($examTahsin)['created_at']) / (60 * 60 * 24)) . '</h3><h4 class="inline"><strong> Hari Lalu</strong></h4>'):('-</h3>') ?>
                    <p>Terakhir Ujian Tahsin</p>
                </div>
                <div class="icon">
                    <i class="fas fa-sm fa-quran"></i>
                </div>
            </div>
        </div>

        <!--TOTAL EXAM TAHSIN-->
        <div class="col-sm-3">
            <div class="small-box bg-lime-active">
                <div class="inner">
                <h3 class="inline"><?= count($examTahsin) ?></h3><h2 class="inline"><strong>X</strong></h2>
                    <p>Total Ujian Tahsin</p>
                </div>
                <div class="icon">
                    <i class="fas fa-sm fa-award"></i>
                </div>
            </div>
        </div>
    </div>

    <!--STUDENT RATING-->
    <div class="row">
        <!--KELANCARAN-->
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header"><h3 class="box-title"><i class="fas fa-sm fa-award"></i> Kelancaran</h3></div>
                <div class="box-body">
                    <?php if ($studentRatingKelancaran >= 80):?>
                        <div class="progress-circle over50 p<?=(int)$studentRatingKelancaran?> bg-success" style="margin: 15px auto;">
                            <span><strong><?=$studentRatingKelancaran?>%</strong></span>
                            <div class="left-half-clipper">
                                <div class="first50-bar"></div>
                                <div class="value-bar"></div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php if ($studentRatingKelancaran >= 50):?>
                            <div class="progress-circle over50 p<?=(int)$studentRatingKelancaran?> bg-danger" style="margin: 15px auto;">
                                <span><strong><?=$studentRatingKelancaran?>%</strong></span>
                                <div class="left-half-clipper">
                                    <div class="first50-bar"></div>
                                    <div class="value-bar"></div>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="progress-circle p<?=(int)$studentRatingKelancaran?> bg-danger" style="margin: 15px auto;">
                                <span><strong><?=$studentRatingKelancaran?>%</strong></span>
                                <div class="left-half-clipper">
                                    <div class="first50-bar"></div>
                                    <div class="value-bar"></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <!--MAKHROJ-->
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header"><h3 class="box-title"><i class="fas fa-sm fa-award"></i> Makhroj</h3></div>
                <div class="box-body">
                    <?php if ($studentRatingMakhroj >= 80):?>
                        <div class="progress-circle over50 p<?=(int)$studentRatingMakhroj?> bg-success" style="margin: 15px auto;">
                            <span><strong><?=$studentRatingMakhroj?>%</strong></span>
                            <div class="left-half-clipper">
                                <div class="first50-bar"></div>
                                <div class="value-bar"></div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php if ($studentRatingMakhroj >= 50):?>
                            <div class="progress-circle over50 p<?=(int)$studentRatingMakhroj?> bg-danger" style="margin: 15px auto;">
                                <span><strong><?=$studentRatingMakhroj?>%</strong></span>
                                <div class="left-half-clipper">
                                    <div class="first50-bar"></div>
                                    <div class="value-bar"></div>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="progress-circle p<?=(int)$studentRatingMakhroj?> bg-danger" style="margin: 15px auto;">
                                <span><strong><?=$studentRatingMakhroj?>%</strong></span>
                                <div class="left-half-clipper">
                                    <div class="first50-bar"></div>
                                    <div class="value-bar"></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!--TIMELINE EXAM TAHFIZ-->
    <?php if($yearTahfiz == null || count($yearTahfiz) == 0): ?>
        <div class="box box-success">
            <div class="box-header with-border"><h1 class="box-title">Ujian Tahfiz</h1></div>
            <div class="box-body row">
                <div class="col-md-12 text-center"><p>Santri belum pernah ujian tahfiz.</p></div>
            </div>
        </div>
    <?php else: ?>
        <?php foreach($yearTahfiz as $y): ?>
            <?php 
            $rand = rand(1, 3);
            if ($rand == 1) {
                $box = 'warning';
            } elseif ($rand == 2) {
                $box = 'success';
            } else {
                $box = 'primary';
            }
            ?>
            <div class="box box-<?= $box ?> collapsed-box">
                <div class="box-header with-border">
                    <h1 class="box-title">Hasil Ujian Tahfiz <?= $this->title .' Tahun '. $y?></h1>
                    <div class="box-tools pull-right">
                       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                       </button>
                    </div>
                </div>
                <div class="row box-body">
                    <div style="margin: auto;">
                        <?php $i = 1; ?>
                        <?php $iterate = 1?>
                        <?php foreach($examTahfiz as $x): ?>
                            <?php 
                                if ($i == count($examTahfiz)) {
                                    $day = time() - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                } else {
                                    $day = $examTahfiz[$i]['created_at'] - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                }

                                if ($x['abbr'] === 'M') {
                                    $bg = 'primary';
                                } else if ($x['abbr'] === 'JJ') {
                                    $bg = 'success';
                                } else if ($x['abbr'] === 'J') {
                                    $bg = 'warning';
                                } else {
                                    $bg = 'danger';
                                }
                            ?>
                            <?php if (date('Y', $x['created_at']) == $y):  ?>
                                <?php if ($iterate % 4 == 1): ?>
                                    <?php if($iterate != count($examTahfiz)): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-heading text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif ($iterate == count($examTahfiz) && $iterate % 4 != 0): ?>
                                    <div class="col-md-3">
                                        <div class="panel">
                                            <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                            <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                            <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                            <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                            <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                <div class="left-half-clipper">
                                                    <div class="first50-bar"></div>
                                                    <div class="value-bar"></div>
                                                </div>
                                            </div>
                                            <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                            <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                        </div>
                                    </div>
                                <?php else :?>
                                    <?php if ($iterate % 4 == 0): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right _last"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded _last"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $iterate++; ?>
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <!--TIMELINE EXAM TAHSIN-->
    <?php if($yearTahsin == null || count($yearTahsin) == 0): ?>
        <div class="box box-success">
            <div class="box-header with-border"><h1 class="box-title">Ujian Tahsin</h1></div>
            <div class="box-body row">
                <div class="col-md-12 text-center"><p>Santri belum pernah ujian tahsin.</p></div>
            </div>
        </div>
    <?php else: ?>
        <?php foreach($yearTahsin as $y): ?>
            <?php 
            $rand = rand(1, 3);
            if ($rand == 1) {
                $box = 'warning';
            } elseif ($rand == 2) {
                $box = 'success';
            } else {
                $box = 'primary';
            }
            ?>
            <div class="box box-<?= $box ?> collapsed-box">
                <div class="box-header with-border">
                    <h1 class="box-title">Hasil Ujian Tahsin <?= $this->title .' Tahun '. $y?></h1>
                    <div class="box-tools pull-right">
                       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                       </button>
                    </div>
                </div>
                <div class="row box-body">
                    <div style="margin: auto;">
                        <?php $i = 1; ?>
                        <?php $iterate = 1?>
                        <?php foreach($examTahsin as $x): ?>
                            <?php 
                                if ($i == count($examTahsin)) {
                                    $day = time() - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                } else {
                                    $day = $examTahsin[$i]['created_at'] - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                }

                                if ($x['abbr'] === 'M') {
                                    $bg = 'primary';
                                } else if ($x['abbr'] === 'JJ') {
                                    $bg = 'success';
                                } else if ($x['abbr'] === 'J') {
                                    $bg = 'warning';
                                } else {
                                    $bg = 'danger';
                                }
                            ?>
                            <?php if (date('Y', $x['created_at']) == $y):  ?>
                                <?php if ($iterate % 4 == 1): ?>
                                    <?php if($iterate != count($examTahsin)): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-heading text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif ($iterate == count($examTahsin ) && $iterate % 4 != 0): ?>
                                    <div class="col-md-3">
                                        <div class="panel">
                                            <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                            <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                            <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                            <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                            <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                <div class="left-half-clipper">
                                                    <div class="first50-bar"></div>
                                                    <div class="value-bar"></div>
                                                </div>
                                            </div>
                                            <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                            <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                        </div>
                                    </div>
                                <?php else :?>
                                    <?php if ($iterate % 4 == 0): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right _last"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded _last"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="panel-header text-center"><?= ucwords(strtolower($x['category'] .' '. $x['type'])) ?></div>
                                                <div class="progress-circle bg-<?=$bg?> over50 p<?=(int)Score::findOne($x['score_id'])->score?>" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=Score::findOne($x['score_id'])->abbr?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower($x['name'])) ?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $iterate++; ?>
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <!--TIMELINE TASMI-->
    <?php if($yearTasmi== null || count($yearTasmi) == 0): ?>
        <div class="box box-success">
            <div class="box-header with-border">
                <h1 class="box-title">Ujian Tasmi</h1>
            </div>
            <div class="box-body row">
                <div class="col-md-12 text-center"><p>Santri belum pernah tasmi.</p></div>
            </div>
        </div>
    <?php else: ?>
        <?php foreach($yearTasmi as $y): ?>
            <?php 
            $rand = rand(1, 3);
            if ($rand == 1) {
                $box = 'info';
            } elseif ($rand == 2) {
                $box = 'success';
            } else {
                $box = 'primary';
            }
            ?>
            <div class="box box-<?= $box ?> collapsed-box">
                <div class="box-header with-border">
                    <h1 class="box-title">Hasil Tasmi <?= $this->title .' Tahun '. $y?></h1>
                    <div class="box-tools pull-right">
                       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-plus"></i>
                       </button>
                    </div>
                </div>
                <div class="row box-body">
                    <div style="margin: auto;">
                        <?php $i = 1; ?>
                        <?php $iterate = 1?>
                        <?php foreach($tasmi as $x): ?>
                            <?php 
                                if ($i == count($tasmi)) {
                                    $day = time() - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                } else {
                                    $day = $tasmi[$i]['created_at'] - $x['created_at'];
                                    $dev = round($day / (60 * 60 * 24)) . ' Hari';
                                }

                                $random = rand(1, 4);
                                if ($random == 1) {
                                    $bg = 'primary';
                                } else if ($random == 2) {
                                    $bg = 'success';
                                } else if ($random == 3) {
                                    $bg = 'warning';
                                } else {
                                    $bg = 'danger';
                                }
                            ?>
                            <?php if (date('Y', $x['created_at']) == $y):  ?>
                                <?php if ($iterate % 4 == 1): ?>
                                    <?php if($iterate != count($tasmi)): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="progress-circle bg-<?=$bg?> over50 p100" style="margin: 15px auto;">
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <span><strong><?=TasmiProgress::findOne($x['tasmi_progress_id'])->juz?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower(TasmiProgress::findOne($x['tasmi_progress_id'])->name ))?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                                <div class="panel-footer text-center"><a href="<?= $x['video_link'] ?>" target="_blank">Tonton Proses Tasmi</a></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="progress-circle bg-<?=$bg?> over50 p100" style="margin: 15px auto;">
                                                    <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                    <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=TasmiProgress::findOne($x['tasmi_progress_id'])->juz?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower(TasmiProgress::findOne($x['tasmi_progress_id'])->name ))?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                                <div class="panel-footer text-center"><a href="<?= $x['video_link'] ?>" target="_blank">Tonton Proses Tasmi</a></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif ($iterate == count($tasmi) && $iterate % 4 != 0): ?>
                                    <div class="col-md-3">
                                        <div class="panel">
                                            <div class="progress-circle bg-<?=$bg?> over50 p100" style="margin: 15px auto;">
                                            <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                            <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                            <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <span><strong><?=TasmiProgress::findOne($x['tasmi_progress_id'])->juz?></strong></span>
                                                <div class="left-half-clipper">
                                                    <div class="first50-bar"></div>
                                                    <div class="value-bar"></div>
                                                </div>
                                            </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower(TasmiProgress::findOne($x['tasmi_progress_id'])->name ))?></div>
                                            <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                                <div class="panel-footer text-center"><a href="<?= $x['video_link'] ?>" target="_blank">Tonton Proses Tasmi</a></div>
                                        </div>
                                    </div>
                                <?php else :?>
                                    <?php if ($iterate % 4 == 0): ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="progress-circle bg-<?=$bg?> over50 p100" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right _last"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded _last"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=TasmiProgress::findOne($x['tasmi_progress_id'])->juz?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower(TasmiProgress::findOne($x['tasmi_progress_id'])->name ))?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                                <div class="panel-footer text-center"><a href="<?= $x['video_link'] ?>" target="_blank">Tonton Proses Tasmi</a></div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-md-3">
                                            <div class="panel">
                                                <div class="progress-circle bg-<?=$bg?> over50 p100" style="margin: 15px auto;">
                                                <div class="progress-sm _progress-right"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                <div class="_rounded"><span class="date"><?=$dev?></span></div>
                                                <div class="progress-sm _progress-left"><div class="progress-bar progress-bar-aqua" style="width: 100%;"></div></div>
                                                    <span><strong><?=TasmiProgress::findOne($x['tasmi_progress_id'])->juz?></strong></span>
                                                    <div class="left-half-clipper">
                                                        <div class="first50-bar"></div>
                                                        <div class="value-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer text-center"><?= ucwords(strtolower(TasmiProgress::findOne($x['tasmi_progress_id'])->name ))?></div>
                                                <div class="panel-footer text-center"><?= TimeTrait::translateDate($x['created_at']); ?></div>
                                                <div class="panel-footer text-center"><a href="<?= $x['video_link'] ?>" target="_blank">Tonton Proses Tasmi</a></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $iterate++; ?>
                            <?php endif; ?>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    
    <!--LINE CHART EXAM-->
    <div class="row">
        <div class="col-md-12">
            <?= Highcharts::widget([
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting'
                ],
                'options' => [
                    'chart' => [
                        'type' => 'line'
                    ],
                    'title' => ['text' => "Data Keberhasilan Ujian Tahfiz $model->name Tahun $year"],
                    'plotOptions' => [
                        'line' => [
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                            'enableMouseTracking' => true,
                        ],
                    ],
                    'xAxis' => [
                        'categories' =>$arrayMonth,
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => 'Jumlah',
                        ],
                    ],
                    'series' => $countExam,
                    'credits' => ['enabled' => false],
                ],
            ]) ?>
        </div>
    </div>

    <!--LINE CHART ZIYADAH-->
    <div class="row">
        <div class="col-md-12">
            <?= Highcharts::widget([
                'scripts' => [
                    'highcharts-more',
                    'modules/exporting'
                ],
                'options' => [
                    'chart' => [
                        'type' => 'line'
                    ],
                    'title' => ['text' => "Perkembangan Ziyadah $model->name Tahun $year"],
                    'plotOptions' => [
                        'line' => [
                            'dataLabels' => [
                                'enabled' => true,
                            ],
                            'enableMouseTracking' => true,
                        ],
                    ],
                    'xAxis' => [
                        'categories' => $arrayMonth,
                    ],
                    'yAxis' => [
                        'title' => [
                            'text' => 'Jumlah',
                        ],
                    ],
                    'series' => $countZiyadah,
                    'credits' => ['enabled' => false],
                ],
            ]) ?>
        </div>
    </div>

    <!--ZIYADAH-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success box-solid">
                <div class="box-header"><h3 class="box-title">Ziyadah</h3></div>
                <div class="box-body">
                    <?php Pjax::begin(['id' => 'table-ziyadah']) ?>
                        <?= $this->render('_table/ziyadah', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Ziyadah::find()
                                ->where(['student_id' => $model->id])
                                ->orderBy(['created_at' => SORT_DESC, 'start' => SORT_ASC]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ])
                        ]) ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>

    <!--STUDENT EXAM-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info box-solid">
                <div class="box-header"><h3 class="box-title">Hasil Ujian</h3></div>
                <div class="box-body">
                    <?php Pjax::begin(['id' => 'table-student-exam']) ?>
                        <?= $this->render('_table/student-exam', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => StudentExam::find()
                                ->where(['student_id' => $model->id])
                                ->orderBy(['created_at' => SORT_DESC, 'start' => SORT_ASC]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ])
                        ]) ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>   
    
    <!--STUDENT RATING-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning box-solid">
                <div class="box-header"><h3 class="box-title">Hasil Penilaian</h3></div>
                <div class="box-body">
                    <?php Pjax::begin(['id' => 'table-student-rating']) ?>
                        <?= $this->render('_table/student-rating', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => StudentRating::find()
                                ->where(['student_id' => $model->id])
                                ->orderBy(['created_at' => SORT_DESC]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ])
                        ]) ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>  
    
    <!--Tasmi-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box-solid">
                <div class="box-header"><h3 class="box-title">Hasil Tasmi</h3></div>
                <div class="box-body">
                    <?php Pjax::begin(['id' => 'table-tasmi']) ?>
                        <?= $this->render('_table/tasmi', [
                            'dataProvider' => new ActiveDataProvider([
                                'query' => Tasmi::find()
                                ->where(['student_id' => $model->id])
                                ->orderBy(['tasmi_progress_id' => SORT_ASC]),
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                                'sort' => false
                            ])
                        ]) ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div> 
</div>

<?php 
Modal::begin([
    'id' => 'modal',
    'size' => 'modal-md',
]);
echo "<div id='role-content'></div>";
Modal::end();

$this->registerJs("
    $('img[data-enlargable]').addClass('img-enlargable').click(function(){
        var src = $(this).attr('src');
        var modal;
        function removeModal(){ modal.remove(); $('body').off('keyup.modal-close'); }
        modal = $('<div>').css({
            background: 'RGBA(0,0,0,.5) url('+src+') no-repeat center',
            backgroundSize: 'contain',
            width:'100%', height:'100%',
            position:'fixed',
            zIndex:'10000',
            top:'0', left:'0',
            cursor: 'zoom-out',
        }).click(function(){
            removeModal();
        }).appendTo('body');
        //handling ESC
        $('body').on('keyup.modal-close', function(e){
          if(e.key==='Escape'){ removeModal(); } 
        });
    });
    $(function(){
        $('.updateStudentMahad').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
    $(function(){
        $('.updateStudentGraduated').click(function (){
            $.post($(this).attr('href'), function(data) {
              $('#modal').modal('show').find('#role-content').html(data)
           });
           return false;
        });
    });
")
?>