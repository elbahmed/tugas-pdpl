<?php 

?>
<div class="table-responsive">
    <table class="table-bordered text-center" style="color: #fff; float: none; margin: 0 auto 10px auto;">
        <?php $i = 0; ?>
        <?php for ($i = 0; $i < count($result); $i++): ?>
            <?php if ($i == 0): ?>
                <tr><td class='td-<?= $result[$i]['bg'] ?>'>Juz <?= $result[$i]['juz'] ?></td>
            <?php elseif ($i == count($result)): ?>
                <td class='td-<?= $result[$i]['bg'] ?>'>Juz <?= $result[$i]['juz'] ?></td></tr>
            <?php elseif (($i + 1) % 5 == 0): ?>
                <td class='td-<?= $result[$i]['bg'] ?>'>Juz <?= $result[$i]['juz'] ?></td></tr><tr>
            <?php else: ?>
                <td class="td-<?= $result[$i]['bg'] ?>">Juz <?= $result[$i]['juz'] ?></td>
            <?php endif; ?>
        <?php endfor; ?>
    </table>
    <table>
        <th><i>NB</i>:</th>
        <tr>
            <td>
                <div class="label label-success text-center">Telah Selesai</div>
                <div class="label label-warning text-center">Dalam Progres</div>
                <div class="label label-danger text-center">Belum di Hafal</div>
            </td>
        </tr>
    </table>
</div>