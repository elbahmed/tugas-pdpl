<?php

use app\models\Branch;
use app\models\User;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tasmi */
/* @var $form ActiveForm */

$this->title = 'Input Tasmi';
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tasmi">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="formtasmi">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'branch_id')->widget(Select2::className(), [
                'data' => !Yii::$app->user->can('super') ? (
                    ArrayHelper::map(Branch::find()->where(['id' => User::findOne(Yii::$app->user->getId())->branch_id])->all(),'id', 'name')
                ) : (
                    ArrayHelper::map(Branch::find()->all(),'id', 'name')
                ),
                'options' => [
                    'placeholder' => "Pilih Ma'had...",
                    'id' => 'branch_id',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ])->label("Ma'had") ?>

            <?= $form->field($model, 'id')->widget(DepDrop::className(), [
                'options'=>['id'=>'id'],
                'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions' => [
                    'depends' => ['branch_id'],
                    'placeholder' => 'Pilih Santri...',
                    'url' => Url::to(['/student/student']),
                ],
            ])?>

            <?= $form->field($model, 'tasmi_id')->widget(DepDrop::className(), [
                'options'=>['id'=>'tasmi_id'],
                'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions' => [
                    'depends' => ['id'],
                    'placeholder' => 'Pilih Progres Tasmi...',
                    'url' => Url::to(['/student/progrestasmi']),
                ],
            ])?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>

    </div><!-- tasmi-_form -->
</div>
