<?php

use app\models\Teacher;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentRating */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-group-create">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mahad_id')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'teacher_id')->widget(Select2::className(), [
        'data' => ArrayHelper::map(Teacher::find()->where(['mahad_id' => $model->mahad_id])->all(), 'id', 'name'),
        'options' => [
            'placeholder' => 'Pilih Ustadz Pengawas...',
            'id' => 'teacher_id',
        ],
        'pluginOptions' => [
            'allowClear' => true
        ]
    ]) ?>

    <?= $form->field($model, 'name')->textInput()->label('Nama Halaqoh') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
