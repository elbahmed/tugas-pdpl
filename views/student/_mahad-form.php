<?php

use app\models\Mahad;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

?>

<div class="student-mahad-form">
    <?php $form = ActiveForm::begin() ?>

        <div class="form-group">
            <label>Santri</label>
            <div readonly class="form-control"><?= $model->name ?></div>
        </div>
        
        <?= $form->field($model, 'mahad_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map(Mahad::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => 'Pilih Ma\'had...',
                'id' => 'mahad-id',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ]
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    <?php ActiveForm::end() ?>

</div>