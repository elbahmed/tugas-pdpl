<?php

use yii\db\Migration;

/**
 * Class m200429_031138_rbac_init
 */
class m200429_031138_rbac_init extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        
        // add "stackholder" permission
        $supervisor = $auth->createRole('supervisor');
        $supervisor->description = 'Superisor Access';
        $auth->add($supervisor);

        // add "admin" role and give this role the "Access By ID" permission
        $admin = $auth->createRole('admin');
        $admin->description = 'Admin Access';
        $auth->add($admin);

        // add "super" role and give this role the "All Access" permission
        $super = $auth->createRole('super');
        $super->description = 'The Highest Access';
        $auth->add($super);

    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }
}
