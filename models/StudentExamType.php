<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_exam_type".
 *
 * @property int $id
 * @property string $name
 * @property int $quality
 * @property int $student_exam_type_category_id
 *
 * @property StudentExam[] $studentExams
 * @property StudentExamTypeCategory $studentExamTypeCategory
 */
class StudentExamType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_exam_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'quality', 'student_exam_type_category_id'], 'required'],
            [['quality', 'student_exam_type_category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['student_exam_type_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentExamTypeCategory::className(), 'targetAttribute' => ['student_exam_type_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quality' => 'Quality',
            'student_exam_type_category_id' => 'Student Exam Type Category ID',
        ];
    }

    /**
     * Gets query for [[StudentExams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExams()
    {
        return $this->hasMany(StudentExam::className(), ['student_exam_type_id' => 'id']);
    }

    /**
     * Gets query for [[StudentExamTypeCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExamTypeCategory()
    {
        return $this->hasOne(StudentExamTypeCategory::className(), ['id' => 'student_exam_type_category_id']);
    }
}
