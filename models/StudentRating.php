<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_rating".
 *
 * @property int $id
 * @property int $student_rating_type_id
 * @property int $score_id
 * @property string $student_id
 * @property int $teacher_id
 * @property int $comment_id
 * @property int $created_at
 *
 * @property Comment $comment
 * @property Score $score
 * @property Student $student
 * @property StudentRatingType $studentRatingType
 * @property Teacher $teacher
 */
class StudentRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_rating_type_id', 'score_id', 'student_id', 'teacher_id', 'comment_id', 'created_at'], 'required'],
            [['student_rating_type_id', 'score_id', 'teacher_id', 'comment_id', 'created_at'], 'integer'],
            [['student_id'], 'string', 'max' => 255],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['score_id'], 'exist', 'skipOnError' => true, 'targetClass' => Score::className(), 'targetAttribute' => ['score_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
            [['student_rating_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentRatingType::className(), 'targetAttribute' => ['student_rating_type_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_rating_type_id' => 'Student Rating Type ID',
            'score_id' => 'Score ID',
            'student_id' => 'Student ID',
            'teacher_id' => 'Teacher ID',
            'comment_id' => 'Comment ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * Gets query for [[Score]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScore()
    {
        return $this->hasOne(Score::className(), ['id' => 'score_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * Gets query for [[StudentRatingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatingType()
    {
        return $this->hasOne(StudentRatingType::className(), ['id' => 'student_rating_type_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }
}
