<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_exam_type_category".
 *
 * @property int $id
 * @property int $name
 * @property int $quality
 *
 * @property StudentExamType[] $studentExamTypes
 */
class StudentExamTypeCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_exam_type_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'quality'], 'required'],
            [['name', 'quality'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quality' => 'Quality',
        ];
    }

    /**
     * Gets query for [[StudentExamTypes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExamTypes()
    {
        return $this->hasMany(StudentExamType::className(), ['student_exam_type_category_id' => 'id']);
    }
}
