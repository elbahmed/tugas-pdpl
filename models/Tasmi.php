<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasmi".
 *
 * @property int $id
 * @property string $student_id
 * @property int $teacher_id
 * @property int $tasmi_progress_id
 * @property int $comment_id
 * @property string $video_link
 * @property int $created_at
 *
 * @property Comment $comment
 * @property TasmiProgress $tasmiProgress
 * @property Teacher $teacher
 * @property Student $student
 */
class Tasmi extends \yii\db\ActiveRecord
{
    public $comment;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasmi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tasmi_progress_id'], 'required'],
            [['teacher_id', 'tasmi_progress_id', 'comment_id', 'created_at'], 'integer'],
            [['student_id', 'video_link'], 'string', 'max' => 255],
            [['comment', 'comment_id', 'video_link', 'created_at', 'student_id', 'teacher_id'], 'safe'],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['tasmi_progress_id'], 'exist', 'skipOnError' => true, 'targetClass' => TasmiProgress::className(), 'targetAttribute' => ['tasmi_progress_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Santri',
            'teacher_id' => 'Musyrif',
            'tasmi_progress_id' => 'Progres Tasmi',
            'comment_id' => 'Komentar',
            'video_link' => 'Link Video',
            'created_at' => 'Tanggal',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->comment != null || $this->comment != '') {
                $comment = new Comment();
                $comment->created_at = $this->created_at;
                $comment->comment = $this->comment;
                if ($comment->save(false)) {
                    $this->comment_id = $comment->id;
    
                    return true;
                }
        
            }
        } else {
            return false;
        }
        
        return true;
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * Gets query for [[TasmiProgress]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmiProgress()
    {
        return $this->hasOne(TasmiProgress::className(), ['id' => 'tasmi_progress_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }
}
