<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "teacher".
 *
 * @property int $id
 * @property int $teacher_position_id
 * @property string $mahad_id
 * @property int $education_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property int $total_tahfiz
 * @property int $dob
 * @property int $doe
 *
 * @property MahadRating[] $mahadRatings
 * @property StudentExam[] $studentExams
 * @property StudentGroup[] $studentGroups
 * @property StudentRating[] $studentRatings
 * @property Tasmi[] $tasmis
 * @property Education $education
 * @property TeacherPosition $teacherPosition
 * @property Mahad $mahad
 * @property Ziyadah[] $ziyadahs
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_position_id', 'mahad_id', 'education_id', 'name', 'email', 'phone', 'total_tahfiz'], 'required'],
            [['teacher_position_id', 'education_id', 'total_tahfiz'], 'integer'],
            [['doe', 'dob'], 'safe'],
            [['mahad_id', 'name', 'email', 'phone'], 'string', 'max' => 255],
            [['education_id'], 'exist', 'skipOnError' => true, 'targetClass' => Education::className(), 'targetAttribute' => ['education_id' => 'id']],
            [['teacher_position_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeacherPosition::className(), 'targetAttribute' => ['teacher_position_id' => 'id']],
            [['mahad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mahad::className(), 'targetAttribute' => ['mahad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_position_id' => 'Jabatan',
            'mahad_id' => 'Ma\'had',
            'education_id' => 'Pendidikan Terakhir',
            'name' => 'Nama',
            'email' => 'Email',
            'phone' => 'Nomer Telpon',
            'total_tahfiz' => 'Hafalan',
            'dob' => 'Tanggal Lahir',
            'doe' => 'Tanggal Muqim',
        ];
    }

    /**
     * Gets query for [[MahadRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatings()
    {
        return $this->hasMany(MahadRating::className(), ['teacher_id' => 'id']);
    }

    /**
     * Gets query for [[StudentExams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExams()
    {
        return $this->hasMany(StudentExam::className(), ['teacher_id' => 'id']);
    }

    /**
     * Gets query for [[StudentGroups]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroups()
    {
        return $this->hasMany(StudentGroup::className(), ['teacher_id' => 'id']);
    }

    /**
     * Gets query for [[StudentRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatings()
    {
        return $this->hasMany(StudentRating::className(), ['teacher_id' => 'id']);
    }

    /**
     * Gets query for [[Tasmis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmis()
    {
        return $this->hasMany(Tasmi::className(), ['teacher_id' => 'id']);
    }

    /**
     * Gets query for [[Education]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEducation()
    {
        return $this->hasOne(Education::className(), ['id' => 'education_id']);
    }

    /**
     * Gets query for [[TeacherPosition]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherPosition()
    {
        return $this->hasOne(TeacherPosition::className(), ['id' => 'teacher_position_id']);
    }

    /**
     * Gets query for [[Mahad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahad()
    {
        return $this->hasOne(Mahad::className(), ['id' => 'mahad_id']);
    }

    /**
     * Gets query for [[Ziyadahs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZiyadahs()
    {
        return $this->hasMany(Ziyadah::className(), ['teacher_id' => 'id']);
    }
}
