<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_rating_type".
 *
 * @property int $id
 * @property string $name
 * @property int $min_score
 * @property int $quality
 *
 * @property StudentRating[] $studentRatings
 */
class StudentRatingType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_rating_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'min_score', 'quality'], 'required'],
            [['min_score', 'quality'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'min_score' => 'Min Score',
            'quality' => 'Quality',
        ];
    }

    /**
     * Gets query for [[StudentRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatings()
    {
        return $this->hasMany(StudentRating::className(), ['student_rating_type_id' => 'id']);
    }
}
