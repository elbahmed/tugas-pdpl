<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_exam".
 *
 * @property int $id
 * @property string $student_id
 * @property int $teacher_id
 * @property int $score_id
 * @property int $student_exam_type_id
 * @property int $comment_id
 * @property int $created_at
 * @property string $start
 * @property string $end
 *
 * @property Comment $comment
 * @property Score $score
 * @property StudentExamType $studentExamType
 * @property Teacher $teacher
 * @property Student $student
 */
class StudentExam extends \yii\db\ActiveRecord
{
    public $comment;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_exam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['score_id', 'student_exam_type_id', 'start', 'end'], 'required'],
            [['teacher_id', 'score_id', 'student_exam_type_id', 'comment_id', 'created_at'], 'integer'],
            [['comment', 'comment_id'], 'safe'],
            [['student_id', 'start', 'end'], 'string', 'max' => 255],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['score_id'], 'exist', 'skipOnError' => true, 'targetClass' => Score::className(), 'targetAttribute' => ['score_id' => 'id']],
            [['student_exam_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentExamType::className(), 'targetAttribute' => ['student_exam_type_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Santri',
            'teacher_id' => 'Musyrif P.J.',
            'score_id' => 'Nilai',
            'student_exam_type_id' => 'Tipe Ujian',
            'comment_id' => 'Komentar',
            'created_at' => 'Tanggal',
            'start' => 'Dari',
            'end' => 'Sampai',
            'comment' => 'Komentar',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->comment != null || $this->comment != '') {
                $comment = new Comment();
                $comment->created_at = $this->created_at;
                $comment->comment = $this->comment;
                if ($comment->save(false)) {
                    $this->comment_id = $comment->id;
    
                    return true;
                }
        
            }
        } else {
            return false;
        }
        
        return true;
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * Gets query for [[Score]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScore()
    {
        return $this->hasOne(Score::className(), ['id' => 'score_id']);
    }

    /**
     * Gets query for [[StudentExamType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExamType()
    {
        return $this->hasOne(StudentExamType::className(), ['id' => 'student_exam_type_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }
}
