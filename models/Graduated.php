<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "graduated".
 *
 * @property string $id ID
 * @property string $branch_id Mah'ad
 * @property int $city_id Kota
 * @property int $province_id Provinsi
 * @property string $name Nama
 * @property string $address Alamat
 * @property int|null $ziyadah Total Hafalan
 * @property int|null $tasmi_id Total Tasmi
 * @property string $desc Alasan Keluar
 * @property string $dob Tanggal Lahir
 * @property string $doe Tanggal Masuk
 * @property string $dog Tanggal Lulus
 *
 * @property Branch $branch
 * @property City $city
 * @property Province $province
 * @property Tasmi $tasmi
 */
class Graduated extends \yii\db\ActiveRecord
{   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'graduated';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'branch_id', 'city_id', 'province_id', 'name', 'address', 'desc', 'dob', 'doe', 'dog'], 'required'],
            [['city_id', 'province_id', 'tasmi_id'], 'integer'],
            [['address'], 'string'],
            [['dob', 'doe', 'dog'], 'safe'],
            [['id', 'branch_id', 'name', 'desc', 'ziyadah'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branch_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
            [['tasmi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasmi::className(), 'targetAttribute' => ['tasmi_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['createByStudent'] = ['id', 'branch_id', 'city_id', 'province_id', 'name', 'ziyadah', 'tasmi_id', 'address', 'desc', 'dob', 'doe', 'tasmi_id'];
        $scenarios['create'] = ['branch_id', 'city_id', 'province_id', 'name', 'ziyadah', 'tasmi_id', 'address', 'desc', 'dob', 'doe', 'dog'];

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_id' => 'Mah\'ad',
            'city_id' => 'Kota',
            'province_id' => 'Provinsi',
            'name' => 'Nama',
            'address' => 'Alamat',
            'ziyadah' => 'Total Hafalan',
            'tasmi_id' => 'Progres Tasmi 5 juz',
            'desc' => 'Alasan Keluar',
            'dob' => 'Tanggal Lahir',
            'doe' => 'Tanggal Masuk',
            'dog' => 'Tanggal Lulus',
        ];
    }

    /**
     * Gets query for [[Branch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['id' => 'branch_id']);
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * Gets query for [[Tasmi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmi()
    {
        return $this->hasOne(Tasmi::className(), ['id' => 'tasmi_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {

                if ($this->dog == null) {
                    $this->dog = date('Y-m-d');
                    return true;
                }

                $branch = new Branch();
                $branch->scenario = 'updateGraduated';
                $branch = $branch->findOne($this->branch_id);
                $branch->total_student = $branch->total_student + 1;
                $branch->save();
                $date = new DateTime($this->doe);
                $this->id = sprintf('%07s%4s%04s', $this->branch_id, $date->format('y'), $branch->total_student);
                $this->id = preg_replace('/\s+/', '', $this->id);

                return true;
            }
            return true;
        }

        return false;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $branch = new Branch();
        $branch = $branch->findOne($this->branch_id);
        $branch->total_student = $branch->total_student - 1;
        $branch->save();

        return true;
    }
}
