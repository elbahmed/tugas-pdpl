<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "branch".
 *
 * @property string $id ID
 * @property int $city_id Kota
 * @property int $province_id Provinsi
 * @property string $address Alamat
 * @property string|null $maps Lokasi Maps
 * @property string $name Nama
 * @property string $cheif Mudir
 * @property int $active_student Santri Aktif
 * @property int $total_student Total Santri
 *
 * @property City $city
 * @property Province $province
 * @property Graduated[] $graduateds
 * @property Student[] $students
 */
class Branch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'province_id', 'address', 'name', 'cheif'], 'required'],
            [['city_id', 'province_id', 'active_student', 'total_student'], 'integer'],
            [['address'], 'string'],
            [['id', 'maps', 'name', 'cheif'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['city_id', 'province_id', 'address', 'name', 'cheif', 'maps'];
        $scenarios['updateStudent'] = ['active_student', 'total_student'];
        $scenarios['updateGraduated'] = ['total_student'];


        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'Kota',
            'province_id' => 'Provinsi',
            'address' => 'Alamat',
            'maps' => 'Lokasi Maps',
            'name' => 'Nama',
            'cheif' => 'Mudir',
            'active_student' => 'Santri Aktif',
            'total_student' => 'Total Santri',
        ];
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCity($province = null)
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * Gets query for [[Graduateds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGraduateds()
    {
        return $this->hasMany(Graduated::className(), ['branch_id' => 'id']);
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['branch_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $province = new Province();
                $province = $province->findOne($this->province_id);
                $province->total_branch = $province->total_branch + 1;
                $this->id = sprintf('%04s%03s',$this->city_id, $province->total_branch);

                $province->save();
                
                return true;
            }

            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $student = new Student();
        $graduated = new Graduated();
        $user = new User();
        $province = new Province();

        $province = $province->findOne($this->province_id);
        $province->total_branch = $province->total_branch - 1;
        
        $student->deleteAll("branch_id = $this->id");
        $graduated->deleteAll("branch_id = $this->id");
        $user->deleteAll("branch_id = $this->id");

        return true;
    }
}