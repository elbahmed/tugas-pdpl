<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "score".
 *
 * @property int $id
 * @property string $name
 * @property string $abbr
 * @property int $score
 *
 * @property MahadRating[] $mahadRatings
 * @property StudentExam[] $studentExams
 * @property StudentRating[] $studentRatings
 */
class Score extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'abbr', 'score'], 'required'],
            [['score'], 'integer'],
            [['name', 'abbr'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'abbr' => 'Abbr',
            'score' => 'Score',
        ];
    }

    /**
     * Gets query for [[MahadRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatings()
    {
        return $this->hasMany(MahadRating::className(), ['score_id' => 'id']);
    }

    /**
     * Gets query for [[StudentExams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExams()
    {
        return $this->hasMany(StudentExam::className(), ['score_id' => 'id']);
    }

    /**
     * Gets query for [[StudentRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatings()
    {
        return $this->hasMany(StudentRating::className(), ['score_id' => 'id']);
    }
}
