<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pass_reason".
 *
 * @property int $id
 * @property string $student_id
 * @property int $pass_reason_description_id
 *
 * @property Student $student
 * @property PassReasonDescription $passReasonDescription
 */
class PassReason extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pass_reason';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'pass_reason_description_id'], 'required'],
            [['pass_reason_description_id'], 'integer'],
            [['student_id'], 'string', 'max' => 255],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
            [['pass_reason_description_id'], 'exist', 'skipOnError' => true, 'targetClass' => PassReasonDescription::className(), 'targetAttribute' => ['pass_reason_description_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Santri',
            'pass_reason_description_id' => 'Alasan Keluar',
        ];
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * Gets query for [[PassReasonDescription]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassReasonDescription()
    {
        return $this->hasOne(PassReasonDescription::className(), ['id' => 'pass_reason_description_id']);
    }
}
