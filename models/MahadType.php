<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahad_type".
 *
 * @property int $id
 * @property string $name
 *
 * @property Mahad[] $mahads
 */
class MahadType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahad_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Mahads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahads()
    {
        return $this->hasMany(Mahad::className(), ['mahad_type_id' => 'id']);
    }
}
