<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ziyadah".
 *
 * @property int $id
 * @property int $start
 * @property int $end
 * @property int $teacher_id
 * @property string $student_id
 * @property int $comment_id
 * @property int $created_at
 *
 * @property Comment $comment
 * @property Student $student
 * @property Teacher $teacher
 */
class Ziyadah extends \yii\db\ActiveRecord
{
    public $comment;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ziyadah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start', 'end', 'teacher_id', 'student_id'], 'required'],
            [['start', 'end', 'teacher_id', 'comment_id', 'created_at'], 'integer'],
            [['student_id'], 'string', 'max' => 255],
            [['comment'], 'safe'],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start' => 'Dari',
            'end' => 'Sampai',
            'teacher_id' => 'Musyrif',
            'student_id' => 'Santri',
            'comment_id' => 'Komentar',
            'created_at' => 'Tanggal',
            'comment'    => 'Komentar'
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->comment != null) {
                $comment = new Comment();
                $comment->created_at = $this->created_at;
                $comment->comment = $this->comment;
                if ($comment->save(false)) {
                    $this->comment_id = $comment->id;
    
                    return true;
                }
        
            }
        } else {
            return false;
        }
        
        return true;
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * Gets query for [[Student]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }
}
