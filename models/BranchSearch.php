<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Branch;
use Yii;

/**
 * BranchSearch represents the model behind the search form of `app\models\Branch`.
 */
class BranchSearch extends Branch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'address', 'maps', 'name', 'cheif'], 'safe'],
            [['city_id', 'province_id', 'active_student', 'total_student'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->user->can('admin')) {
            $branch_id = User::findOne(Yii::$app->user->getId())->branch_id;
            $query = Branch::find()->where(['id' => $branch_id]);
        } else {
            $query = Branch::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 
			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'city_id' => $this->city_id,
            'province_id' => $this->province_id,
            'active_student' => $this->active_student,
            'total_student' => $this->total_student,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'maps', $this->maps])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'cheif', $this->cheif]);

        return $dataProvider;
    }
}
