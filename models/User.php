<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $confirmation_token
 * @property int $status
 * @property int|null $superadmin
 * @property string $created_at
 * @property string|null $branch_id
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    // holds the password confirmation word
    public $initialPassword;
    
    //will hold the encrypted password for update actions.
    public $confirmPassword;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'created_at', 'initialPassword', 'confirmPassword'], 'required'],
            [['status', 'superadmin'], 'integer'],
            [['created_at'], 'safe'],
            [['username', 'password_hash', 'confirmation_token', 'branch_id'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'initialPassword'],
            [['initialPassword'],'string', 'min' => 5, 'max' => 32]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username', 'initialPassword', 'confirmPassword', 'superadmin', 'branch_id'];
        $scenarios['login'] = ['username', 'initialPassword'];

        return $scenarios;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {

            if ($this->superadmin == 0) {
                $auth = Yii::$app->authManager;
                $roleObject = $auth->getRole('super');
                $auth->assign($roleObject, $this->id);
            }

            else if ($this->superadmin == 1) {

                $auth = Yii::$app->authManager;
                $roleObject = $auth->getRole('supervisor');
                $auth->assign($roleObject, $this->id);
            }

            else if ($this->superadmin == 2) {

                $auth = Yii::$app->authManager;
                $roleObject = $auth->getRole('admin');
                $auth->assign($roleObject, $this->id);
            }
        }
        
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->genarateAuthKey();
                $this->hashPassword();
                $this->created_at = date('Y-m-d H:i:s');
                
                return true;
            }

            return false;
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $auth = Yii::$app->authManager;
        $roleObject = $auth->getRole('admin');
        $auth->revoke($roleObject, $this->id);
        
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'confirmation_token' => 'Confirmation Token',
            'status' => 'Status',
            'superadmin' => 'Superadmin',
            'created_at' => 'Created At',
            'branch_id' => 'Branch ID',
            'initialPassword' => 'Password',
            'confirmPassword' => 'Confirm Password',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function genarateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function hashPassword()
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->initialPassword);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
}
