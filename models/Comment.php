<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $comment
 * @property int $created_at
 *
 * @property MahadRating[] $mahadRatings
 * @property StudentExam[] $studentExams
 * @property StudentRating[] $studentRatings
 * @property Tasmi[] $tasmis
 * @property Ziyadah[] $ziyadahs
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment', 'created_at'], 'required'],
            [['comment'], 'string'],
            [['created_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[MahadRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatings()
    {
        return $this->hasMany(MahadRating::className(), ['comment_id' => 'id']);
    }

    /**
     * Gets query for [[StudentExams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExams()
    {
        return $this->hasMany(StudentExam::className(), ['comment_id' => 'id']);
    }

    /**
     * Gets query for [[StudentRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatings()
    {
        return $this->hasMany(StudentRating::className(), ['comment_id' => 'id']);
    }

    /**
     * Gets query for [[Tasmis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmis()
    {
        return $this->hasMany(Tasmi::className(), ['comment_id' => 'id']);
    }

    /**
     * Gets query for [[Ziyadahs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZiyadahs()
    {
        return $this->hasMany(Ziyadah::className(), ['comment_id' => 'id']);
    }
}
