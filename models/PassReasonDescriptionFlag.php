<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pass_reason_description_flag".
 *
 * @property int $id
 * @property string $name
 *
 * @property PassReasonDescription[] $passReasonDescriptions
 */
class PassReasonDescriptionFlag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pass_reason_description_flag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[PassReasonDescriptions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassReasonDescriptions()
    {
        return $this->hasMany(PassReasonDescription::className(), ['pass_reason_description_flag_id' => 'id']);
    }
}
