<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quran_page".
 *
 * @property int $id
 * @property int $quran_juz_id
 * @property int $page
 *
 * @property QuranJuz $quranJuz
 */
class QuranPage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quran_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'quran_juz_id', 'page'], 'required'],
            [['id', 'quran_juz_id', 'page'], 'integer'],
            [['id'], 'unique'],
            [['quran_juz_id'], 'exist', 'skipOnError' => true, 'targetClass' => QuranJuz::className(), 'targetAttribute' => ['quran_juz_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quran_juz_id' => 'Quran Juz ID',
            'page' => 'Page',
        ];
    }

    /**
     * Gets query for [[QuranJuz]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuranJuz()
    {
        return $this->hasOne(QuranJuz::className(), ['id' => 'quran_juz_id']);
    }
}
