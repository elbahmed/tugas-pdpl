<?php

namespace app\models\users;

use Yii;

/**
 * This is the model class for table "attribute".
 *
 * @property int $id
 * @property string $table_name
 * @property string $column_name
 * @property string|null $label
 * @property string|null $hint
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table_name', 'column_name'], 'required'],
            [['table_name', 'column_name', 'label', 'hint'], 'string', 'max' => 255],
            [['table_name', 'column_name'], 'unique', 'targetAttribute' => ['table_name', 'column_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'column_name' => 'Column Name',
            'label' => 'Label',
            'hint' => 'Hint',
        ];
    }
}
