<?php

namespace app\models\users;

use Yii;

/**
 * This is the model class for table "users_log".
 *
 * @property int $id
 * @property int $type_id
 * @property string $user
 * @property int $time
 * @property string $user_ip
 * @property string $description
 *
 * @property LogType $type
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'user', 'time', 'user_ip', 'description'], 'required'],
            [['type_id', 'time'], 'integer'],
            [['description'], 'string'],
            [['user', 'user_ip'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'user' => 'User',
            'time' => 'Time',
            'user_ip' => 'User Ip',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Type]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(LogType::className(), ['id' => 'type_id']);
    }
}
