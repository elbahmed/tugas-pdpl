<?php

namespace app\models\users\forms;

use app\models\users\User;
use yii\base\Model;
use app\traits\LogingTrait;

class LoginForm extends Model
{   
    use LogingTrait;

    public $username;
    public $password;
    public $user;

    public $rememberMe = true;
    private $_user = false;

    public function rules()
    {
        return [
            [['username', 'password'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
            ['username', 'validateUsername'],
            ['password', 'validatePassword'],
            ['rememberMe', 'boolean'],
        ];
    }

    public function validateUsername($attribute) 
    {
        $user = $this->getUser();
        if (!$user) {
            $this->addError($attribute,  "Akun dengan username $this->username, tidak ditemukan.");
        }
        else {
            if ($user->getIsBlocked()) {
                $this->addError($attribute, 'Akun yang anda miliki telah di blokir silahkan hubungi admin.');
            }
        }
    }

    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user->validatePassword($this->password)) {
                $this->addError('password', 'Kata sandi yang anda masukkan salah.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->last_login_at = time();
            $user->last_login_ip = \Yii::$app->request->userIP;
            $user->save(false);

            return \Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*7 : 0);
        }

        return false;
    }

    public function getUser()
    {
        if ($this->_user === false) {
            return User::findByUsername($this->username);
        }

        return $this->_user;
    }

    public function afterValidate()
    {
        if ($this->hasErrors()) {
            $message = $this->getErrors("username");
            $message = $message + $this->getErrors('password');
            $message = strtoupper(implode(" ", $message));
            $this->createLogInLog($this->username, "[ERROR]LOGIN | [MSG=$message]");
        } else {
            $this->createLogInLog($this->username, '[SUCCESS]LOGIN | [MSG=ACK]');
        }
    }
}
