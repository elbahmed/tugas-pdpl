<?php

namespace app\models\users\forms;

use app\models\users\User;
use app\traits\GeneratorTrait;
use app\traits\LogingTrait;
use yii;

class RegistrationForm extends User 
{   
    use GeneratorTrait; 
    use LogingTrait;

    public $initialPassword;
    public $confirmPassword;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'first_name', 'last_name', 'confirmPassword', 'initialPassword'], 'required'],
            [['username', 'first_name', 'last_name', 'email'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'email'],
            [['confirmPassword'], 'compare', 'compareAttribute' => 'initialPassword'],
            [['initialPassword'],'string', 'min' => 5, 'max' => 32],
            [['mahad_id', 'role'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => "Username",
            'email' => "Email",
            'initialPassword' => 'Kata Sandi',
            'confirmPassword' => "Konfirmasi Kata Sandi",
            'first_name' => "Nama Depan",
            'last_name' => "Nama Belakang",
            'mahad_id' => "Ma'had",
            'role' => "Role",
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = $this->generateRandomString(32);
                $this->salt = $this->generateRandomAlphaNum(6);
                $this->password_hash = $this->hashPassword($this->initialPassword, $this->salt);
                $this->created_at = time();
                $this->created_ip = Yii::$app->request->userIP;
                $this->nick_name = $this->first_name .' '. $this->last_name;
                $this->initialPassword = '';
                $this->confirmPassword = '';

                return true;
            }

            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $auth = Yii::$app->authManager;
        $roleObject = $auth->getRole($this->role);
        $auth->assign($roleObject, $this->id);
        $this->createTransactionLog('[INSERT] at Role Assignment Table');
    }
}