<?php

namespace app\models\users\forms;

use yii\base\Model;

class AssignUserRoleForm extends Model
{
    public $item_name;
    public $user_id;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
        ];
    }
}