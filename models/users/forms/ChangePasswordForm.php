<?php

namespace app\models\users\forms;

use app\models\users\User;
use app\traits\GeneratorTrait;
use app\traits\LogingTrait;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    use GeneratorTrait;
    use LogingTrait;

    protected $salt;
    protected $user;
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'confirmPassword'], 'required'],
            ['oldPassword', 'validatePassword'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'newPassword'],
            [['newPassword'],'string', 'min' => 5, 'max' => 32]
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword()
    {
        $this->getUser();
        
        if (!$this->user->validatePassword($this->oldPassword)) {
            $this->addError('oldPassword', 'Kata sandi yang anda masukkan salah.');
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        $this->user = User::findOne(\Yii::$app->user->id);
    }

    public function changed()
    {
        $user = User::findOne($this->user->id);
        $this->salt = $this->generateRandomAlphaNum(6);
        $user->password_hash = $this->hashPassword($this->newPassword, $this->salt);
        $user->auth_key = $this->generateRandomString(32);
        $user->salt = $this->salt;

        if ($user->save(false)) {
            $this->newPassword = '';
            $this->confirmPassword = '';
            $this->salt = '';
            $this->createTransactionLog('[UPDATE] at User Table [Password]');
            
            return true;
        }
        return true;
    }
}