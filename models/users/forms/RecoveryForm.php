<?php

namespace app\models\users\forms;

use app\models\users\User;
use yii\base\Model;
use app\traits\GeneratorTrait;
use app\traits\LogingTrait;
use Yii;

class RecoveryForm extends Model
{
    use GeneratorTrait;
    use LogingTrait;

    public $username;
    protected $password;
    protected $salt;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'validateUsername'],
        ];
    }

    public function validateUsername($attribute) 
    {
        $user = $this->findUser();
        if (!$user) {
            $this->addError($attribute, "Akun dengan username $this->username, tidak ditemukan.");
        }
    }

    /**
     * Sends recovery message.
     *
     * @return bool
     */
    public function sendRecoveryMessage()
    {
        if (!$this->validate()) {
            return false;

        } else {
            $user = $this->findUser();
            $this->password = $this->generateRandomAlphaNum(8);
            $this->salt = $this->generateRandomAlphaNum(6);
            $user->password_hash = $this->hashPassword($this->password, $this->salt);
            $user->auth_key = $this->generateRandomString(32);
            $user->salt = $this->salt;

            if ($user->save(false)) {
                \Yii::$app->mailer->htmlLayout = "@app/mail/layouts/html";
                \Yii::$app->mailer
                ->compose(['html' => '@app/mail/recovery/html', 'text' => '@app/mail/recovery/text'], ['password' => $this->password])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject("Kata sandi Baru Anda")
                ->send();

                $this->createTransactionLog('[UPDATE] at User Table [Forgot Password]');
                
                return true;
            }
        }

        return false;
    }

    protected function findUser() 
    {
        if (($user = User::findOne(['username' => $this->username])) !== null) {
            return $user;
        }
    }
}