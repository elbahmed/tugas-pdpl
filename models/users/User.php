<?php

namespace app\models\users;

use app\models\Mahad;
use Yii;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "users_user".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $auth_key
 * @property string $salt
 * @property string $created_ip
 * @property string $last_login_ip
 * @property int $created_at
 * @property int $last_login_at
 * @property string $first_name
 * @property string $last_name
 * @property string $nick_name
 * @property string $email
 * @property string $mahad_id
 * @property int $blocked_status
 * @property int $blocked_at
 * @property string $role
 *
 * @property Mahad $mahad
 * @property UserToken[] $UserTokens
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'auth_key', 'salt', 'created_ip', 'last_login_ip', 'created_at', 'last_login_at', 'first_name', 'last_name', 'nick_name', 'email', 'blocked_status', 'blocked_at'], 'required'],
            [['created_at', 'last_login_at', 'blocked_status', 'blocked_at'], 'integer'],
            [['username', 'password_hash', 'auth_key', 'salt', 'created_ip', 'last_login_ip', 'first_name', 'last_name', 'nick_name', 'email', 'mahad_id', 'role'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['mahad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mahad::className(), 'targetAttribute' => ['mahad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'salt' => 'Salt',
            'created_ip' => 'Created Ip',
            'last_login_ip' => 'Last Login Ip',
            'created_at' => 'Created At',
            'last_login_at' => 'Last Login At',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'nick_name' => 'Nick Name',
            'email' => 'Email',
            'mahad_id' => 'Mahad ID',
            'blocked_status' => 'Blocked Status',
            'blocked_at' => 'Blocked At',
            'role' => 'Role',
        ];
    }

    /**
     * Gets query for [[Mahad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahad()
    {
        return $this->hasOne(Mahad::className(), ['id' => 'mahad_id']);
    }

    /**
     * Gets query for [[UserTokens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserTokens()
    {
        return $this->hasMany(UserToken::className(), ['user_id' => 'id']);
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        $hash = $this->getSalt() . $password;

        return Yii::$app->security->validatePassword($hash, $this->password_hash);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockedStatus() 
    {
        return $this->blocked_status;
    }

    /**
     * {@inheritdoc}
     */
    public function getIsBlocked() 
    {
        if ($this->getBlockedStatus()) {
           return true;
        }

        return false;
    }

    /**
     * Blocks the user by setting 'blocked_at' field to current time and regenerates auth_key.
     */
    public function block()
    {
        $this->updateAttributes([
            'auth_key' => \Yii::$app->security->generateRandomString(), 
            'blocked_at' => time(),
            'blocked_status' => 1,
        ]);

        return true;
    }

    /**
     * UnBlocks the user by setting 'blocked_at' field to null.
     */
    public function unblock()
    {
        $this->updateAttributes([
            'blocked_at' => null,
            'blocked_status' => 0,
        ]);

        return true;
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Method "' . __CLASS__ . '::' . __METHOD__ . '" is not implemented.');
    }
}
