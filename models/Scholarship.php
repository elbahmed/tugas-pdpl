<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scholarship".
 *
 * @property int $id
 * @property string $name
 * @property string $abbr
 * @property int $target
 *
 * @property Student[] $students
 */
class Scholarship extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'scholarship';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'abbr', 'target'], 'required'],
            [['target'], 'integer'],
            [['name', 'abbr'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'abbr' => 'Abbr',
            'target' => 'Target',
        ];
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['scholarship_id' => 'id']);
    }
}
