<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quran_juz".
 *
 * @property int $id
 * @property int $juz
 *
 * @property QuranPage[] $quranPages
 */
class QuranJuz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quran_juz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'juz'], 'required'],
            [['id', 'juz'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'juz' => 'Juz',
        ];
    }

    /**
     * Gets query for [[QuranPages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuranPages()
    {
        return $this->hasMany(QuranPage::className(), ['quran_juz_id' => 'id']);
    }
}
