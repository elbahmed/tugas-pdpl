<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property int $id
 * @property string $name
 * @property float $latitude
 * @property float $longitude
 * @property int $total_mahad
 *
 * @property Mahad[] $mahads
 * @property Regency[] $regencies
 * @property Student[] $students
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'latitude', 'longitude'], 'required'],
            [['id', 'total_mahad'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'total_mahad' => 'Total Mahad',
        ];
    }

    /**
     * Gets query for [[Mahads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahads()
    {
        return $this->hasMany(Mahad::className(), ['province_id' => 'id']);
    }

    /**
     * Gets query for [[Regencies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegencies()
    {
        return $this->hasMany(Regency::className(), ['province_id' => 'id']);
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['province_id' => 'id']);
    }
}
