<?php
namespace app\models\forms;

use app\models\Comment;
use app\models\MahadRating;
use app\models\MahadRatingType;
use yii\base\Model;

class MahadRatingForm extends Model 
{
    public $mahad_id;
    public $kebersihan;
    public $administrasi;
    public $keaktifan;
    public $pengabdian;
    public $ubudiyah;
    public $teacher_id;
    public $comment;
    public $created_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mahad_id', 'kebersihan', 'administrasi', 'keaktifan', 'pengabdian', 'ubudiyah', 'teacher_id', 'comment', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kebersihan' => 'Kebersihan',
            'administrasi' => 'Administrasi',
            'keaktifan' => 'Keaktifan',
            'pengabdian' => 'Pengabdian',
            'ubudiyah' => 'Ubudiyah',
            'teacher_id' => 'Ustadz Penilai',
            'comment' => 'Komentar',
        ];
    }

    public function insert()
    {
        if ($this->comment != null) {
            $mahadRatingType = MahadRatingType::find()->all();
            $comment = new Comment();
            $comment->comment = $this->comment;
            $comment->created_at = $this->created_at;
            $comment->save(false);
            foreach($mahadRatingType as $x) {
                $model = new MahadRating;
                $model->mahad_id = $this->mahad_id;
                $model->mahad_rating_type_id = $x['id'];
                $model->score_id = $this->{strtolower($x['name'])};
                $model->teacher_id = $this->teacher_id;
                $model->comment_id = $comment->id;
                $model->created_at = $this->created_at;
                $model->save(false);
            }
        } else {
            $mahadRatingType = MahadRatingType::find()->all();
            foreach($mahadRatingType as $x) {
                $model = new MahadRating;
                $model->mahad_id = $this->mahad_id;
                $model->mahad_rating_type_id = $x['id'];
                $model->score_id = $this->{strtolower($x['name'])};
                $model->teacher_id = $this->teacher_id;
                $model->created_at = $this->created_at;
                $model->save(false);
            }
        }
        return true;
    }
}
?>