<?php
namespace app\models\forms;

use app\models\Comment;
use app\models\StudentRating;
use app\models\StudentRatingType;
use yii\base\Model;

class StudentRatingForm extends Model 
{
    public $student_id;
    public $kelancaran;
    public $makhroj;
    public $teacher_id;
    public $comment;
    public $created_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kelancaran', 'makhroj', 'student_id', 'teacher_id', 'comment', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kelancaran' => 'Kelancaran',
            'Makhroj' => 'Makhroj',
            'teacher_id' => 'Ustadz Penilai',
            'comment' => 'Komentar',
        ];
    }

    public function insert()
    {
        if ($this->comment != null) {
            $studentRatingType = StudentRatingType::find()->all();
            $comment = new Comment();
            $comment->comment = $this->comment;
            $comment->created_at = $this->created_at;
            $comment->save(false);
            foreach($studentRatingType as $x) {
                $model = new StudentRating();
                $model->student_id = $this->student_id;
                $model->student_rating_type_id = $x['id'];
                $model->score_id = $this->{strtolower($x['name'])};
                $model->teacher_id = $this->teacher_id;
                $model->comment_id = $comment->id;
                $model->created_at = $this->created_at;
                $model->save(false);
            }
        } else {
            $studentRatingType = StudentRatingType::find()->all();
            foreach($studentRatingType as $x) {
                $model = new StudentRating();
                $model->student_id = $this->student_id;
                $model->student_rating_type_id = $x['id'];
                $model->score_id = $this->{strtolower($x['name'])};
                $model->teacher_id = $this->teacher_id;
                $model->created_at = $this->created_at;
                $model->save(false);
            }
        }
        return true;
    }
}
?>