<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahad_rating".
 *
 * @property int $id
 * @property string $mahad_id
 * @property int $mahad_rating_type_id
 * @property int $score_id
 * @property int $teacher_id
 * @property int $comment_id
 * @property int $created_at
 *
 * @property MahadRatingType $mahadRatingType
 * @property Score $score
 * @property Teacher $teacher
 * @property Mahad $mahad
 * @property Comment $comment
 */
class MahadRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahad_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mahad_id', 'mahad_rating_type_id', 'score_id', 'teacher_id', 'comment_id', 'created_at'], 'safe'],
            [['mahad_rating_type_id', 'score_id', 'teacher_id', 'comment_id', 'created_at'], 'integer'],
            [['mahad_id'], 'string', 'max' => 255],
            [['mahad_rating_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MahadRatingType::className(), 'targetAttribute' => ['mahad_rating_type_id' => 'id']],
            [['score_id'], 'exist', 'skipOnError' => true, 'targetClass' => Score::className(), 'targetAttribute' => ['score_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacher_id' => 'id']],
            [['mahad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mahad::className(), 'targetAttribute' => ['mahad_id' => 'id']],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mahad_id' => 'Mahad ID',
            'mahad_rating_type_id' => 'Tipe Penilaian Ma\'had',
            'score_id' => 'Nilai',
            'teacher_id' => 'Penilai',
            'comment_id' => 'Komentar',
            'created_at' => 'Tanggal',
        ];
    }

    /**
     * Gets query for [[MahadRatingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatingType()
    {
        return $this->hasOne(MahadRatingType::className(), ['id' => 'mahad_rating_type_id']);
    }

    /**
     * Gets query for [[Score]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScore()
    {
        return $this->hasOne(Score::className(), ['id' => 'score_id']);
    }

    /**
     * Gets query for [[Teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * Gets query for [[Mahad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahad()
    {
        return $this->hasOne(Mahad::className(), ['id' => 'mahad_id']);
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }
}
