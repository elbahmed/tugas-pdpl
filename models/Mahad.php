<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahad".
 *
 * @property string $id
 * @property int $mahad_type_id
 * @property int $province_id
 * @property int $regency_id
 * @property string $name
 * @property int $mudir
 * @property string $map
 * @property string $address
 *
 * @property MahadType $mahadType
 * @property Province $province
 * @property Regency $regency
 * @property MahadRating[] $mahadRatings
 * @property Student[] $students
 * @property Teacher[] $teachers
 */
class Mahad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mahad_type_id', 'province_id', 'regency_id', 'name', 'mudir', 'address'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
            [['mahad_type_id', 'province_id', 'regency_id'], 'integer'],
            [['address'], 'string'],
            [['id', 'name', 'map', 'mudir'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['mahad_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MahadType::className(), 'targetAttribute' => ['mahad_type_id' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
            [['regency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regency::className(), 'targetAttribute' => ['regency_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mahad_type_id' => 'Jenis Ma\'had',
            'province_id' => 'Provinsi',
            'regency_id' => 'Daerah',
            'name' => 'Nama',
            'mudir' => 'Mudir',
            'map' => 'Url Google Maps',
            'address' => 'Alamat',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $province = Province::findOne($this->province_id);
                $province->total_mahad = $province->total_mahad + 1;
                $this->id = sprintf('%s%03s', $this->regency_id, $province->total_mahad);
                $province->save(false);

                return true;
            }

            return true;
        }
        return false;
    }
    

    /**
     * Gets query for [[MahadType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadType()
    {
        return $this->hasOne(MahadType::className(), ['id' => 'mahad_type_id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * Gets query for [[Regency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegency()
    {
        return $this->hasOne(Regency::className(), ['id' => 'regency_id']);
    }

    /**
     * Gets query for [[MahadRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatings()
    {
        return $this->hasMany(MahadRating::className(), ['mahad_id' => 'id']);
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['mahad_id' => 'id']);
    }

    /**
     * Gets query for [[Teachers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::className(), ['mahad_id' => 'id']);
    }
}
