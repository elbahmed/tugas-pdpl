<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pass_reason_description".
 *
 * @property int $id
 * @property int $pass_reason_description_flag_id
 * @property string $description
 *
 * @property PassReason[] $passReasons
 * @property PassReasonDescriptionFlag $passReasonDescriptionFlag
 */
class PassReasonDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pass_reason_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pass_reason_description_flag_id', 'description'], 'required'],
            [['pass_reason_description_flag_id'], 'integer'],
            [['description'], 'string'],
            [['pass_reason_description_flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => PassReasonDescriptionFlag::className(), 'targetAttribute' => ['pass_reason_description_flag_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pass_reason_description_flag_id' => 'Pass Reason Description Flag ID',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[PassReasons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassReasons()
    {
        return $this->hasMany(PassReason::className(), ['pass_reason_description_id' => 'id']);
    }

    /**
     * Gets query for [[PassReasonDescriptionFlag]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassReasonDescriptionFlag()
    {
        return $this->hasOne(PassReasonDescriptionFlag::className(), ['id' => 'pass_reason_description_flag_id']);
    }
}
