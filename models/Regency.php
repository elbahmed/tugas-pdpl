<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "regency".
 *
 * @property int $id
 * @property int $province_id
 * @property string $name
 * @property float $latitude
 * @property float $longitude
 *
 * @property Mahad[] $mahads
 * @property Province $province
 * @property Student[] $students
 */
class Regency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'regency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'province_id', 'name', 'latitude', 'longitude'], 'required'],
            [['id', 'province_id'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'province_id' => 'Province ID',
            'name' => 'Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    /**
     * Gets query for [[Mahads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahads()
    {
        return $this->hasMany(Mahad::className(), ['regency_id' => 'id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['regency_id' => 'id']);
    }
}
