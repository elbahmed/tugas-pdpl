<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasmi_progress".
 *
 * @property int $id
 * @property string $name
 *
 * @property Tasmi[] $tasmis
 */
class TasmiProgress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasmi_progress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Tasmis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmis()
    {
        return $this->hasMany(Tasmi::className(), ['tasmi_progress_id' => 'id']);
    }
}
