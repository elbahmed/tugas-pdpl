<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_graduate".
 *
 * @property int $id
 * @property int $year
 * @property int $total_student
 *
 * @property Student[] $students
 */
class StudentGraduate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_graduate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year'], 'required'],
            [['year', 'total_student'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Year',
            'total_student' => 'Total Student',
        ];
    }

    /**
     * Gets query for [[Students]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['student_graduate_id' => 'id']);
    }
}
