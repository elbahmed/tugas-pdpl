<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calendar_day".
 *
 * @property int $id
 * @property string $day
 */
class CalendarDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['day'], 'required'],
            [['day'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => 'Day',
        ];
    }
}
