<?php

namespace app\models;

use PHPUnit\Framework\Constraint\Count;
use Yii;

/**
 * This is the model class for table "student".
 *
 * @property string $id ID
 * @property string $mahad_id
 * @property int $scholarship_id Beasiswa
 * @property int $regency_id Kota
 * @property int $province_id province
 * @property int $education_id Pendidikan Terakhir
 * @property int $student_flag_id status
 * @property int $student_graduate_id
 * @property int $student_group_id Pendidikan Terakhir
 * @property string $name name
 * @property string|null $nisn Nomor Induk Siswa Nasional
 * @property string|null $school_origin Asal Sekolah
 * @property int|null $school_origin_graduate Tahun Kelulusan di Sekolah Asal
 * @property string $address Alamat
 * @property string $father Ayah
 * @property string $mother Ibu
 * @property string|null $phone_parent Nomor Telp Wali
 * @property string $phone Nomer Telp
 * @property string|null $email Email
 * @property string $pob Tempat Lahir
 * @property int $dob Tanggal Lahir
 * @property int $doe Tanggal Masuk
 * @property string $photo Foto
 *
 * @property PassReason[] $passReasons
 * @property Province $province
 * @property Regency $regency
 * @property Scholarship $scholarship
 * @property Education $education
 * @property StudentFlag $studentFlag
 * @property StudentGraduate $studentGraduate
 * @property StudentGroup $studentGroup
 * @property Mahad $mahad
 * @property StudentExam[] $studentExams
 * @property StudentRating[] $studentRatings
 * @property Tasmi[] $tasmis
 * @property Ziyadah[] $ziyadahs
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mahad_id', 'scholarship_id', 'regency_id', 'province_id', 'education_id', 'student_group_id', 'name', 'address', 'father', 'mother', 'phone', 'pob', 'dob', 'doe'], 'required'],
            [['scholarship_id', 'regency_id', 'province_id', 'education_id', 'student_flag_id', 'student_graduate_id', 'student_group_id', 'school_origin_graduate', 'dob', 'doe'], 'integer'],
            [['address'], 'string'],
            [['id', 'mahad_id', 'name', 'nisn', 'school_origin', 'father', 'mother', 'phone_parent', 'phone', 'email', 'pob', 'photo'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
            [['regency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regency::className(), 'targetAttribute' => ['regency_id' => 'id']],
            [['scholarship_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scholarship::className(), 'targetAttribute' => ['scholarship_id' => 'id']],
            [['education_id'], 'exist', 'skipOnError' => true, 'targetClass' => Education::className(), 'targetAttribute' => ['education_id' => 'id']],
            [['student_flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentFlag::className(), 'targetAttribute' => ['student_flag_id' => 'id']],
            [['student_graduate_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentGraduate::className(), 'targetAttribute' => ['student_graduate_id' => 'id']],
            [['student_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentGroup::className(), 'targetAttribute' => ['student_group_id' => 'id']],
            [['mahad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mahad::className(), 'targetAttribute' => ['mahad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mahad_id' => 'Ma\'had',
            'scholarship_id' => 'Beasiswa',
            'regency_id' => 'Daerah',
            'province_id' => 'Provinsi',
            'education_id' => 'Pendidikan Terakhir',
            'student_flag_id' => 'Student Flag ID',
            'student_graduate_id' => 'Angkatan',
            'student_group_id' => 'Halaqoh',
            'name' => 'Nama',
            'nisn' => 'NISN',
            'school_origin' => 'Asal Sekolah',
            'school_origin_graduate' => 'Tahun kelulusan dari sekolah asal',
            'address' => 'Alamat',
            'father' => 'Ayah',
            'mother' => 'Ibu',
            'phone_parent' => 'Nomer Telpon Orang Tua',
            'phone' => 'Nomer Telpon',
            'email' => 'Email',
            'pob' => 'Tempat Lahir',
            'dob' => 'Tanggal Lahir',
            'doe' => 'Tanggal Muqim',
            'photo' => 'Foto',
        ];
    }

    public function totalZiyadah($date = null) 
    {
        if ($date != null) {
            $ziyadah = Ziyadah::find()
            ->select(['COUNT(DISTINCT quran_page.page) AS count_page, COUNT(DISTINCT quran_juz.juz) AS count_juz, quran_juz.juz, quran_juz.total_page'])
            ->join('LEFT OUTER JOIN', 'quran_page', 'quran_page.id BETWEEN ziyadah.start AND ziyadah.end')
            ->join('LEFT OUTER JOIN', 'quran_juz', 'quran_juz.id = quran_page.quran_juz_id')
            ->asArray()
            ->groupBy('quran_juz.juz')
            ->where(['student_id' => $this->id])
            ->andWhere(['>=',  'ziyadah.created_at', $date])
            ->orderBy(['quran_juz.juz' => SORT_ASC])
            ->all();
        } else {
            $ziyadah = Ziyadah::find()
            ->select(['COUNT(DISTINCT quran_page.page) AS count_page, COUNT(DISTINCT quran_juz.juz) AS count_juz, quran_juz.juz, quran_juz.total_page'])
            ->join('LEFT OUTER JOIN', 'quran_page', 'quran_page.id BETWEEN ziyadah.start AND ziyadah.end')
            ->join('LEFT OUTER JOIN', 'quran_juz', 'quran_juz.id = quran_page.quran_juz_id')
            ->asArray()
            ->groupBy('quran_juz.juz')
            ->where(['student_id' => $this->id])
            ->orderBy(['quran_juz.juz' => SORT_ASC])
            ->all();
        }

        $ziyadah_juz = 0;
        $ziyadah_page = 0;

        for ($i = 0; $i < count($ziyadah); $i++) {
            if ($ziyadah[$i]['count_page'] == $ziyadah[$i]['total_page']) {
                $ziyadah_juz++;

            } else {
                $ziyadah_page = $ziyadah_page + $ziyadah[$i]['count_page'];
            }
        }
        if ($ziyadah_juz == 0 && $ziyadah_page == 0) {
            return 'Belum ada';
        } else if ($ziyadah_juz == 0) {
            return $ziyadah_page . ' Safha';
        } elseif ($ziyadah_page == 0) {
            return $ziyadah_juz .' Juz';
        } else {
            return $ziyadah_juz . ' Juz ' . $ziyadah_page . ' Halaman';
        }
    }

    public function listZiyadah($date = null) 
    {
        if ($date != null) {
            $ziyadah = Ziyadah::find()
            ->select(['COUNT(DISTINCT quran_page.page) AS count_page, COUNT(DISTINCT quran_juz.juz) AS count_juz, quran_juz.juz, quran_juz.total_page'])
            ->join('LEFT OUTER JOIN', 'quran_page', 'quran_page.id BETWEEN ziyadah.start AND ziyadah.end')
            ->join('LEFT OUTER JOIN', 'quran_juz', 'quran_juz.id = quran_page.quran_juz_id')
            ->asArray()
            ->groupBy('quran_juz.juz')
            ->where(['student_id' => $this->id])
            ->andWhere(['>=', 'ziyadah.created_at', $date])
            ->orderBy(['quran_juz.juz' => SORT_ASC])
            ->all();
        } else {
            $ziyadah = Ziyadah::find()
            ->select(['COUNT(DISTINCT quran_page.page) AS count_page, COUNT(DISTINCT quran_juz.juz) AS count_juz, quran_juz.juz, quran_juz.total_page'])
            ->join('LEFT OUTER JOIN', 'quran_page', 'quran_page.id BETWEEN ziyadah.start AND ziyadah.end')
            ->join('LEFT OUTER JOIN', 'quran_juz', 'quran_juz.id = quran_page.quran_juz_id')
            ->asArray()
            ->groupBy('quran_juz.juz')
            ->where(['student_id' => $this->id])
            ->orderBy(['quran_juz.juz' => SORT_ASC])
            ->all();
        }
        $modelJuz = QuranJuz::find()->all();
        $result = [];
        $k = 0;
        for ($i = 0; $i < count($modelJuz); $i++) {
            $juz = $modelJuz[$i]['juz'];
            $l = 0;

            for ($j = 0; $j < count($ziyadah); $j++) {
                if ($ziyadah[$j]['juz'] == $juz) {
                    if ($ziyadah[$j]['count_page'] == $ziyadah[$j]['total_page']) {
                        $l = 2;
                    } elseif ($ziyadah[$j]['count_page'] < $ziyadah[$j]['total_page']) {
                        $l = 1;
                    }
                }
            }

            if ($l == 2) {
                $bg = 'success';
                $result[$k] = ['bg' => $bg, 'juz' => $juz];
                $k++;
            } elseif ($l == 1) {
                $bg = 'warning';
                $result[$k] = ['bg' => $bg, 'juz' => $juz];
                $k++;
            } elseif ($l == 0) {
                $bg = 'danger';
                $result[$k] = ['bg' => $bg, 'juz' => $juz];
                $k++;
            }
        }
        return $result;
    }

    /**
     * Gets query for [[PassReasons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassReasons()
    {
        return $this->hasMany(PassReason::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * Gets query for [[Regency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegency()
    {
        return $this->hasOne(Regency::className(), ['id' => 'regency_id']);
    }

    /**
     * Gets query for [[Scholarship]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScholarship()
    {
        return $this->hasOne(Scholarship::className(), ['id' => 'scholarship_id']);
    }

    /**
     * Gets query for [[Education]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEducation()
    {
        return $this->hasOne(Education::className(), ['id' => 'education_id']);
    }

    /**
     * Gets query for [[StudentFlag]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentFlag()
    {
        return $this->hasOne(StudentFlag::className(), ['id' => 'student_flag_id']);
    }

    /**
     * Gets query for [[StudentGraduate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGraduate()
    {
        return $this->hasOne(StudentGraduate::className(), ['id' => 'student_graduate_id']);
    }

    /**
     * Gets query for [[StudentGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroup()
    {
        return $this->hasOne(StudentGroup::className(), ['id' => 'student_group_id']);
    }

    /**
     * Gets query for [[Mahad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahad()
    {
        return $this->hasOne(Mahad::className(), ['id' => 'mahad_id']);
    }

    /**
     * Gets query for [[StudentExams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentExams()
    {
        return $this->hasMany(StudentExam::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[StudentRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentRatings()
    {
        return $this->hasMany(StudentRating::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[Tasmis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasmis()
    {
        return $this->hasMany(Tasmi::className(), ['student_id' => 'id']);
    }

    /**
     * Gets query for [[Ziyadahs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZiyadahs()
    {
        return $this->hasMany(Ziyadah::className(), ['student_id' => 'id']);
    }
}
