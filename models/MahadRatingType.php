<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahad_rating_type".
 *
 * @property int $id
 * @property string $name
 * @property int $quality
 * @property int $min_rating
 *
 * @property MahadRating[] $mahadRatings
 */
class MahadRatingType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahad_rating_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'quality', 'min_rating'], 'required'],
            [['quality', 'min_rating'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quality' => 'Quality',
            'min_rating' => 'Min Rating',
        ];
    }

    /**
     * Gets query for [[MahadRatings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahadRatings()
    {
        return $this->hasMany(MahadRating::className(), ['mahad_rating_type_id' => 'id']);
    }
}
